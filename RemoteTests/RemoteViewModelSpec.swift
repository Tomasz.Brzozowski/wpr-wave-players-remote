//
//  RemoteViewModelSpec.swift
//  WPR - Wave Players Remote
//
//  Created by Tomasz Brzozowski on 07/06/2021.
//

import Nimble
import Quick
import RxSwift
@testable import WPR___Wave_Players_Remote

private let dummyMediaPlayerVariables =
  MediaPlayerVariables(fileName: "Movie.avi", filePath: "C:/", state: "1", position: "0.5", positionString: "00:00:00",
                       duration: "3661000", durationString: "01:01:01", volumeLevel: "0.5")

private class StubTimerService: TimerProvider {
  func timer(_: RxTimeInterval, period _: RxTimeInterval) -> Observable<Int> { Observable.just(0) }
  func singleTimer(dueTime _: RxTimeInterval) -> Observable<Int> { Observable.just(0) }
}

private class StubUserDefaults: UserDefaultsProvider {
  let dummycontrolledPlayer: MediaPlayer = .mpc
  let dummyServerAddress = "192.168.0.0"
  let dummyServerPort = 13579
  let dummyServerPassword = "1234"
  let dummyVibrateOnWave = true
  let dummyPauseOnIncomingCall = true
  let dummyDimScreenTimeout: DimScreenTimeout = .ten

  func serverAddress() -> String { dummyServerAddress }
  func serverPort() -> Int { dummyServerPort }
  func serverPassword() -> String { dummyServerPassword }
  func controlledPlayer() -> MediaPlayer { dummycontrolledPlayer }
  func vibrateOnWave() -> Bool { dummyVibrateOnWave }
  func pauseOnIncomingCall() -> Bool { dummyPauseOnIncomingCall }
  func dimScreenTimeout() -> DimScreenTimeout { dummyDimScreenTimeout }

  func setServerAddress(_: String) {}
  func setServerPort(_: Int) {}
  func setServerPassword(_: String) {}
  func setControlledPlayer(_: MediaPlayer) {}
  func setVibrateOnWave(_: Bool) {}
  func setPauseOnIncomingCall(_: Bool) {}
  func setDimScreenTimeout(_: DimScreenTimeout) {}

  func serverAddressObservable() -> Observable<String> { Observable.just(dummyServerAddress) }
  func serverPortObservable() -> Observable<Int> { Observable.just(dummyServerPort) }
  func serverPasswordObservable() -> Observable<String> { Observable.just(dummyServerPassword) }
  func controlledPlayerObservable() -> Observable<MediaPlayer> { Observable.just(dummycontrolledPlayer) }
  func vibrateOnWaveObservable() -> Observable<Bool> { Observable.just(dummyVibrateOnWave) }
  func pauseOnIncomingCallObservable() -> Observable<Bool> { Observable.just(dummyPauseOnIncomingCall) }
  func dimScreenTimeoutObservable() -> Observable<DimScreenTimeout> { Observable.just(dummyDimScreenTimeout) }
}

private class StubProximityStateService: ProximityStateProvider {
  func proximityStateClose() -> Observable<Void> { Observable.never() }
  func toggleObservingProximityState() {}
  func disableObservingProximityState() {}
}

private class StubIncomingCallService: IncomingCallProvider {
  func incomingCall() -> PublishSubject<Void> { PublishSubject<Void>() }
}

private class StubUserNotificationsService: UserNotificationsProvider {
  func didBecomeActiveNotification() -> Observable<Void> { Observable.never() }
  func willResignActiveNotification() -> Observable<Void> { Observable.never() }
}

private class StubMediaPlayerClient: MediaPlayerClient {
  func getVariables(_: String, _: Int, _: String) -> Single<MediaPlayerVariables> {
    Single.just(dummyMediaPlayerVariables)
  }
  func setVolume(to _: Float, _: String, _: Int, _: String) -> Completable { Completable.empty() }
  func seek(to _: TimeCode, _: String, _: Int, _: String) -> Completable { Completable.empty() }
  func jump(by _: Int, _: String, _: Int, _: String) -> Completable { Completable.empty() }
  func togglePlayPause(_: String, _: Int, _: String) -> Completable { Completable.empty() }
  func pause(_: String, _: Int, _: String) -> Completable { Completable.empty() }
  func previousFile(_: String, _: Int, _: String) -> Completable { Completable.empty() }
  func nextFile(_: String, _: Int, _: String) -> Completable { Completable.empty() }
  func nextSubtitleTrack(subtitleOn _: Bool, _: String, _: Int, _: String) -> Completable { Completable.empty() }
  func toggleFullscreen(_: String, _: Int, _: String) -> Completable { Completable.empty() }
  func nextAudioTrack(_: String, _: Int, _: String) -> Completable { Completable.empty() }
  func nextRepeatMode(after _: Int, _: String, _: Int, _: String) -> Completable { Completable.empty() }
  func getSnapshot(_: String, _: Int, _: String) -> Single<Data> { Single.just(Data()) }
}

private class StubLogger: Logger {
  func verbose(_: Any, _: String, _: String, _: Int) {}
  func debug(_: Any, _: String, _: String, _: Int) {}
  func info(_: Any, _: String, _: String, _: Int) {}
  func warning(_: Any, _: String, _: String, _: Int) {}
  func error(_: Any, _: String, _: String, _: Int) {}
}

class RemoteViewModelSpec: QuickSpec {
  override func spec() {
    var viewModel: RemoteViewModel!
    beforeEach {
      viewModel =
        RemoteViewModel(log: StubLogger(), timerService: StubTimerService(), userDefaultsService: StubUserDefaults(),
                        proximityService: StubProximityStateService(), incomingCallService: StubIncomingCallService(),
                        userNotificationsService: StubUserNotificationsService(),
                        mpcClient: StubMediaPlayerClient(), vlcClient: StubMediaPlayerClient())
    }

    describe("restartTimers") {
      it("eventually sets view model state with fetched variables.") {
        viewModel.restartTimers()
        let expectedPosition = dummyMediaPlayerVariables.position
        expect(viewModel.mediaFileName.value).toEventually(equal(dummyMediaPlayerVariables.fileName))
        expect(viewModel.playPauseIconState.value).toEventually(equal(.play))
        expect(String(viewModel.mediaProgressSliderPosition.value)).toEventually(equal(expectedPosition))
        expect(viewModel.mediaCurrentPosition.value).toEventually(equal(dummyMediaPlayerVariables.positionString))
        expect(String(viewModel.mediaEndPosition.value)).toEventually(equal(dummyMediaPlayerVariables.durationString))
        expect(String(viewModel.volumeSliderPosition.value)).toEventually(equal(dummyMediaPlayerVariables.volumeLevel))
      }
    }

    describe("togglePlayPause") {
      it("toggles playPauseIconState value.") {
        let playPauseIconState = viewModel.playPauseIconState.value
        viewModel.togglePlayPause()
        expect(viewModel.playPauseIconState.value).notTo(equal(playPauseIconState))
      }
    }
  }
}
