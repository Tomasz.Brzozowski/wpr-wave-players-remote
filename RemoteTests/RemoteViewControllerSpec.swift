//
//  RemoteViewControllerSpec.swift
//  WPR - Wave Players Remote
//
//  Created by Tomasz Brzozowski on 09/06/2021.
//

import Nimble
import Quick
import RxRelay
import RxSwift
@testable import WPR___Wave_Players_Remote

private enum DummyRemoteVmState {
  static let fileName = "Movie.avi"
  static let waveIconState: WaveButtonIconState = .active
  static let playPauseIconState: PlayPauseButtonIconState = .play
  static let position: Float = 0.5
  static let volume: Float = 0.5
  static let imageData = Data()
  static let contentMode: ImageViewContentMode = .scaleToFill
  static let positionString = "00:30:00"
  static let endPositionString = "01:00:00"
  static let player: MediaPlayer = .mpc
}

private class MockRemoteViewModel: RemoteViewModeling {
  let mediaFileName = BehaviorRelay<String>(value: DummyRemoteVmState.fileName)
  let waveIconState = BehaviorRelay<WaveButtonIconState>(value: DummyRemoteVmState.waveIconState)
  let playPauseIconState = BehaviorRelay<PlayPauseButtonIconState>(value: DummyRemoteVmState.playPauseIconState)
  let mediaProgressSliderPosition = BehaviorRelay<Float>(value: DummyRemoteVmState.position)
  let volumeSliderPosition = BehaviorRelay<Float>(value: DummyRemoteVmState.volume)
  let mediaImageViewImageData = BehaviorRelay<Data>(value: DummyRemoteVmState.imageData)
  let mediaImageViewContentMode = BehaviorRelay<ImageViewContentMode>(value: DummyRemoteVmState.contentMode)
  let mediaCurrentPosition = BehaviorRelay<String>(value: DummyRemoteVmState.positionString)
  let mediaEndPosition = BehaviorRelay<String>(value: DummyRemoteVmState.endPositionString)
  let controlledPlayer = BehaviorRelay<MediaPlayer>(value: DummyRemoteVmState.player)
  let dimScreen = PublishSubject<Void>()
  let vibrate = PublishSubject<Void>()
  let didBecomeActiveNotification = Observable<Void>.never()
  let willResignActiveNotification = Observable<Void>.never()

  var restartTimersCallCount = 0
  var stopTimersCallCount = 0

  func restartTimers() {
    restartTimersCallCount += 1
  }

  func stopTimers() {
    stopTimersCallCount += 1
  }

  func restartFetchVariablesTimer() {}
  func stopFetchVariablesTimer() {}
  func restartDimScreenTimer() {}
  func togglePlayPause() {}
  func toggleFullscreen() {}
  func setVolume(to _: Float) {}
  func seek(to _: Float) {}
  func fastForward() {}
  func fastBackward() {}
  func nextSubtitle() {}
  func nextAudioTrack() {}
  func toggleRepeatMode() {}
  func nextFile() {}
  func previousFile() {}
  func toggleProximityIcon() {}
  func onProximityStateClose() {}
  func handleIncomingCall() {}
}

class RemoteViewControllerSpec: QuickSpec {
  override func spec() {
    var viewController: RemoteViewController!
    var viewModel: MockRemoteViewModel!
    beforeEach {
      viewModel = MockRemoteViewModel()
      viewController = RemoteViewController(viewModel: viewModel)
    }

    it("Restarts timers every time the view is about to appear") {
      expect(viewModel.restartTimersCallCount) == 0
      viewController.viewWillAppear(true)
      expect(viewModel.restartTimersCallCount) == 1
      viewController.viewWillAppear(true)
      expect(viewModel.restartTimersCallCount) == 2
    }

    it("Stops timers every time the view disappeared") {
      expect(viewModel.stopTimersCallCount) == 0
      viewController.viewWillDisappear(true)
      expect(viewModel.stopTimersCallCount) == 1
      viewController.viewWillDisappear(true)
      expect(viewModel.stopTimersCallCount) == 2
    }
  }
}
