//
//  TextSettingsCellViewModelSpec.swift
//  SettingsTests
//
//  Created by Tomasz Brzozowski on 08/06/2021.
//

import Nimble
import Quick
import RxRelay
import RxSwift
@testable import WPR___Wave_Players_Remote

let dummyValidIpAddresses = ["192.168.0.0", "192.168.1.1", "192.168.2.2"]

private class FakeUserDefaults: UserDefaultsProvider {
  let fakeServerAddress = BehaviorRelay<String>(value: dummyValidIpAddresses[0])
  let dummyServerPort = 13579
  let dummyServerPassword = "1234"
  let dummycontrolledPlayer: MediaPlayer = .mpc
  let dummyPauseOnIncomingCall = true
  let dummyVibrateOnWave = true
  let dummyDimScreenTimeout: DimScreenTimeout = .ten

  func serverAddress() -> String { fakeServerAddress.value }
  func serverPort() -> Int { dummyServerPort }
  func serverPassword() -> String { dummyServerPassword }
  func controlledPlayer() -> MediaPlayer { dummycontrolledPlayer }
  func vibrateOnWave() -> Bool { dummyVibrateOnWave }
  func pauseOnIncomingCall() -> Bool { dummyPauseOnIncomingCall }
  func dimScreenTimeout() -> DimScreenTimeout { dummyDimScreenTimeout }

  func setServerAddress(_ value: String) { fakeServerAddress.accept(value) }
  func setServerPort(_: Int) {}
  func setServerPassword(_: String) {}
  func setControlledPlayer(_: MediaPlayer) {}
  func setVibrateOnWave(_: Bool) {}
  func setPauseOnIncomingCall(_: Bool) {}
  func setDimScreenTimeout(_: DimScreenTimeout) {}

  func serverAddressObservable() -> Observable<String> { fakeServerAddress.asObservable() }
  func serverPortObservable() -> Observable<Int> { Observable.just(dummyServerPort) }
  func serverPasswordObservable() -> Observable<String> { Observable.just(dummyServerPassword) }
  func controlledPlayerObservable() -> Observable<MediaPlayer> { Observable.just(dummycontrolledPlayer) }
  func vibrateOnWaveObservable() -> Observable<Bool> { Observable.just(dummyVibrateOnWave) }
  func pauseOnIncomingCallObservable() -> Observable<Bool> { Observable.just(dummyPauseOnIncomingCall) }
  func dimScreenTimeoutObservable() -> Observable<DimScreenTimeout> { Observable.just(dummyDimScreenTimeout) }
}

private class StubTextValidation: TextValidationProvider {
  func validateIpWhileTyping(_: String) -> Bool { true }
  func validateIp(_ text: String) -> Bool { dummyValidIpAddresses.contains(text) }
  func validatePortText(_: String) -> Bool { true }
  func textWithoutLastCharacter(from text: String) -> String { text }
}

class ServerAddressSettingsCellViewModelSpec: QuickSpec {
  override func spec() {
    var fakeUserDefaults: UserDefaultsProvider!
    var viewModel: ServerAddressSettingsCellViewModel!
    beforeEach {
      fakeUserDefaults = FakeUserDefaults()
      viewModel = ServerAddressSettingsCellViewModel(
        userDefaultsService: fakeUserDefaults,
        textValidationService: StubTextValidation()
      )
    }

    it("sets its value from UserDefaults.") {
      fakeUserDefaults.setServerAddress(dummyValidIpAddresses[1])
      expect(viewModel.text.value).to(equal(dummyValidIpAddresses[1]))
      fakeUserDefaults.setServerAddress(dummyValidIpAddresses[2])
      expect(viewModel.text.value).to(equal(dummyValidIpAddresses[2]))
    }

    describe("validateIp") {
      it("sets given valid IPv4 address in UserDefaults") {
        let validAddress = dummyValidIpAddresses[1]
        viewModel.setSettingValue(for: validAddress)
        expect(viewModel.text.value).to(equal(validAddress))
      }
    }

    describe("setSetting") {
      it("sets given value for the given key in UserDefaults.") {
        viewModel.setSettingValue(for: dummyValidIpAddresses[2])
        expect(viewModel.text.value).to(equal(fakeUserDefaults.serverAddress()))
      }
    }
  }
}
