//
//  DimScreenTimeoutViewModelSpec.swift
//  SettingsTests
//
//  Created by Tomasz Brzozowski on 10/06/2021.
//

import Nimble
import Quick
import RxRelay
import RxSwift
@testable import WPR___Wave_Players_Remote

private let dummyDimScreenTimeout: DimScreenTimeout = .ten

private class FakeUserDefaults: UserDefaultsProvider {
  let dummyServerAddress = "192.168.0.0"
  let dummyServerPort = 13579
  let dummyServerPassword = "1234"
  let dummyPauseOnIncomingCall = true
  let dummycontrolledPlayer: MediaPlayer = .mpc
  let dummyVibrateOnWave = true
  let fakeDimScreenTimeout = BehaviorRelay<DimScreenTimeout>(value: dummyDimScreenTimeout)

  func serverAddress() -> String { dummyServerAddress }
  func serverPort() -> Int { dummyServerPort }
  func serverPassword() -> String { dummyServerPassword }
  func controlledPlayer() -> MediaPlayer { dummycontrolledPlayer }
  func vibrateOnWave() -> Bool { dummyVibrateOnWave }
  func pauseOnIncomingCall() -> Bool { dummyPauseOnIncomingCall }
  func dimScreenTimeout() -> DimScreenTimeout { fakeDimScreenTimeout.value }

  func setServerAddress(_: String) {}
  func setServerPort(_: Int) {}
  func setServerPassword(_: String) {}
  func setControlledPlayer(_: MediaPlayer) {}
  func setVibrateOnWave(_: Bool) {}
  func setPauseOnIncomingCall(_: Bool) {}
  func setDimScreenTimeout(_ value: DimScreenTimeout) { fakeDimScreenTimeout.accept(value) }

  func serverAddressObservable() -> Observable<String> { Observable.just(dummyServerAddress) }
  func serverPortObservable() -> Observable<Int> { Observable.just(dummyServerPort) }
  func serverPasswordObservable() -> Observable<String> { Observable.just(dummyServerPassword) }
  func controlledPlayerObservable() -> Observable<Int> { Observable.just(dummycontrolledPlayer.rawValue) }
  func controlledPlayerObservable() -> Observable<MediaPlayer> { Observable.just(dummycontrolledPlayer) }
  func vibrateOnWaveObservable() -> Observable<Bool> { Observable.just(dummyVibrateOnWave) }
  func pauseOnIncomingCallObservable() -> Observable<Bool> { Observable.just(dummyPauseOnIncomingCall) }
  func dimScreenTimeoutObservable() -> Observable<DimScreenTimeout> { fakeDimScreenTimeout.asObservable() }
}

class DimScreenTimeoutViewModelSpec: QuickSpec {
  override func spec() {
    var fakeUserDefaults: UserDefaultsProvider!
    var viewModel: DimScreenTimeoutViewModel!
    beforeEach {
      fakeUserDefaults = FakeUserDefaults()
      viewModel = DimScreenTimeoutViewModel(userDefaultsService: fakeUserDefaults)
    }

    it("sets cell selected for dim screen timeout from UserDefaults.") {
      fakeUserDefaults.setDimScreenTimeout(DimScreenTimeout.thirty)
      var selectedCellModels = viewModel.cellViewModels.value.filter { $0.selected == true }
      expect(selectedCellModels.count).to(equal(1))
      expect(selectedCellModels[0].description).to(equal(DimScreenTimeout.thirty.description))
      fakeUserDefaults.setDimScreenTimeout(DimScreenTimeout.oneMinute)
      selectedCellModels = viewModel.cellViewModels.value.filter { $0.selected == true }
      expect(selectedCellModels.count).to(equal(1))
      expect(selectedCellModels[0].description).to(equal(DimScreenTimeout.oneMinute.description))
    }
  }
}
