//
//  ToggleSettingsCellViewModelSpec.swift
//  WPR - Wave Players Remote
//
//  Created by Tomasz Brzozowski on 08/06/2021.
//

import Nimble
import Quick
import RxRelay
import RxSwift
@testable import WPR___Wave_Players_Remote

private class FakeUserDefaults: UserDefaultsProvider {
  let dummycontrolledPlayer: MediaPlayer = .mpc
  let dummyServerAddress = "192.168.0.0"
  let dummyServerPort = 13579
  let dummyServerPassword = "1234"
  let fakeVibrateOnWave = BehaviorRelay<Bool>(value: true)
  let dummyPauseOnIncomingCall = true
  let dummyDimScreenTimeout: DimScreenTimeout = .ten

  func serverAddress() -> String { dummyServerAddress }
  func serverPort() -> Int { dummyServerPort }
  func serverPassword() -> String { dummyServerPassword }
  func controlledPlayer() -> MediaPlayer { dummycontrolledPlayer }
  func vibrateOnWave() -> Bool { fakeVibrateOnWave.value }
  func pauseOnIncomingCall() -> Bool { dummyPauseOnIncomingCall }
  func dimScreenTimeout() -> DimScreenTimeout { dummyDimScreenTimeout }

  func setServerAddress(_: String) {}
  func setServerPort(_: Int) {}
  func setServerPassword(_: String) {}
  func setControlledPlayer(_: MediaPlayer) {}
  func setVibrateOnWave(_ value: Bool) { fakeVibrateOnWave.accept(value) }
  func setPauseOnIncomingCall(_: Bool) {}
  func setDimScreenTimeout(_: DimScreenTimeout) {}

  func serverAddressObservable() -> Observable<String> { Observable.just(dummyServerAddress) }
  func serverPortObservable() -> Observable<Int> { Observable.just(dummyServerPort) }
  func serverPasswordObservable() -> Observable<String> { Observable.just(dummyServerPassword) }
  func controlledPlayerObservable() -> Observable<MediaPlayer> { Observable.just(dummycontrolledPlayer) }
  func vibrateOnWaveObservable() -> Observable<Bool> { fakeVibrateOnWave.asObservable() }
  func pauseOnIncomingCallObservable() -> Observable<Bool> { Observable.just(dummyPauseOnIncomingCall) }
  func dimScreenTimeoutObservable() -> Observable<DimScreenTimeout> { Observable.just(dummyDimScreenTimeout) }
}

class VibrateOnWaveSettingsCellViewModelSpec: QuickSpec {
  override func spec() {
    var fakeUserDefaults: UserDefaultsProvider!
    var viewModel: VibrateOnWaveSettingsCellViewModel!
    beforeEach {
      fakeUserDefaults = FakeUserDefaults()
      viewModel = VibrateOnWaveSettingsCellViewModel(userDefaultsService: fakeUserDefaults)
    }

    it("sets its state from UserDefaults.") {
      fakeUserDefaults.setVibrateOnWave(true)
      expect(viewModel.state.value).to(equal(true))
      fakeUserDefaults.setVibrateOnWave(false)
      expect(viewModel.state.value).to(equal(false))
    }

    describe("switchState") {
      it("switches state value.") {
        let state = viewModel.state.value
        viewModel.setSettingValue()
        expect(viewModel.state.value).to(equal(!state))
      }
    }
  }
}
