//
//  ControlledPlayerViewModelSpec.swift
//  SettingsTests
//
//  Created by Tomasz Brzozowski on 09/06/2021.
//

import Nimble
import Quick
import RxRelay
import RxSwift
@testable import WPR___Wave_Players_Remote

private let dummycontrolledPlayer: MediaPlayer = .mpc

private class FakeUserDefaults: UserDefaultsProvider {
  let dummyServerAddress = "192.168.0.0"
  let dummyServerPort = 13579
  let dummyServerPassword = "1234"
  let dummyPauseOnIncomingCall = true
  let fakeControlledPlayer = BehaviorRelay<MediaPlayer>(value: dummycontrolledPlayer)
  let dummyVibrateOnWave = true
  let dummyDimScreenTimeout: DimScreenTimeout = .ten

  func serverAddress() -> String { dummyServerAddress }
  func serverPort() -> Int { dummyServerPort }
  func serverPassword() -> String { dummyServerPassword }
  func controlledPlayer() -> MediaPlayer { fakeControlledPlayer.value }
  func vibrateOnWave() -> Bool { dummyVibrateOnWave }
  func pauseOnIncomingCall() -> Bool { dummyPauseOnIncomingCall }
  func dimScreenTimeout() -> DimScreenTimeout { dummyDimScreenTimeout }

  func setServerAddress(_: String) {}
  func setServerPort(_: Int) {}
  func setServerPassword(_: String) {}
  func setControlledPlayer(_ value: MediaPlayer) { fakeControlledPlayer.accept(value) }
  func setVibrateOnWave(_: Bool) {}
  func setPauseOnIncomingCall(_: Bool) {}
  func setDimScreenTimeout(_: DimScreenTimeout) {}

  func serverAddressObservable() -> Observable<String> { Observable.just(dummyServerAddress) }
  func serverPortObservable() -> Observable<Int> { Observable.just(dummyServerPort) }
  func serverPasswordObservable() -> Observable<String> { Observable.just(dummyServerPassword) }

  func controlledPlayerObservable() -> Observable<MediaPlayer> { fakeControlledPlayer.asObservable() }
  func vibrateOnWaveObservable() -> Observable<Bool> { Observable.just(dummyVibrateOnWave) }
  func pauseOnIncomingCallObservable() -> Observable<Bool> { Observable.just(dummyPauseOnIncomingCall) }
  func dimScreenTimeoutObservable() -> Observable<DimScreenTimeout> { Observable.just(dummyDimScreenTimeout) }
}

class ControlledPlayerViewModelSpec: QuickSpec {
  override func spec() {
    var fakeUserDefaults: UserDefaultsProvider!
    var viewModel: ControlledPlayerViewModel!
    beforeEach {
      fakeUserDefaults = FakeUserDefaults()
      viewModel = ControlledPlayerViewModel(userDefaultsService: fakeUserDefaults)
    }

    it("sets cell selected for controlled media player from UserDefaults.") {
      fakeUserDefaults.setControlledPlayer(MediaPlayer.mpc)
      var selectedCellModels = viewModel.cellViewModels.value.filter { $0.selected == true }
      expect(selectedCellModels.count).to(equal(1))
      expect(selectedCellModels[0].description).to(equal(MediaPlayer.mpc.description))
      fakeUserDefaults.setControlledPlayer(MediaPlayer.vlc)
      selectedCellModels = viewModel.cellViewModels.value.filter { $0.selected == true }
      expect(selectedCellModels.count).to(equal(1))
      expect(selectedCellModels[0].description).to(equal(MediaPlayer.vlc.description))
    }
  }
}
