//
//  ServerDetailViewModelSpec.swift
//  SettingsTests
//
//  Created by Tomasz Brzozowski on 09/06/2021.
//
// swiftlint:disable function_body_length

import Nimble
import Quick
import RxRelay
import RxSwift
@testable import WPR___Wave_Players_Remote

private let dummyServer = Server(name: "ServerMpc", address: "192.168.0.0", port: 13579, password: "1234")
private let dummyServerFromDatabase = Server(name: "ServerVlc", address: "192.168.0.1", port: 8080, password: "1234")
private let dummyServerToAdd = Server(name: "ServerVlc2", address: "192.168.0.2", port: 8081, password: "12345")
private let dummyServerToEdit = Server(name: "ServerVlc3", address: "192.168.0.1", port: 8080, password: "12345")

private class StubLogger: Logger {
  func verbose(_: Any, _: String, _: String, _: Int) {}
  func debug(_: Any, _: String, _: String, _: Int) {}
  func info(_: Any, _: String, _: String, _: Int) {}
  func warning(_: Any, _: String, _: String, _: Int) {}
  func error(_: Any, _: String, _: String, _: Int) {}
}

private class StubUserDefaults: UserDefaultsProvider {
  let dummycontrolledPlayer: MediaPlayer = .mpc
  let dummyServerAddress = dummyServer.address
  let dummyServerPort = dummyServer.port
  let dummyServerPassword = dummyServer.password
  let dummyVibrateOnWave = true
  let dummyPauseOnIncomingCall = true
  let dummyDimScreenTimeout: DimScreenTimeout = .ten

  func serverAddress() -> String { dummyServerAddress }
  func serverPort() -> Int { dummyServerPort }
  func serverPassword() -> String { dummyServerPassword }
  func controlledPlayer() -> MediaPlayer { dummycontrolledPlayer }
  func vibrateOnWave() -> Bool { dummyVibrateOnWave }
  func pauseOnIncomingCall() -> Bool { dummyPauseOnIncomingCall }
  func dimScreenTimeoutValue() -> Int { dummyDimScreenTimeout.rawValue }
  func dimScreenTimeout() -> DimScreenTimeout { dummyDimScreenTimeout }

  func setServerAddress(_: String) {}
  func setServerPort(_: Int) {}
  func setServerPassword(_: String) {}
  func setControlledPlayer(_: MediaPlayer) {}
  func setVibrateOnWave(_: Bool) {}
  func setPauseOnIncomingCall(_: Bool) {}
  func setDimScreenTimeout(_: DimScreenTimeout) {}

  func serverAddressObservable() -> Observable<String> { Observable.just(dummyServerAddress) }
  func serverPortObservable() -> Observable<Int> { Observable.just(dummyServerPort) }
  func serverPasswordObservable() -> Observable<String> { Observable.just(dummyServerPassword) }
  func controlledPlayerObservable() -> Observable<MediaPlayer> { Observable.just(dummycontrolledPlayer) }
  func vibrateOnWaveObservable() -> Observable<Bool> { Observable.just(dummyVibrateOnWave) }
  func pauseOnIncomingCallObservable() -> Observable<Bool> { Observable.just(dummyPauseOnIncomingCall) }
  func dimScreenTimeoutObservable() -> Observable<DimScreenTimeout> { Observable.just(dummyDimScreenTimeout) }
}

private class FakeServerProvider: ServerProvider {
  let respondingLocalServers: [Server] = [dummyServer]
  var servers: [Server] = [dummyServerFromDatabase]

  func serverFromDatabase(_ address: String, _ port: Int) -> Server? {
    servers.first { $0.address == address && $0.port == port }
  }

  func serversFromDatabase(_ address: String, _ port: Int) -> [Server] {
    servers.filter { $0.address == address && $0.port == port }
  }

  func getServersFromDatabase() -> Single<[Server]> {
    Single.just(servers)
  }

  func getServerFromDatabase(address: String, port: Int) -> Single<Server?> {
    let server = serversFromDatabase(address, port)
    if !server.isEmpty {
      return Single.just(server[0])
    }
    return Single.just(nil)
  }

  func serverExists(address: String, port: Int) -> Bool {
    let server = serversFromDatabase(address, port)
    if !server.isEmpty {
      return true
    }
    return false
  }

  func addServer(_ server: Server) -> Completable {
    if !serverExists(address: server.address, port: server.port) {
      servers.append(server)
      return Completable.empty()
    }
    return Completable.error(NSError(domain: "addServer", code: 0, userInfo: nil))
  }

  func deleteServer(address: String, port: Int) -> Completable {
    servers.removeAll { server -> Bool in
      server.address == address && server.port == port
    }
    return Completable.empty()
  }

  func updateServer(_ server: Server) -> Completable {
    let index = servers.firstIndex { serverInDatabase -> Bool in
      server.address == serverInDatabase.address && server.port == serverInDatabase.port
    }
    if let index = index {
      servers.replaceSubrange(Range(index ... index), with: [server])
    } else {
      servers.append(server)
    }
    return Completable.empty()
  }

  func getRespondingLocalServers(port _: Int, password _: String) -> Observable<Server> {
    return Observable.just(respondingLocalServers[0])
  }
}

private class StubTextValidation: TextValidationProvider {
  func validateIpWhileTyping(_: String) -> Bool { true }
  func validateIp(_: String) -> Bool { true }
  func validatePortText(_: String) -> Bool { true }
  func textWithoutLastCharacter(from text: String) -> String { text }
}

private class FakeTextCellViewModel: TextCellViewModel {
  let name: String
  let text = BehaviorRelay<String>(value: "")
  func validateInputWhileTyping(_: String) {}

  init(_ name: String, _ text: String) {
    self.name = name
    self.text.accept(text)
  }
}

class ServerDetailViewModelSpec: QuickSpec {
  override func spec() {
    var addViewModel: ServerDetailViewModel!
    var editViewModel: ServerDetailViewModel!
    var getServerNameDetailCellViewModel: ((_ text: String) -> TextCellViewModel)!
    var getServerAddressDetailCellViewModel: ((_ text: String) -> TextCellViewModel)!
    var getServerPortDetailCellViewModel: ((_ text: String) -> TextCellViewModel)!
    var getServerPasswordDetailCellViewModel: ((_ text: String) -> TextCellViewModel)!
    var stubUserDefaults: UserDefaultsProvider!
    var stubTextValidation: StubTextValidation!
    var fakeServerProvider: FakeServerProvider!

    beforeEach {
      getServerNameDetailCellViewModel = { (text: String) in
        FakeTextCellViewModel(R.string.localizable.serverNameDetailName(), text)
      }
      getServerAddressDetailCellViewModel = { (text: String) in
        FakeTextCellViewModel(R.string.localizable.serverAddressDetailName(), text)
      }
      getServerPortDetailCellViewModel = { (text: String) in
        FakeTextCellViewModel(R.string.localizable.serverPortDetailName(), text)
      }
      getServerPasswordDetailCellViewModel = { (text: String) in
        FakeTextCellViewModel(R.string.localizable.serverPasswordDetailName(), text)
      }

      stubUserDefaults = StubUserDefaults()
      stubTextValidation = StubTextValidation()
      fakeServerProvider = FakeServerProvider()
      addViewModel =
        ServerDetailViewModel(
          log: StubLogger(),
          userDefaultsService: stubUserDefaults,
          textValidationService: stubTextValidation,
          serverMpcService: fakeServerProvider,
          serverVlcService: fakeServerProvider,
          getServerNameDetailCellViewModel: getServerNameDetailCellViewModel,
          getServerAddressDetailCellViewModel: getServerAddressDetailCellViewModel,
          getServerPortDetailCellViewModel: getServerPortDetailCellViewModel,
          getServerPasswordDetailCellViewModel: getServerPasswordDetailCellViewModel
        )
      editViewModel =
        ServerDetailViewModel(
          log: StubLogger(),
          userDefaultsService: stubUserDefaults,
          textValidationService: stubTextValidation,
          serverMpcService: fakeServerProvider,
          serverVlcService: fakeServerProvider,
          getServerNameDetailCellViewModel: getServerNameDetailCellViewModel,
          getServerAddressDetailCellViewModel: getServerAddressDetailCellViewModel,
          getServerPortDetailCellViewModel: getServerPortDetailCellViewModel,
          getServerPasswordDetailCellViewModel: getServerPasswordDetailCellViewModel,
          editAddress: dummyServerToEdit.address,
          editPort: String(dummyServerToEdit.port)
        )
    }

    describe("confirm add") {
      it("adds new server with given data to database") {
        let name = dummyServerToAdd.name
        let address = dummyServerToAdd.address
        let port = dummyServerToAdd.port
        let password = dummyServerToAdd.password

        addViewModel.sections.accept([
          SectionOfServerDetail(items: [
            .name(FakeTextCellViewModel(R.string.localizable.serverNameDetailName(), name)),
            .address(FakeTextCellViewModel(R.string.localizable.serverAddressDetailName(), address)),
            .port(FakeTextCellViewModel(R.string.localizable.serverPortDetailName(), String(port))),
            .password(FakeTextCellViewModel(R.string.localizable.serverPasswordDetailName(), password)),
          ]),
        ])
        addViewModel.confirm()

        let addedServerFromDatabase = fakeServerProvider.serverFromDatabase(address, port)
        expect(addedServerFromDatabase).notTo(beNil())
        expect(addedServerFromDatabase?.name).to(equal(name))
        expect(addedServerFromDatabase?.address).to(equal(address))
        expect(addedServerFromDatabase?.port).to(equal(port))
        expect(addedServerFromDatabase?.password).to(equal(password))
      }

      it("does not add already existing server to database") {
        let name = dummyServerFromDatabase.name
        let address = dummyServerFromDatabase.address
        let port = dummyServerFromDatabase.port
        let password = dummyServerFromDatabase.password

        let serversFromDatabase = fakeServerProvider.serversFromDatabase(address, port)
        expect(serversFromDatabase.count).to(equal(1))

        addViewModel.sections.accept([
          SectionOfServerDetail(items: [
            .name(FakeTextCellViewModel(R.string.localizable.serverNameDetailName(), name)),
            .address(FakeTextCellViewModel(R.string.localizable.serverAddressDetailName(), address)),
            .port(FakeTextCellViewModel(R.string.localizable.serverPortDetailName(), String(port))),
            .password(FakeTextCellViewModel(R.string.localizable.serverPasswordDetailName(), password)),
          ]),
        ])
        addViewModel.confirm()

        let updatedServersFromDatabase = fakeServerProvider.serversFromDatabase(address, port)
        expect(updatedServersFromDatabase.count).to(equal(1))
      }
    }

    describe("confirm edit") {
      it("updates existing server in database") {
        let name = dummyServerToEdit.name
        let address = dummyServerToEdit.address
        let port = dummyServerToEdit.port
        let password = dummyServerToEdit.password

        editViewModel.sections.accept([
          SectionOfServerDetail(items: [
            .name(FakeTextCellViewModel(R.string.localizable.serverNameDetailName(), name)),
            .address(FakeTextCellViewModel(R.string.localizable.serverAddressDetailName(), address)),
            .port(FakeTextCellViewModel(R.string.localizable.serverPortDetailName(), String(port))),
            .password(FakeTextCellViewModel(R.string.localizable.serverPasswordDetailName(), password)),
          ]),
        ])
        editViewModel.confirm()

        let updatedServersFromDatabase = fakeServerProvider.serversFromDatabase(address, port)
        expect(updatedServersFromDatabase).notTo(beNil())
        expect(updatedServersFromDatabase.count).to(equal(1))
        expect(updatedServersFromDatabase[0].name).to(equal(name))
        expect(updatedServersFromDatabase[0].address).to(equal(address))
        expect(updatedServersFromDatabase[0].port).to(equal(port))
        expect(updatedServersFromDatabase[0].password).to(equal(password))
      }
    }
  }
}

// swiftlint:enable function_body_length
