//
//  ServerManagerViewModelTests.swift
//  WPR - Wave Players Remote
//
//  Created by Tomasz Brzozowski on 07/06/2021.
//

import Nimble
import Quick
import RxSwift
@testable import WPR___Wave_Players_Remote

private let dummyServer = Server(name: "ServerMpc", address: "192.168.0.0", port: 13579, password: "1234")
private let dummyServerFromDatabase = Server(name: "ServerVlc", address: "192.168.0.1", port: 8080, password: "1234")

private class StubLogger: Logger {
  func verbose(_: Any, _: String, _: String, _: Int) {}
  func debug(_: Any, _: String, _: String, _: Int) {}
  func info(_: Any, _: String, _: String, _: Int) {}
  func warning(_: Any, _: String, _: String, _: Int) {}
  func error(_: Any, _: String, _: String, _: Int) {}
}

private class StubUserDefaults: UserDefaultsProvider {
  let dummycontrolledPlayer: MediaPlayer = .mpc
  let dummyServerAddress = dummyServer.address
  let dummyServerPort = dummyServer.port
  let dummyServerPassword = dummyServer.password
  let dummyVibrateOnWave = true
  let dummyPauseOnIncomingCall = true
  let dummyDimScreenTimeout: DimScreenTimeout = .ten

  func serverAddress() -> String { dummyServerAddress }
  func serverPort() -> Int { dummyServerPort }
  func serverPassword() -> String { dummyServerPassword }
  func controlledPlayer() -> MediaPlayer { dummycontrolledPlayer }
  func vibrateOnWave() -> Bool { dummyVibrateOnWave }
  func pauseOnIncomingCall() -> Bool { dummyPauseOnIncomingCall }
  func dimScreenTimeout() -> DimScreenTimeout { dummyDimScreenTimeout }

  func setServerAddress(_: String) {}
  func setServerPort(_: Int) {}
  func setServerPassword(_: String) {}
  func setControlledPlayer(_: MediaPlayer) {}
  func setVibrateOnWave(_: Bool) {}
  func setPauseOnIncomingCall(_: Bool) {}
  func setDimScreenTimeout(_: DimScreenTimeout) {}

  func serverAddressObservable() -> Observable<String> { Observable.just(dummyServerAddress) }
  func serverPortObservable() -> Observable<Int> { Observable.just(dummyServerPort) }
  func serverPasswordObservable() -> Observable<String> { Observable.just(dummyServerPassword) }
  func controlledPlayerObservable() -> Observable<MediaPlayer> { Observable.just(dummycontrolledPlayer) }
  func vibrateOnWaveObservable() -> Observable<Bool> { Observable.just(dummyVibrateOnWave) }
  func pauseOnIncomingCallObservable() -> Observable<Bool> { Observable.just(dummyPauseOnIncomingCall) }
  func dimScreenTimeoutObservable() -> Observable<DimScreenTimeout> { Observable.just(dummyDimScreenTimeout) }
}

private class FakeServerProvider: ServerProvider {
  let respondingLocalServers: [Server] = [dummyServer]
  var servers: [Server] = [dummyServerFromDatabase]

  func serversFromDatabase(_ address: String, _ port: Int) -> [Server] {
    servers.filter { $0.address == address && $0.port == port }
  }

  func getServersFromDatabase() -> Single<[Server]> {
    Single.just(servers)
  }

  func getServerFromDatabase(address: String, port: Int) -> Single<Server?> {
    let server = serversFromDatabase(address, port)
    if !server.isEmpty {
      return Single.just(server[0])
    }
    return Single.just(nil)
  }

  func serverExists(address: String, port: Int) -> Bool {
    let server = serversFromDatabase(address, port)
    if !server.isEmpty {
      return true
    }
    return false
  }

  func addServer(_ server: Server) -> Completable {
    if !serverExists(address: server.address, port: server.port) {
      servers.append(server)
      return Completable.empty()
    }
    return Completable.error(NSError(domain: "addServer", code: 0, userInfo: nil))
  }

  func deleteServer(address: String, port: Int) -> Completable {
    servers.removeAll { server -> Bool in
      server.address == address && server.port == port
    }
    return Completable.empty()
  }

  func updateServer(_ server: Server) -> Completable {
    let index = servers.firstIndex { serverInDatabase -> Bool in
      server.address == serverInDatabase.address && server.port == serverInDatabase.port
    }
    if let index = index {
      servers.replaceSubrange(Range(index ... index), with: [server])
    } else {
      servers.append(server)
    }
    return Completable.empty()
  }

  func getRespondingLocalServers(port _: Int, password _: String) -> Observable<Server> {
    return Observable.just(respondingLocalServers[0])
  }
}

class ServerManagerViewModelSpec: QuickSpec {
  private func getServerCellViewModel(serverCell: ServerCell) -> ServerCellViewModeling {
    ServerCellViewModel(userDefaultsService: StubUserDefaults(), cell: serverCell)
  }

  override func spec() {
    var viewModel: ServerManagerViewModel!
    var stubUserDefaults: UserDefaultsProvider!
    var fakeServerProvider: FakeServerProvider!
    beforeEach {
      stubUserDefaults = StubUserDefaults()
      fakeServerProvider = FakeServerProvider()
      let getServerCellViewModel = { serverCell in
        ServerCellViewModel(userDefaultsService: stubUserDefaults, cell: serverCell)
      }
      viewModel = ServerManagerViewModel(
        log: StubLogger(),
        userDefaultsService: stubUserDefaults,
        serverMpcService: fakeServerProvider,
        serverVlcService: fakeServerProvider,
        getServerCellViewModel: getServerCellViewModel
      )
    }

    describe("refreshServers") {
      it("sets cells with servers from database and servers responding in local network") {
        let databaseServerCellViewModels = fakeServerProvider.servers.map { server -> ServerCellViewModeling in
          ServerCellViewModel(userDefaultsService: stubUserDefaults,
                              cell: ServerCell(server: server, isResponding: false))
        }
        viewModel.refreshServers()

        let respondingLocalServerCellViewModels = fakeServerProvider.respondingLocalServers.map {
          server -> ServerCellViewModeling in
          ServerCellViewModel(userDefaultsService: stubUserDefaults,
                              cell: ServerCell(server: server, isResponding: true))
        }
        let cellViewModels = viewModel.cellViewModels.value
        expect(cellViewModels[0] == databaseServerCellViewModels[0]).to(equal(true))
        expect(cellViewModels[1] == respondingLocalServerCellViewModels[0]).to(equal(true))
      }
    }
  }
}
