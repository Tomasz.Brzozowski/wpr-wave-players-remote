//
//  ServerManagerViewControllerSpec.swift
//  ServerManagerTests
//
//  Created by Tomasz Brzozowski on 09/06/2021.
//

import Nimble
import Quick
import RxRelay
import RxSwift
@testable import WPR___Wave_Players_Remote

private class MockServerManagerViewModel: ServerManagerViewModeling {
  var cellViewModels = BehaviorRelay<[ServerCellViewModeling]>(value: [])
  var refreshInProgress = BehaviorRelay<Bool>(value: false)
  var onSwitchToRemote = PublishSubject<Void>()
  var showAlert = PublishSubject<String>()

  var refreshServersCallCount = 0

  func refreshServers() {
    refreshServersCallCount += 1
  }
  func deleteServer(_: ServerCellViewModeling) {}
  func validatePortNumber(port _: String) -> Bool { return true }
  func goToRemote(with _: ServerCellViewModeling) {}
}

private class StubServerDetailViewModel: ServerDetailViewModeling {
  var sections = BehaviorRelay<[SectionOfServerDetail]>(value: [])
  var showAlert = PublishSubject<String>()
  var dismiss = PublishSubject<Void>()

  func setCellsData() {}
  func confirm() {}
}

class ServerManagerViewControllerSpec: QuickSpec {
  override func spec() {
    var viewController: ServerManagerViewController!
    var viewModel: MockServerManagerViewModel!
    var detailViewModel: StubServerDetailViewModel!
    beforeEach {
      viewModel = MockServerManagerViewModel()
      detailViewModel = StubServerDetailViewModel()
      let getServerDetailVc = { (_: String, _: String, _: String?, _: String?) -> ServerDetailViewController in
        ServerDetailViewController(viewModel: detailViewModel, confirmButtonTitle: "", navigationBarTitle: "")
      }
      viewController = ServerManagerViewController(viewModel: viewModel, getServerDetailVc: getServerDetailVc)
    }

    it("Refreshes servers every time the view is about to appear") {
      expect(viewModel.refreshServersCallCount) == 0
      viewController.viewWillAppear(true)
      expect(viewModel.refreshServersCallCount) == 1
      viewController.viewWillAppear(true)
      expect(viewModel.refreshServersCallCount) == 2
    }
  }
}
