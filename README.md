# WPR Wave Players Remote
Control your media player in local network. Play and pause without touching the phone.

## Features
- Control VLC and MPC-HC desktop media players
- Wave over the smartphone's proximity sensor to play/pause playback
- Automatically find and save responding servers in local network
- Add, edit and save custom local servers
- Dim screen automatically to save battery life
- Pause playback on incoming call

## Screenshots

<p>
  <img src="./Resources/Screenshots/MPC-HC-screenshot1.png"  alt="drawing" width="30%"/>
  <b>&nbsp;&nbsp;&nbsp;</b>
  <img src="./Resources/Screenshots/MPC-HC-screenshot2.png"  alt="drawing" width="30%"/> 
  <b>&nbsp;&nbsp;&nbsp;</b>
  <img src="./Resources/Screenshots/MPC-HC-screenshot4.png"  alt="drawing" width="30%"/>
</p>
<p>
  <img src="./Resources/Screenshots/MPC-HC-screenshot3.png"  alt="drawing" width="30%"/>
  <b>&nbsp;&nbsp;&nbsp;</b>
  <img src="./Resources/Screenshots/MPC-HC-screenshot5.png"  alt="drawing" width="30%"/> 
  <b>&nbsp;&nbsp;&nbsp;</b>
  <img src="./Resources/Screenshots/MPC-HC-screenshot6.png"  alt="drawing" width="30%"/>
</p>
<p>
  <img src="./Resources/Screenshots/VLC-screenshot1.png" alt="drawing" width="30%"/>
  <b>&nbsp;&nbsp;&nbsp;</b>
  <img src="./Resources/Screenshots/VLC-screenshot2.png" alt="drawing" width="30%"/>
</p>

## Installation

1. Download and install the latest version of Cocoapods (tested on Cocoapods 1.10.1)
2. Download and install the latest version of Xcode (tested on Xcode 12.4).
3. Clone the project.
4. Open WPR - Wave Players Remote.xcworkspace in Xcode.
5. Open terminal and from project directory run:

    ``` pod install ```
6. Install the app on Simulator or iPhone (iOS 10.0 or later)
