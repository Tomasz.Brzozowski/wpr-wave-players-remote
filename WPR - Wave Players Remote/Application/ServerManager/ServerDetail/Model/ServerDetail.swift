//
//  ServerDetail.swift
//  WPR - Wave Players Remote
//
//  Created by Tomasz Brzozowski on 14/05/2021.
//

struct ServerDetail {
  let name: String
  let address: String
  let port: String
  let password: String
}

extension ServerDetail {
  init(server: Server) {
    name = server.name
    address = server.address
    port = String(server.port)
    password = server.password
  }

  init() {
    name = ""
    address = ""
    port = ""
    password = ""
  }
}
