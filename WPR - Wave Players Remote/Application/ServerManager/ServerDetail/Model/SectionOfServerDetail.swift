//
//  SectionOfServerDetail.swift
//  WPR - Wave Players Remote
//
//  Created by Tomasz Brzozowski on 29/06/2021.
//

import RxDataSources

struct SectionOfServerDetail {
  var items: [Item]
}

extension SectionOfServerDetail: SectionModelType {
  typealias Item = ServerDetailCellViewModelType

  init(original: SectionOfServerDetail, items: [Item]) {
    self = original
    self.items = items
  }
}
