//
//  ServerDetailViewController.swift
//  WPR - Wave Players Remote
//
//  Created by Tomasz Brzozowski on 30/04/2021.
//

import RxDataSources
import RxSwift
import UIKit

class ServerDetailViewController: UIViewController {
  private static let nameCellReuseId = "nameCell"
  private static let addressCellReuseId = "addressCell"
  private static let portCellReuseId = "portCell"
  private static let passwordCellReuseId = "passwordCell"

  private let cancelButton: UIBarButtonItem = {
    let title = R.string.localizable.cancelActionTitle()
    let cancelButton = UIBarButtonItem(title: title, style: .done, target: self, action: nil)
    return cancelButton
  }()

  private let confirmButton: UIBarButtonItem = {
    let confirmButton = UIBarButtonItem(title: "", style: .done, target: self, action: nil)
    return confirmButton
  }()

  private let tableView: UITableView = {
    let bounds = UIScreen.main.bounds
    let tableView = UITableView(frame: CGRect(x: 0, y: 0, width: bounds.width, height: bounds.height), style: .grouped)
    tableView.backgroundColor = R.color.background()
    tableView.dragDelegate = nil
    tableView.sectionHeaderHeight = 50
    return tableView
  }()

  private let viewModel: ServerDetailViewModeling
  private let disposeBag = DisposeBag()
  private let dataSource: RxTableViewSectionedReloadDataSource<SectionOfServerDetail> = {
    RxTableViewSectionedReloadDataSource<SectionOfServerDetail>(
      configureCell: { _, table, _, item in
        switch item {
        case let .name(textViewModel):
          return makeCell(with: ServerDetailCellViewModelType.name(textViewModel), from: table)
        case let .address(textViewModel):
          return makeCell(with: ServerDetailCellViewModelType.address(textViewModel), from: table)
        case let .port(textViewModel):
          return makeCell(with: ServerDetailCellViewModelType.port(textViewModel), from: table)
        case let .password(textViewModel):
          return makeCell(with: ServerDetailCellViewModelType.password(textViewModel), from: table)
        }
      }
    )
  }()

  private static func makeCell(
    with element: ServerDetailCellViewModelType,
    from tableView: UITableView
  ) -> UITableViewCell {
    switch element {
    case let .name(viewModel):
      if let cell = tableView.dequeueReusableCell(withIdentifier: ServerDetailViewController.nameCellReuseId)
        as? ServerNameDetailViewCell {
        cell.viewModel = viewModel as? ServerNameDetailCellViewModel
        return cell
      }
    case let .address(viewModel):
      if let cell = tableView.dequeueReusableCell(withIdentifier: ServerDetailViewController.addressCellReuseId)
        as? ServerAddressDetailViewCell {
        cell.viewModel = viewModel as? ServerAddressDetailCellViewModel
        cell.valueTextField.keyboardType = .numbersAndPunctuation
        return cell
      }
    case let .port(viewModel):
      if let cell = tableView.dequeueReusableCell(withIdentifier: ServerDetailViewController.portCellReuseId)
        as? ServerPortDetailViewCell {
        cell.viewModel = viewModel as? ServerPortDetailCellViewModel
        cell.valueTextField.keyboardType = .numbersAndPunctuation
        return cell
      }
    case let .password(viewModel):
      if let cell = tableView.dequeueReusableCell(withIdentifier: ServerDetailViewController.passwordCellReuseId)
        as? ServerPasswordDetailViewCell {
        cell.viewModel = viewModel as? ServerPasswordDetailCellViewModel
        return cell
      }
    }
    return UITableViewCell()
  }

  init(viewModel: ServerDetailViewModeling, confirmButtonTitle: String, navigationBarTitle: String) {
    self.viewModel = viewModel
    confirmButton.title = confirmButtonTitle
    super.init(nibName: nil, bundle: nil)
    navigationItem.title = navigationBarTitle
  }

  @available(*, unavailable)
  required init?(coder _: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  override func viewDidLoad() {
    super.viewDidLoad()
    configureUi()
    bindViewModel()
    delegatePresentation()
  }

  private func configureUi() {
    navigationItem.hidesBackButton = false
    navigationItem.leftBarButtonItem = cancelButton
    navigationItem.rightBarButtonItem = confirmButton

    tableView.tableFooterView = UIView()
    tableView.register(
      ServerNameDetailViewCell.self,
      forCellReuseIdentifier: ServerDetailViewController.nameCellReuseId
    )
    tableView.register(
      ServerAddressDetailViewCell.self,
      forCellReuseIdentifier: ServerDetailViewController.addressCellReuseId
    )
    tableView.register(
      ServerPortDetailViewCell.self,
      forCellReuseIdentifier: ServerDetailViewController.portCellReuseId
    )
    tableView.register(
      ServerPasswordDetailViewCell.self,
      forCellReuseIdentifier: ServerDetailViewController.passwordCellReuseId
    )

    view.backgroundColor = R.color.background()
    view.addSubview(tableView)
    view.setNeedsUpdateConstraints()
  }

  override func updateViewConstraints() {
    tableView.snp.makeConstraints { make in
      make.top.bottom.leading.trailing.equalTo(view.safeAreaLayoutGuide)
    }
    super.updateViewConstraints()
  }

  private func bindViewModel() {
    viewModel.sections
      .bind(to: tableView.rx.items(dataSource: dataSource))
      .disposed(by: disposeBag)

    confirmButton.rx.tap
      .subscribe { [weak self] _ in
        self?.viewModel.confirm()
      }.disposed(by: disposeBag)

    viewModel.showAlert
      .bind { [weak self] text in
        let alertVc = UIAlertController(message: text, preferredStyle: .alert)
        alertVc.addAction(UIAlertAction(title: R.string.localizable.alertActionTitle(), style: .default, handler: nil))
        self?.present(alertVc, animated: true, completion: nil)
      }.disposed(by: disposeBag)

    viewModel.dismiss
      .bind { [weak self] _ in
        self?.notifyDismiss()
      }.disposed(by: disposeBag)
  }

  private func notifyDismiss() {
    let presentationDelegate = navigationController?.presentationController?.delegate
    if let presentationDelegate = presentationDelegate, let navController = navigationController {
      let presentationController = UIPresentationController(
        presentedViewController: navController,
        presenting: navController.presentingViewController
      )
      presentationDelegate.presentationControllerDidDismiss?(presentationController)
    }
    dismiss(animated: true, completion: nil)
  }

  private func delegatePresentation() {
    cancelButton.rx.tap
      .subscribe { [weak self] _ in
        self?.notifyDismiss()
      }.disposed(by: disposeBag)
  }
}
