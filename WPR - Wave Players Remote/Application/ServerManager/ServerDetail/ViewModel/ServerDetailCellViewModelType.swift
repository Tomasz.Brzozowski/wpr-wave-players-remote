//
//  ServerDetailCellViewModelType.swift
//  WPR - Wave Players Remote
//
//  Created by Tomasz Brzozowski on 29/06/2021.
//

enum ServerDetailCellViewModelType {
  case name(TextCellViewModel)
  case address(TextCellViewModel)
  case port(TextCellViewModel)
  case password(TextCellViewModel)

  var id: String {
    switch self {
    case let .name(viewModel):
      return viewModel.name
    case let .address(viewModel):
      return viewModel.name
    case let .port(viewModel):
      return viewModel.name
    case let .password(viewModel):
      return viewModel.name
    }
  }
}
