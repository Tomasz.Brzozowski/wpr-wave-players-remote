//
//  ServerDetailViewModel.swift
//  WPR - Wave Players Remote
//
//  Created by Tomasz Brzozowski on 04/05/2021.
//

import RxRelay
import RxSwift

class ServerDetailViewModel: ServerDetailViewModeling {
  let sections = BehaviorRelay<[SectionOfServerDetail]>(value: [])
  let showAlert = PublishSubject<String>()
  let dismiss = PublishSubject<Void>()

  private let log: Logger
  private let userDefaultsService: UserDefaultsProvider
  private let textValidationService: TextValidationProvider
  private let serverMpcService: ServerProvider
  private let serverVlcService: ServerProvider
  private let getServerNameDetailCellViewModel: (_ text: String) -> TextCellViewModel
  private let getServerAddressDetailCellViewModel: (_ text: String) -> TextCellViewModel
  private let getServerPortDetailCellViewModel: (_ text: String) -> TextCellViewModel
  private let getServerPasswordDetailCellViewModel: (_ text: String) -> TextCellViewModel
  private let editAddress: String?
  private let editPort: String?
  private let disposeBag = DisposeBag()

  init(
    log: Logger,
    userDefaultsService: UserDefaultsProvider,
    textValidationService: TextValidationProvider,
    serverMpcService: ServerProvider,
    serverVlcService: ServerProvider,
    getServerNameDetailCellViewModel: @escaping (String) -> TextCellViewModel,
    getServerAddressDetailCellViewModel: @escaping (String) -> TextCellViewModel,
    getServerPortDetailCellViewModel: @escaping (String) -> TextCellViewModel,
    getServerPasswordDetailCellViewModel: @escaping (String) -> TextCellViewModel,
    editAddress: String? = nil,
    editPort: String? = nil
  ) {
    self.log = log
    self.userDefaultsService = userDefaultsService
    self.textValidationService = textValidationService
    self.serverMpcService = serverMpcService
    self.serverVlcService = serverVlcService
    self.getServerNameDetailCellViewModel = getServerNameDetailCellViewModel
    self.getServerAddressDetailCellViewModel = getServerAddressDetailCellViewModel
    self.getServerPortDetailCellViewModel = getServerPortDetailCellViewModel
    self.getServerPasswordDetailCellViewModel = getServerPasswordDetailCellViewModel
    self.editAddress = editAddress
    self.editPort = editPort

    getServerService().getServerFromDatabase(address: editAddress ?? "", port: Int(editPort ?? "") ?? 1)
      .subscribe { [weak self] result in
        guard let self = self else { return }
        var detail = ServerDetail()
        switch result {
        case let .success(editServer):
          if let editServer = editServer {
            detail = ServerDetail(server: editServer)
          }
        case let .failure(error):
          self.log.error(error, #file, #function, #line)
        }
        self.sections.accept([
          SectionOfServerDetail(items: [
            .name(self.getServerNameDetailCellViewModel(detail.name)),
            .address(self.getServerAddressDetailCellViewModel(detail.address)),
            .port(self.getServerPortDetailCellViewModel(detail.port)),
            .password(self.getServerPasswordDetailCellViewModel(detail.password)),
          ]),
        ])
      }.disposed(by: disposeBag)
  }

  private func getServerService() -> ServerProvider {
    switch userDefaultsService.controlledPlayer() {
    case .mpc:
      return serverMpcService
    case .vlc:
      return serverVlcService
    }
  }

  func confirm() {
    if sections.value.isEmpty { return }
    let section = 0
    let _name = sections.value[section].items.first { $0.id == R.string.localizable.serverNameDetailName() }
    let _address = sections.value[section].items.first { $0.id == R.string.localizable.serverAddressDetailName() }
    let _port = sections.value[section].items.first { $0.id == R.string.localizable.serverPortDetailName() }
    let _password = sections.value[section].items.first { $0.id == R.string.localizable.serverPasswordDetailName() }

    guard
      let name = getDetailCellViewModelText(viewModel: _name),
      let address = getDetailCellViewModelText(viewModel: _address),
      let port = getDetailCellViewModelText(viewModel: _port),
      let password = getDetailCellViewModelText(viewModel: _password)
    else { return }

    if !textValidationService.validateIp(address) {
      showAlert.onNext(ServerError.addressValidation.message)
      return
    }

    if !textValidationService.validatePortText(port) {
      showAlert.onNext(ServerError.portValidation.message)
      return
    }

    guard let portNumber = Int(port) else {
      showAlert.onNext(ServerError.portValidation.message)
      return
    }

    let server = Server(name: name, address: address, port: portNumber, password: password)

    if let editAddress = editAddress, let editPort = editPort {
      guard let editPortNumber = Int(editPort) else {
        log.error(ServerError.portNumber.message, #file, #function, #line)
        return
      }
      updateServer(editAddress: editAddress, editPort: editPortNumber,
                   server: server)
    } else {
      addServer(server)
    }
  }

  private func getDetailCellViewModelText(viewModel: ServerDetailCellViewModelType?) -> String? {
    guard let viewModel = viewModel else { return nil }
    switch viewModel {
    case let .name(viewModel):
      return viewModel.text.value
    case let .address(viewModel):
      return viewModel.text.value
    case let .port(viewModel):
      return viewModel.text.value
    case let .password(viewModel):
      return viewModel.text.value
    }
  }

  private func updateServer(editAddress: String, editPort: Int, server: Server) {
    if editAddress != server.address || editPort != server.port {
      getServerService().deleteServer(address: editAddress, port: editPort)
        .subscribe { [weak self] event in
          self?.handleCompletableEvent(event)
        }.disposed(by: disposeBag)
    }

    getServerService().updateServer(server)
      .subscribe { [weak self] event in
        self?.handleCompletableEvent(event) {
          self?.dismiss.onNext(())
        }
      }.disposed(by: disposeBag)
  }

  private func handleCompletableEvent(
    _ completableEvent: CompletableEvent,
    onCompletedAction: @escaping () -> Void = {}
  ) {
    switch completableEvent {
    case .completed:
      onCompletedAction()
    case let .error(error):
      log.error(error, #file, #function, #line)
    }
  }

  private func addServer(_ server: Server) {
    getServerService().serverExists(server.address, server.port)
      .flatMapCompletable { [weak self] serverExists -> Completable in
        if !serverExists {
          return self?.getServerService().updateServer(server) ?? Completable.empty()
        } else {
          return Completable.error(ServerError.serverAlreadyExists)
        }
      }
      .subscribe { [weak self] event in
        self?.handleCompletableEvent(event) {
          self?.dismiss.onNext(())
        }
      }.disposed(by: disposeBag)
  }
}
