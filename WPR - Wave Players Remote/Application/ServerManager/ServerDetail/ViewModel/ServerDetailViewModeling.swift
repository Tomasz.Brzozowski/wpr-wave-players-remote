//
//  ServerDetailViewModeling.swift
//  WPR - Wave Players Remote
//
//  Created by Tomasz Brzozowski on 13/05/2021.
//

import RxRelay
import RxSwift

protocol ServerDetailViewModeling {
  var sections: BehaviorRelay<[SectionOfServerDetail]> { get }
  var showAlert: PublishSubject<String> { get }
  var dismiss: PublishSubject<Void> { get }

  func confirm()
}
