//
//  ServerPortDetailCellViewModel.swift
//  WPR - Wave Players Remote
//
//  Created by Tomasz Brzozowski on 29/06/2021.
//

class ServerPortDetailCellViewModel: ServerDetailCellViewModel, ServerPortCellViewModeling {
  init(text: String = "", textValidationService: TextValidationProvider) {
    super.init(
      name: R.string.localizable.serverPortDetailName(),
      text: text,
      textValidationService: textValidationService
    )
  }

  override func validateInputWhileTyping(_ text: String) {
    self.text.accept(validatedPortWhileTyping(text))
  }
}
