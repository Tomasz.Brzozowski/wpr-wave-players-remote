//
//  ServerAddressDetailCellViewModel.swift
//  WPR - Wave Players Remote
//
//  Created by Tomasz Brzozowski on 28/06/2021.
//

class ServerAddressDetailCellViewModel: ServerDetailCellViewModel, ServerAddressCellViewModel {
  init(text: String = "", textValidationService: TextValidationProvider) {
    super.init(
      name: R.string.localizable.serverAddressDetailName(),
      text: text,
      textValidationService: textValidationService
    )
  }

  override func validateInputWhileTyping(_ text: String) {
    self.text.accept(validatedIpWhileTyping(text))
  }
}
