//
//  ServerNameDetailCellViewModel.swift
//  WPR - Wave Players Remote
//
//  Created by Tomasz Brzozowski on 29/06/2021.
//

class ServerNameDetailCellViewModel: ServerDetailCellViewModel {
  init(text: String = "", textValidationService: TextValidationProvider) {
    super.init(
      name: R.string.localizable.serverNameDetailName(),
      text: text,
      textValidationService: textValidationService
    )
  }
}
