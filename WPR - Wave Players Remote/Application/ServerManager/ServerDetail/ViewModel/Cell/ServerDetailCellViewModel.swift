//
//  ServerDetailCellViewModel.swift
//  WPR - Wave Players Remote
//
//  Created by Tomasz Brzozowski on 04/05/2021.
//

import RxRelay

class ServerDetailCellViewModel: TextCellViewModel, ValidatingTextViewModel {
  let name: String
  let text = BehaviorRelay<String>(value: "")

  let textValidationService: TextValidationProvider

  init(name: String, text: String, textValidationService: TextValidationProvider) {
    self.name = name
    self.text.accept(text)
    self.textValidationService = textValidationService
  }

  func validateInputWhileTyping(_ text: String) {
    self.text.accept(text)
  }
}
