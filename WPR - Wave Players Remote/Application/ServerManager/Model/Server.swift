//
//  Server.swift
//  WPR - Wave Players Remote
//
//  Created by Tomasz Brzozowski on 22/02/2021.
//

struct Server {
  let name: String
  let address: String
  let port: Int
  let password: String
}
