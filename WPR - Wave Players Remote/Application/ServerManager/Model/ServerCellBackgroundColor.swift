//
//  ServerCellBackgroundColor.swift
//  WPR - Wave Players Remote
//
//  Created by Tomasz Brzozowski on 30/04/2021.
//

enum ServerCellBackgroundColor {
  case clear
  case mpc
  case vlc
}
