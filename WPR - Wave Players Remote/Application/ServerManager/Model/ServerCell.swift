//
//  ServerCell.swift
//  WPR - Wave Players Remote
//
//  Created by Tomasz Brzozowski on 30/04/2021.
//

struct ServerCell: Equatable {
  let name: String
  let address: String
  let port: String
  let password: String
  let isResponding: Bool
}

extension ServerCell {
  init(server: Server, isResponding: Bool) {
    name = server.name
    address = server.address
    port = String(server.port)
    password = server.password
    self.isResponding = isResponding
  }
}
