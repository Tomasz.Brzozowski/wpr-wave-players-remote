//
//  ServerError.swift
//  WPR - Wave Players Remote
//
//  Created by Tomasz Brzozowski on 10/05/2021.
//

enum ServerError: Error {
  case addressValidation
  case portValidation
  case portNumber
  case serverAlreadyExists

  var message: String {
    switch self {
    case .addressValidation: return R.string.localizable.serverAddressValidationError()
    case .portValidation: return R.string.localizable.serverPortValidationError()
    case .portNumber: return R.string.localizable.serverPortNumberError()
    case .serverAlreadyExists: return R.string.localizable.serverAlreadyExistsError()
    }
  }
}
