//
//  ServerManagerViewController.swift
//  WPR - Wave Players Remote
//
//  Created by Tomasz Brzozowski on 02/11/2020.
//

import RxSwift
import UIKit

class ServerManagerViewController: UIViewController {
  private let refreshBarButton: UIBarButtonItem = {
    let button = UIBarButtonItem(barButtonSystemItem: .refresh, target: self, action: nil)
    return button
  }()

  private let refreshButton: UIButton = {
    let button = UIButton()
    button.setImage(R.image.server_manager.icon_refresh(), for: .normal)
    return button
  }()

  private let addBarButton: UIBarButtonItem = {
    let button = UIBarButtonItem()
    button.image = R.image.server_manager.icon_add()
    return button
  }()

  private let tableView: UITableView = {
    let tableView = UITableView()
    tableView.backgroundColor = R.color.surface()
    return tableView
  }()

  private let viewModel: ServerManagerViewModeling
  private let getServerDetailVc: (_ navigationBarTitle: String, _ confirmButtonTitle: String,
                                  _ editAddress: String?, _ editPort: String?) -> ServerDetailViewController
  private let disposeBag = DisposeBag()

  init(
    viewModel: ServerManagerViewModeling,
    getServerDetailVc: @escaping (String, String, String?, String?) -> ServerDetailViewController
  ) {
    self.viewModel = viewModel
    self.getServerDetailVc = getServerDetailVc
    super.init(nibName: nil, bundle: nil)
  }

  @available(*, unavailable)
  required init?(coder _: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  override func viewDidLoad() {
    super.viewDidLoad()
    configureUi()
    bindViewModel()
    bindCellTapHandling()
  }

  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    viewModel.refreshServers()
  }

  private func configureUi() {
    definesPresentationContext = true

    navigationItem.title = R.string.localizable.serverManagerViewTitle()
    refreshBarButton.customView = refreshButton
    navigationItem.rightBarButtonItem = refreshBarButton
    navigationItem.leftBarButtonItem = addBarButton

    tableView.delegate = self
    tableView.register(ServerTableViewCell.self, forCellReuseIdentifier: "serverCell")
    tableView.tableFooterView = UIView()

    view.addSubview(tableView)
    view.setNeedsUpdateConstraints()
  }

  override func updateViewConstraints() {
    tableView.snp.makeConstraints { make in
      make.top.bottom.leading.trailing.equalTo(view.safeAreaLayoutGuide)
    }
    super.updateViewConstraints()
  }

  private func bindViewModel() {
    refreshButton.rx.tap
      .bind { [weak self] _ in
        self?.viewModel.refreshServers()
      }.disposed(by: disposeBag)

    addBarButton.rx.tap
      .bind { [weak self] _ in
        guard let self = self else { return }
        let addActionTitle = R.string.localizable.addActionTitle()
        let addServerViewTitle = R.string.localizable.addServerViewTitle()
        self.presentServerDetailVc(addServerViewTitle, addActionTitle)
      }.disposed(by: disposeBag)

    viewModel.cellViewModels
      .bind(to: tableView.rx.items) { tableView, index, element -> UITableViewCell in
        let indexPath = IndexPath(item: index, section: 0)
        guard let viewCell = tableView.dequeueReusableCell(withIdentifier: "serverCell", for: indexPath)
          as? ServerTableViewCell else {
          return UITableViewCell()
        }
        viewCell.viewModel = element
        return viewCell
      }.disposed(by: disposeBag)

    viewModel.onSwitchToRemote
      .bind { [weak self] _ in
        self?.tabBarController?.selectedIndex = 0
      }.disposed(by: disposeBag)

    viewModel.refreshInProgress
      .bind { [weak self] refreshInProgress in
        if refreshInProgress {
          self?.animateRefreshBarButton()
        }
      }.disposed(by: disposeBag)

    viewModel.showAlert
      .bind { [weak self] text in
        let alertVc = UIAlertController(message: text, preferredStyle: .alert)
        alertVc.addAction(UIAlertAction(title: R.string.localizable.alertActionTitle(), style: .default, handler: nil))
        self?.present(alertVc, animated: true, completion: nil)
      }.disposed(by: disposeBag)
  }

  private func presentServerDetailVc(
    _ serverViewTitle: String = R.string.localizable.editServerViewTitle(),
    _ actionTitle: String = R.string.localizable.editActionTitle(),
    _ serverAddress: String? = nil,
    _ serverPort: String? = nil
  ) {
    let serverDetailVc = getServerDetailVc(serverViewTitle, actionTitle, serverAddress, serverPort)
    let navController = UINavigationController(rootViewController: serverDetailVc)
    navController.presentationController?.delegate = self
    navController.modalPresentationStyle = .formSheet
    navigationController?.present(navController, animated: true, completion: nil)
  }

  private func animateRefreshBarButton() {
    UIView.animate(withDuration: 0.5, delay: 0.0, options: UIView.AnimationOptions.curveLinear, animations: {
      self.refreshBarButton.customView?.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi))
    }, completion: nil)
    UIView.animate(withDuration: 0.5, delay: 0.5, options: UIView.AnimationOptions.curveLinear, animations: {
      self.refreshBarButton.customView?.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi * 2))
    }, completion: { [weak self] _ in
      if self?.viewModel.refreshInProgress.value ?? false {
        self?.animateRefreshBarButton()
      }
    })
  }

  private func bindCellTapHandling() {
    tableView.rx.modelSelected(ServerCellViewModeling.self)
      .bind { [weak self] serverCellViewModel in
        self?.viewModel.goToRemote(with: serverCellViewModel)
        if let selectedRowIndexPath = self?.tableView.indexPathForSelectedRow {
          self?.tableView.deselectRow(at: selectedRowIndexPath, animated: true)
        }
      }.disposed(by: disposeBag)
  }
}

extension ServerManagerViewController: UITableViewDelegate {
  func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt index: IndexPath) ->
    UISwipeActionsConfiguration? {
    guard let cellViewModel = (tableView.cellForRow(at: index) as? ServerTableViewCell)?.viewModel else { return nil }

    let deleteActionTitle = R.string.localizable.deleteActionTitle()
    let delete = UIContextualAction(style: .destructive, title: deleteActionTitle) { [weak self] _, _, _ in
      self?.viewModel.deleteServer(cellViewModel)
      if let selectedRowIndexPath = self?.tableView.indexPathForSelectedRow {
        self?.tableView.deselectRow(at: selectedRowIndexPath, animated: true)
      }
      tableView.setEditing(false, animated: true)
    }
    delete.backgroundColor = UIColor.systemRed

    let editActionTitle = R.string.localizable.editActionTitle()
    let editServerViewTitle = R.string.localizable.editServerViewTitle()
    let saveActionTitle = R.string.localizable.saveActionTitle()
    let edit = UIContextualAction(style: .normal, title: editActionTitle) { [weak self] _, _, _ in
      guard let self = self else { return }
      self.presentServerDetailVc(editServerViewTitle, saveActionTitle, cellViewModel.address, cellViewModel.port)
      tableView.setEditing(false, animated: true)
    }
    edit.backgroundColor = UIColor.systemGreen

    return UISwipeActionsConfiguration(actions: [delete, edit])
  }
}

extension ServerManagerViewController: UIAdaptivePresentationControllerDelegate {
  public func presentationControllerDidDismiss(_: UIPresentationController) {
    viewModel.refreshServers()
  }
}
