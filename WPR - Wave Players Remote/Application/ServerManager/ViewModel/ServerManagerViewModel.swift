//
//  ServerManagerViewModel.swift
//  WPR - Wave Players Remote
//
//  Created by Tomasz Brzozowski on 22/02/2021.
//

import RxRelay
import RxSwift

class ServerManagerViewModel: ServerManagerViewModeling {
  let cellViewModels = BehaviorRelay<[ServerCellViewModeling]>(value: [])
  let refreshInProgress = BehaviorRelay<Bool>(value: false)
  let onSwitchToRemote = PublishSubject<Void>()
  let showAlert = PublishSubject<String>()

  private let log: Logger
  private let userDefaultsService: UserDefaultsProvider
  private let serverMpcService: ServerProvider
  private let serverVlcService: ServerProvider
  private let getServerCellViewModel: (ServerCell) -> ServerCellViewModeling
  private let disposeBag = DisposeBag()

  init(
    log: Logger,
    userDefaultsService: UserDefaultsProvider,
    serverMpcService: ServerProvider,
    serverVlcService: ServerProvider,
    getServerCellViewModel: @escaping (ServerCell) -> ServerCellViewModeling
  ) {
    self.log = log
    self.userDefaultsService = userDefaultsService
    self.serverMpcService = serverMpcService
    self.serverVlcService = serverVlcService
    self.getServerCellViewModel = getServerCellViewModel
  }

  func refreshServers() {
    log.info("Refreshing servers", #file, #function, #line)
    refreshInProgress.accept(true)
    clearServers()

    getServersFromDatabase().andThen(getRespondingLocalServers())
      .subscribe { [weak self] event in
        switch event {
        case .completed:
          break
        case let .error(error):
          self?.log.error(error, #file, #function, #line)
        }
        self?.refreshInProgress.accept(false)
      }.disposed(by: disposeBag)
  }

  private func clearServers() {
    cellViewModels.accept([])
  }

  private func getServersFromDatabase() -> Completable {
    getServerService().getServersFromDatabase()
      .flatMapCompletable { [weak self] servers -> Completable in
        for server in servers {
          guard let self = self else { return Completable.empty() }
          let cell = ServerCell(server: server, isResponding: false)
          let cellViewModel = self.getServerCellViewModel(cell)
          let indexOfDuplicate = self.cellViewModels.value.firstIndex(where: { $0 == cellViewModel })
          if indexOfDuplicate == nil {
            self.cellViewModels.accept(self.cellViewModels.value + [cellViewModel])
          }
        }
        return Completable.empty()
      }
  }

  private func getServerService() -> ServerProvider {
    switch userDefaultsService.controlledPlayer() {
    case .mpc:
      return serverMpcService
    case .vlc:
      return serverVlcService
    }
  }

  private func getRespondingLocalServers() -> Observable<Never> {
    let password = userDefaultsService.serverPassword()
    let port = userDefaultsService.serverPort()
    return getServerService().getRespondingLocalServers(port: port, password: password)
      .take(for: .seconds(3), scheduler: MainScheduler.instance)
      .flatMap { [weak self] server -> Completable in
        guard let self = self else { return Completable.empty() }
        let cellViewModel = self.getServerCellViewModel(ServerCell(server: server, isResponding: true))
        let duplicate = self.cellViewModels.value.first(where: { $0 == cellViewModel })
        let indexOfDuplicate = self.cellViewModels.value.firstIndex(where: { $0 == cellViewModel })
        if let duplicate = duplicate, let indexOfDuplicate = indexOfDuplicate {
          var cellsValue = self.cellViewModels.value
          let cell = ServerCell(name: duplicate.name, address: duplicate.address,
                                port: duplicate.port, password: password, isResponding: true)
          let cellViewModel = self.getServerCellViewModel(cell)
          cellsValue.replaceSubrange(indexOfDuplicate ... indexOfDuplicate, with: [cellViewModel])
          self.cellViewModels.accept(cellsValue)
        } else {
          self.cellViewModels.accept(self.cellViewModels.value + [cellViewModel])
        }
        return self.getServerService().updateServer(server)
      }
  }

  func deleteServer(_ serverCellViewModel: ServerCellViewModeling) {
    guard let port = Int(serverCellViewModel.port) else {
      log.error(ServerError.portValidation, #file, #function, #line)
      return
    }
    getServerService().deleteServer(address: serverCellViewModel.address, port: port)
      .subscribe { [weak self] event in
        guard let self = self else { return }
        switch event {
        case .completed:
          self.cellViewModels.accept(self.cellViewModels.value.filter { $0 != serverCellViewModel })
        case let .error(error):
          self.log.error(error, #file, #function, #line)
        }
      }.disposed(by: disposeBag)
  }

  func goToRemote(with serverCellViewModel: ServerCellViewModeling) {
    if serverCellViewModel.isResponding, let port = Int(serverCellViewModel.port) {
      userDefaultsService.setServerAddress(serverCellViewModel.address)
      userDefaultsService.setServerPort(port)
      userDefaultsService.setServerPassword(serverCellViewModel.password)
      onSwitchToRemote.onNext(())
    }
  }
}
