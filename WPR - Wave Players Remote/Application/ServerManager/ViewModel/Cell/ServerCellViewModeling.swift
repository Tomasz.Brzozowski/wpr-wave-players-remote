//
//  ServerCellViewModeling.swift
//  WPR - Wave Players Remote
//
//  Created by Tomasz Brzozowski on 13/05/2021.
//

import RxRelay

protocol ServerCellViewModeling: class {
  var name: String { get }
  var address: String { get }
  var port: String { get }
  var password: String { get }
  var isResponding: Bool { get }
  var backgroundColor: BehaviorRelay<ServerCellBackgroundColor> { get }
}

func == (lhs: ServerCellViewModeling, rhs: ServerCellViewModeling) -> Bool {
  return lhs.address == rhs.address && lhs.port == rhs.port
}

func != (lhs: ServerCellViewModeling, rhs: ServerCellViewModeling) -> Bool {
  return lhs.address != rhs.address || lhs.port != rhs.port
}
