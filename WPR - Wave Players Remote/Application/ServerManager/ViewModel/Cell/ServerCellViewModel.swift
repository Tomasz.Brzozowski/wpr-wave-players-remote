//
//  ServerCellViewModel.swift
//  WPR - Wave Players Remote
//
//  Created by Tomasz Brzozowski on 30/04/2021.
//

import RxRelay
import RxSwift

class ServerCellViewModel: ServerCellViewModeling {
  let name: String
  let address: String
  let port: String
  let password: String
  let isResponding: Bool
  let backgroundColor = BehaviorRelay<ServerCellBackgroundColor>(value: .clear)

  private let disposeBag = DisposeBag()

  init(userDefaultsService: UserDefaultsProvider, cell: ServerCell) {
    name = cell.name
    address = cell.address
    port = String(cell.port)
    password = cell.password
    isResponding = cell.isResponding

    Observable.combineLatest(
      userDefaultsService.serverAddressObservable(),
      userDefaultsService.serverPortObservable(),
      userDefaultsService.serverPasswordObservable(),
      userDefaultsService.controlledPlayerObservable()
    )
    .bind { [weak self] address, port, password, controlledPlayer in
      guard let self = self else { return }
      let isSet = self.isSet(address: address, port: port, password: password)
      switch (isSet, controlledPlayer) {
      case (true, .mpc):
        self.backgroundColor.accept(.mpc)
      case (true, .vlc):
        self.backgroundColor.accept(.vlc)
      default:
        self.backgroundColor.accept(.clear)
      }
    }.disposed(by: disposeBag)
  }

  private func isSet(address: String, port: Int, password: String) -> Bool {
    return self.address == address && self.port == String(port) && self.password == password
  }
}
