//
//  ServerManagerViewModeling.swift
//  WPR - Wave Players Remote
//
//  Created by Tomasz Brzozowski on 13/05/2021.
//

import RxRelay
import RxSwift

protocol ServerManagerViewModeling {
  var cellViewModels: BehaviorRelay<[ServerCellViewModeling]> { get }
  var refreshInProgress: BehaviorRelay<Bool> { get }
  var onSwitchToRemote: PublishSubject<Void> { get }
  var showAlert: PublishSubject<String> { get }

  func refreshServers()
  func deleteServer(_ serverCellViewModel: ServerCellViewModeling)
  func goToRemote(with serverCellViewModel: ServerCellViewModeling)
}
