//
//  ServerProvider.swift
//  WPR - Wave Players Remote
//
//  Created by Tomasz Brzozowski on 22/02/2021.
//

import RealmSwift
import RxSwift

protocol ServerProvider {
  func getServersFromDatabase() -> Single<[Server]>
  func getServerFromDatabase(address: String, port: Int) -> Single<Server?>
  func deleteServer(address: String, port: Int) -> Completable
  func serverExists(_ address: String, _ port: Int) -> Single<Bool>
  func updateServer(_ server: Server) -> Completable
  func getRespondingLocalServers(port: Int, password: String) -> Observable<Server>
}

extension ServerProvider {
  func getServersFromDatabase() -> Single<[Server]> {
    return getServersDataFromDatabase()
      .map { serversData -> [Server] in
        serversData.map { serverData -> Server in
          Server(name: serverData.name, address: serverData.address,
                 port: Int(serverData.port), password: serverData.password)
        }
      }
  }

  private func getServersDataFromDatabase() -> Single<Results<ServerData>> {
    return Single<Results<ServerData>>.create { result -> Disposable in
      do {
        let realm = try Realm()
        result(.success(realm.objects(ServerData.self)))
      } catch {
        result(.failure(error))
      }
      return Disposables.create()
    }
  }

  func getServerFromDatabase(address: String, port: Int) -> Single<Server?> {
    return getServerDataFromDatabase(address: address, port: port)
      .map { serverData -> Server? in
        if let serverData = serverData {
          return Server(name: serverData.name, address: serverData.address,
                        port: Int(serverData.port), password: serverData.password)
        } else {
          return nil
        }
      }
  }

  private func getServerDataFromDatabase(address: String, port: Int) -> Single<ServerData?> {
    return Single<ServerData?>.create { result -> Disposable in
      do {
        let realm = try Realm()
        let serverData = ServerData()
        serverData.set(address: address, port: port)
        let primaryKey = serverData.value(forKey: ServerData.primaryKey() ?? "")
        result(.success(realm.object(ofType: ServerData.self, forPrimaryKey: primaryKey)))
      } catch {
        result(.failure(error))
      }
      return Disposables.create()
    }
  }

  func deleteServer(address: String, port: Int) -> Completable {
    return getServerDataFromDatabase(address: address, port: port)
      .flatMapCompletable { serverData -> Completable in
        do {
          let realm = try Realm()
          try realm.write {
            if let serverData = serverData {
              realm.delete(serverData)
            }
          }
          return Completable.empty()
        } catch {
          return Completable.error(error)
        }
      }
  }

  func serverExists(_ address: String, _ port: Int) -> Single<Bool> {
    return getServerDataFromDatabase(address: address, port: port)
      .map { serverData -> Bool in
        serverData != nil
      }
  }

  func updateServer(_ server: Server) -> Completable {
    return Completable.create { event -> Disposable in
      do {
        let realm = try Realm()
        let serverData = ServerData()
        serverData.set(address: server.address, port: server.port, name: server.name, password: server.password)
        try realm.write {
          realm.add(serverData, update: .modified)
        }
        event(.completed)
      } catch {
        event(.error(error))
      }
      return Disposables.create()
    }
  }
}

protocol ExtendedServerProvider: ServerProvider {
  var defaultPort: Int { get }

  func getServerResponse(port: Int, address: String, password: String) -> Single<Void>
}

extension ExtendedServerProvider {
  func getRespondingLocalServers(port: Int, password: String = "") -> Observable<Server> {
    var serverObservables: [Observable<Server>] = []
    for ip4 in 0 ... 255 {
      let address = "192.168.0." + String(ip4)
      let defaultLocalServerResponse = serverResponse(port: defaultPort, address: address, password: password)
      serverObservables.append(defaultLocalServerResponse)
      if port != defaultPort {
        let localServerResponse = serverResponse(port: port, address: address, password: password)
        serverObservables.append(localServerResponse)
      }
    }
    let serverObservable = Observable<Server>.merge(serverObservables)
    return serverObservable
  }

  private func serverResponse(port: Int, address: String, password: String) -> Observable<Server> {
    getServerResponse(port: port, address: address, password: password).asObservable()
      .map { _ -> Server in
        Server(name: R.string.localizable.serverDefaultName(), address: address, port: port, password: password)
      }.catch { _ in
        Observable.never()
      }
  }
}
