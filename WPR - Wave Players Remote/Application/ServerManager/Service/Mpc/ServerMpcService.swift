//
//  ServerMpcService.swift
//  WPR - Wave Players Remote
//
//  Created by Tomasz Brzozowski on 22/02/2021.
//

import RxSwift

class ServerMpcService: ExtendedServerProvider {
  let defaultPort = 13579

  private let httpMpcClient: HttpMediaPlayerProvider

  init(httpMpcClient: HttpMediaPlayerProvider) {
    self.httpMpcClient = httpMpcClient
  }

  func getServerResponse(port: Int, address: String, password: String) -> Single<Void> {
    httpMpcClient.getInfo(address, port, password).map { _ in }
  }
}
