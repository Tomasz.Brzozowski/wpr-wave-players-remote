//
//  ServerData.swift
//  WPR - Wave Players Remote
//
//  Created by Tomasz Brzozowski on 04/05/2021.
//

import RealmSwift

public final class ServerData: Object {
  @objc dynamic var name = ""
  @objc dynamic var address = "0.0.0.0"
  @objc dynamic var port = 1
  @objc dynamic var password = ""
  @objc public dynamic var compoundKey = "0.0.0.0:1"

  override public static func primaryKey() -> String? {
    return "compoundKey"
  }

  public func setCompoundAddress(_ address: String) {
    self.address = address
    compoundKey = compoundKeyValue()
  }

  private func compoundKeyValue() -> String {
    return "\(address):\(port)"
  }

  public func setCompoundPort(_ port: Int) {
    self.port = port
    compoundKey = compoundKeyValue()
  }

  public func set(address: String, port: Int, name: String = "", password: String = "") {
    self.name = name
    setCompoundAddress(address)
    setCompoundPort(port)
    self.password = password
  }
}
