//
//  ServerVlcService.swift
//  WPR - Wave Players Remote
//
//  Created by Tomasz Brzozowski on 23/02/2021.
//

import RxSwift

class ServerVlcService: ExtendedServerProvider {
  let defaultPort = 8080

  private let httpVlcClient: HttpMediaPlayerProvider

  init(httpVlcClient: HttpMediaPlayerProvider) {
    self.httpVlcClient = httpVlcClient
  }

  func getServerResponse(port: Int, address: String, password: String) -> Single<Void> {
    return httpVlcClient.getStatusJson(address, port, password).map { _ in }
  }
}
