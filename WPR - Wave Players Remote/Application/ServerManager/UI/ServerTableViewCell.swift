//
//  ServerTableViewCell.swift
//  WPR - Wave Players Remote
//
//  Created by Tomasz Brzozowski on 22/02/2021.
//

import RxSwift
import UIKit

class ServerTableViewCell: UITableViewCell {
  var viewModel: ServerCellViewModeling? {
    didSet {
      bindViewModel()
    }
  }

  private let computerImageView = UIImageView()
  private let nameLabel = UILabel(font: .wprFont(size: 19))
  private let addressLabel = UILabel(font: .wprFont())
  private let isRespondingImageView = UIImageView()

  private let disposeBag = DisposeBag()

  private func bindViewModel() {
    backgroundColor = R.color.surface()

    if let viewModel = viewModel {
      nameLabel.text = "\(viewModel.name)"
      addressLabel.text = "\(viewModel.address):\(viewModel.port)"
      if viewModel.isResponding {
        isRespondingImageView.image = R.image.server_manager.icon_server_responding()
        computerImageView.image = R.image.server_manager.icon_computer_responding()
        nameLabel.textColor = R.color.onSurface()
        addressLabel.textColor = R.color.onSurface()
      } else {
        isRespondingImageView.image = R.image.server_manager.icon_server_not_responding()
        computerImageView.image = R.image.server_manager.icon_computer_not_responding()
        nameLabel.textColor = R.color.onSurfaceDark()
        addressLabel.textColor = R.color.onSurfaceDark()
      }

      viewModel.backgroundColor
        .observe(on: MainScheduler.instance)
        .bind { [weak self] color in
          switch color {
          case .clear:
            self?.backgroundView = UIView(backgroundColor: R.color.surface())
          case .mpc:
            self?.backgroundView = UIView(backgroundColor: R.color.primaryLightSemiTransparentMpc())
          case .vlc:
            self?.backgroundView = UIView(backgroundColor: R.color.primaryLightSemiTransparentVlc())
          }
        }.disposed(by: disposeBag)
    }
  }

  override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
    super.init(style: style, reuseIdentifier: reuseIdentifier)
    configureUi()
  }

  private func configureUi() {
    contentView.addSubview(computerImageView)
    contentView.addSubview(nameLabel)
    contentView.addSubview(addressLabel)
    contentView.addSubview(isRespondingImageView)
    setNeedsUpdateConstraints()
  }

  override func updateConstraints() {
    let labelHeight = 16
    computerImageView.snp.makeConstraints { make in
      make.leading.equalTo(contentView.safeAreaLayoutGuide).inset(Dimension.viewMarginPaddingNormal)
      make.centerY.equalTo(contentView.safeAreaLayoutGuide)
    }
    nameLabel.snp.makeConstraints { make in
      make.top.equalTo(contentView.safeAreaLayoutGuide).inset(Dimension.viewMarginPaddingNormal)
      make.leading.equalTo(computerImageView.snp.trailing).inset(-Dimension.viewMarginPaddingNormal)
      make.height.equalTo(labelHeight)
    }
    addressLabel.snp.makeConstraints { make in
      make.top.equalTo(nameLabel.snp.bottom).inset(-Dimension.viewMarginPaddingNormal)
      make.bottom.equalTo(contentView.safeAreaLayoutGuide).inset(Dimension.viewMarginPaddingNormal)
      make.leading.equalTo(nameLabel.snp.leading)
      make.height.equalTo(labelHeight)
    }
    isRespondingImageView.snp.makeConstraints { make in
      make.centerY.equalTo(addressLabel)
      make.leading.equalTo(addressLabel.snp.trailing).inset(-Dimension.viewMarginPaddingNormal)
    }
    super.updateConstraints()
  }

  @available(*, unavailable)
  required init?(coder _: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
}
