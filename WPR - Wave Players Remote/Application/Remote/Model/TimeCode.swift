//
//  TimeCode.swift
//  WPR - Wave Players Remote
//
//  Created by Tomasz Brzozowski on 09/12/2020.
//

class TimeCode {
  static let start = TimeCode(totalSeconds: 0)

  private var hours: Int
  private var minutes: Int
  private var seconds: Int
  private var totalSeconds: Int

  convenience init(totalSeconds seconds: Int) {
    let hours = seconds / 3600
    var seconds = seconds % 3600
    let minutes = seconds / 60
    seconds = seconds % 60
    self.init(clampingHours: hours, clampingMinutes: minutes, clampingSeconds: seconds)
  }

  convenience init?(timeCode: String) {
    let times = timeCode.split(separator: ":")
    if times.count != 3 {
      return nil
    }
    guard let hours = Int(times[0]), let minutes = Int(times[1]), let seconds = Int(times[2]) else {
      return nil
    }
    self.init(hours: hours, minutes: minutes, seconds: seconds)
  }

  init?(hours: Int, minutes: Int, seconds: Int) {
    if minutes >= 60 || seconds >= 60 {
      return nil
    } else if hours < 0 || minutes < 0 || seconds < 0 {
      return nil
    }
    self.hours = hours
    self.minutes = minutes
    self.seconds = seconds
    totalSeconds = TimeCode.timeComponentsToSeconds(hours, minutes, seconds)
  }

  private static func timeComponentsToSeconds(_ hours: Int, _ minutes: Int, _ seconds: Int) -> Int {
    return (hours * 3600) + (minutes * 60) + seconds
  }

  init(clampingHours hours: Int, clampingMinutes minutes: Int, clampingSeconds seconds: Int) {
    let timeRange = 0 ... 59
    self.hours = hours < 0 ? 0 : hours
    self.minutes = minutes.clamped(to: timeRange)
    self.seconds = seconds.clamped(to: timeRange)
    totalSeconds = TimeCode.timeComponentsToSeconds(hours, minutes, seconds)
  }

  static func plus(augend: TimeCode, addend: TimeCode) -> TimeCode {
    return TimeCode(totalSeconds: augend.totalSeconds + addend.totalSeconds)
  }

  static func minus(minuend: TimeCode, subtrahend: TimeCode) -> TimeCode {
    return TimeCode(totalSeconds: minuend.totalSeconds - subtrahend.totalSeconds)
  }

  static func getTotalMiliseconds(timeCode: String) -> Int? {
    let components = timeCode.split(separator: ".")
    if components.count != 2 { return nil }
    guard let timeCode = TimeCode(timeCode: String(components[0])) else { return nil }
    let miliseconds = String(components[1])
    let startIndex = miliseconds.startIndex
    let endIndex = miliseconds.index(startIndex, offsetBy: 2)
    guard let trailingMiliseconds = Int(miliseconds[startIndex ... endIndex]) else { return nil }
    return timeCode.totalSeconds * 1000 + trailingMiliseconds
  }

  func getHours() -> Int {
    return hours
  }

  func getMinutes() -> Int {
    return minutes
  }

  func getSeconds() -> Int {
    return seconds
  }

  func getTotalSeconds() -> Int {
    return totalSeconds
  }

  func compareTo(other: TimeCode) -> Int {
    return (totalSeconds < other.totalSeconds) ? -1 : ((totalSeconds == other.totalSeconds) ? 0 : 1)
  }

  func hashCode() -> Int {
    return totalSeconds
  }

  public func toString() -> String {
    let hoursString = timeComponentToString(component: hours)
    let minutesString = timeComponentToString(component: minutes)
    let secondsString = timeComponentToString(component: seconds)
    return hoursString + ":" + minutesString + ":" + secondsString
  }

  private func timeComponentToString(component: Int) -> String {
    return component < 10 ? String("0\(component)") : String(component)
  }
}
