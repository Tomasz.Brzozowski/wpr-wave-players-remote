//
//  ImageViewContentMode.swift
//  WPR - Wave Players Remote
//
//  Created by Tomasz Brzozowski on 28/05/2021.
//

enum ImageViewContentMode {
  case scaleToFill
  case scaleAspectFit
}
