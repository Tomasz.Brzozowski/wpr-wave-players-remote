//
//  VlcVariableKey.swift
//  WPR - Wave Players Remote
//
//  Created by Tomasz Brzozowski on 23/04/2021.
//

enum VlcVariableKey {
  static let fileName = ["information", "category", "meta", "filename"]
  static let state = "state"
  static let length = "length"
  static let positionRatio = "position"
  static let positionSeconds = "time"
  static let durationString = ["information", "category", "meta", "DURATION"]
  static let volumeLevel = "volume"
}
