//
//  MediaPlayerState.swift
//  WPR - Wave Players Remote
//
//  Created by Tomasz Brzozowski on 02/02/2021.
//

enum MediaPlayerState: String {
  case stopped = "0"
  case paused = "1"
  case playing = "2"
}
