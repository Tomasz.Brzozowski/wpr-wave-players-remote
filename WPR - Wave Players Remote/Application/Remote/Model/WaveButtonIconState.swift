//
//  WaveButtonIconState.swift
//  WPR - Wave Players Remote
//
//  Created by Tomasz Brzozowski on 15/02/2021.
//

enum WaveButtonIconState {
  case active
  case inactive
}
