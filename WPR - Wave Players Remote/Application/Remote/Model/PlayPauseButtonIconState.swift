//
//  PlayPauseIconState.swift
//  WPR - Wave Players Remote
//
//  Created by Tomasz Brzozowski on 26/01/2021.
//

enum PlayPauseButtonIconState {
  case pause
  case play
}
