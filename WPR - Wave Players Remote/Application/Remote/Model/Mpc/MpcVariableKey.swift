//
//  MpcVariablesKeys.swift
//  WPR - Wave Players Remote
//
//  Created by Tomasz Brzozowski on 29/01/2021.
//

enum MpcVariableKey {
  static let fileName = "file"
  static let filePath = "filepath"
  static let state = "state"
  static let position = "position"
  static let positionString = "positionstring"
  static let duration = "duration"
  static let durationString = "durationstring"
  static let volumeLevel = "volumelevel"

  static func all() -> [String] {
    return [
      MpcVariableKey.fileName,
      MpcVariableKey.filePath,
      MpcVariableKey.state,
      MpcVariableKey.position,
      MpcVariableKey.positionString,
      MpcVariableKey.duration,
      MpcVariableKey.durationString,
      MpcVariableKey.volumeLevel,
    ]
  }
}
