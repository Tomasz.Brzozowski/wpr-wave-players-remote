//
//  MediaVariables.swift
//  WPR - Wave Players Remote
//
//  Created by Tomasz Brzozowski on 28/01/2021.
//

struct MediaPlayerVariables {
  let fileName: String
  let filePath: String
  let state: String
  let position: String
  let positionString: String
  let duration: String
  let durationString: String
  let volumeLevel: String
}

extension MediaPlayerVariables {
  init(mpcVariables: [String: String]) {
    fileName = mpcVariables[MpcVariableKey.fileName] ?? ""
    filePath = mpcVariables[MpcVariableKey.filePath] ?? ""
    state = mpcVariables[MpcVariableKey.state] ?? ""
    position = mpcVariables[MpcVariableKey.position] ?? ""
    positionString = mpcVariables[MpcVariableKey.positionString] ?? ""
    duration = mpcVariables[MpcVariableKey.duration] ?? ""
    durationString = mpcVariables[MpcVariableKey.durationString] ?? ""
    volumeLevel = mpcVariables[MpcVariableKey.volumeLevel] ?? ""
  }
}
