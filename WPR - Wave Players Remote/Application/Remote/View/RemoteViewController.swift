//
//  RemoteViewController.swift
//  WPR - Wave Players Remote
//
//  Created by Tomasz Brzozowski on 28/10/2020.
//

import RxCocoa
import RxSwift
import SnapKit
import UIKit

class RemoteViewController: UIViewController {
  override var prefersStatusBarHidden: Bool {
    return true
  }

  private let volumeSlider: UISlider = {
    UISlider(minimumValueImage: R.image.remote.icon_volume_down(), maximumValueImage: R.image.remote.icon_volume_up())
  }()

  private let mediaImageView = UIImageView(backgroundColor: R.color.surface())

  private let topHorizontalStackView = UIStackView(axis: .horizontal, distribution: .fillProportionally)
  private let midHorizontalStackView = UIStackView(axis: .horizontal)
  private let bottomHorizontalStackView = UIStackView(axis: .horizontal)

  private let mediaProgressSlider = UISlider(maximumTrackTintColor: R.color.greyedOut())

  private let fastBackwardButton: UIButton = {
    UIButton(image: R.image.remote.icon_fast_backward(), text: R.string.localizable.fastBackwardButtonTitle())
  }()

  private let mediaCurrentPositionLabel: UILabel = {
    UILabel(textColor: R.color.onBackground(), text: R.string.localizable.mediaDefaultPosition(), textAlignment: .right)
  }()

  private let mediaPositionSlashLabel: UILabel = {
    UILabel(textColor: R.color.onBackground(), text: R.string.localizable.mediaSlashLabel(), textAlignment: .center)
  }()

  private let mediaEndPositionLabel: UILabel = {
    UILabel(textColor: R.color.onBackground(), text: R.string.localizable.mediaDefaultPosition(), textAlignment: .left)
  }()

  private let fastForwardButton: UIButton = {
    UIButton(image: R.image.remote.icon_fast_forward(), text: R.string.localizable.fastForwardButtonTitle())
  }()

  private let skipPreviousButton: UIButton = {
    UIButton(image: R.image.remote.icon_skip_previous(), text: R.string.localizable.skipPreviousButtonTitle())
  }()

  private let waveButton: UIButton = {
    UIButton(image: R.image.remote.icon_wave_inactive(), text: R.string.localizable.waveButtonTitle())
  }()

  private let playPauseButton: UIButton = {
    UIButton(image: R.image.remote.icon_play(), text: R.string.localizable.playPauseButtonTitle())
  }()

  private let subtitlesButton: UIButton = {
    UIButton(image: R.image.remote.icon_subtitles(), text: R.string.localizable.subtitlesButtonTitle())
  }()

  private let skipNextButton: UIButton = {
    UIButton(image: R.image.remote.icon_skip_next(), text: R.string.localizable.skipNextButtonTitle())
  }()

  private let fullScreenButton: UIButton = {
    UIButton(image: R.image.remote.icon_full_screen(), text: R.string.localizable.fullScreenButtonTitle())
  }()

  private let audioTrackButton: UIButton = {
    UIButton(image: R.image.remote.icon_audio_track(), text: R.string.localizable.audioTrackButtonTitle())
  }()

  private let repeatButton: UIButton = {
    UIButton(image: R.image.remote.icon_repeat(), text: R.string.localizable.repeatButtonTitle())
  }()

  private let dimView = UIView(frame: UIScreen.main.bounds, backgroundColor: .init(white: 0, alpha: 0.9))

  private let tapGestureRecognizer: UITapGestureRecognizer = {
    let recognizer = UITapGestureRecognizer()
    recognizer.cancelsTouchesInView = false
    return recognizer
  }()

  private let viewModel: RemoteViewModeling
  private let disposeBag = DisposeBag()

  init(viewModel: RemoteViewModeling) {
    self.viewModel = viewModel
    super.init(nibName: nil, bundle: nil)
  }

  @available(*, unavailable)
  required init?(coder _: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  override func viewDidLoad() {
    super.viewDidLoad()
    configureUi()
    bindViewModel()
  }

  override func viewWillAppear(_: Bool) {
    super.viewWillAppear(true)
    prepareViewToAppear()
  }

  private func prepareViewToAppear() {
    UIApplication.shared.isIdleTimerDisabled = true
    viewModel.restartTimers()
  }

  override func viewWillDisappear(_: Bool) {
    super.viewWillDisappear(true)
    prepareViewToDisappear()
  }

  private func prepareViewToDisappear() {
    UIApplication.shared.isIdleTimerDisabled = false
    dimView.removeFromSuperview()
    viewModel.stopTimers()
  }

  private func configureUi() {
    definesPresentationContext = true

    navigationItem.title = R.string.localizable.remoteViewTitle()
    navigationItem.hidesBackButton = true

    topHorizontalStackView.addArrangedSubview(fastBackwardButton)
    topHorizontalStackView.addArrangedSubview(mediaCurrentPositionLabel)
    topHorizontalStackView.addArrangedSubview(mediaPositionSlashLabel)
    topHorizontalStackView.addArrangedSubview(mediaEndPositionLabel)
    topHorizontalStackView.addArrangedSubview(fastForwardButton)

    midHorizontalStackView.addArrangedSubview(skipPreviousButton)
    midHorizontalStackView.addArrangedSubview(waveButton)
    midHorizontalStackView.addArrangedSubview(playPauseButton)
    midHorizontalStackView.addArrangedSubview(subtitlesButton)
    midHorizontalStackView.addArrangedSubview(skipNextButton)

    bottomHorizontalStackView.addArrangedSubview(fullScreenButton)
    bottomHorizontalStackView.addArrangedSubview(audioTrackButton)
    bottomHorizontalStackView.addArrangedSubview(repeatButton)

    view.backgroundColor = R.color.background()
    view.addSubview(volumeSlider)
    view.addSubview(mediaImageView)
    view.addSubview(topHorizontalStackView)
    view.addSubview(mediaProgressSlider)
    view.addSubview(midHorizontalStackView)
    view.addSubview(bottomHorizontalStackView)
    view.addGestureRecognizer(tapGestureRecognizer)
    view.setNeedsUpdateConstraints()
  }

  override func updateViewConstraints() {
    let iconHeight = 48

    volumeSlider.snp.makeConstraints { make in
      make.centerX.equalTo(view)
      make.top.equalTo(view.safeAreaLayoutGuide).inset(Dimension.viewMarginPaddingNormal)
      make.leading.trailing.equalTo(view.safeAreaLayoutGuide).inset(Dimension.viewMarginPaddingLarge)
      make.height.equalTo(36)
      make.bottom.equalTo(mediaImageView.snp.top).offset(-Dimension.viewMarginPaddingNormal)
    }

    mediaImageView.snp.makeConstraints { make in
      make.centerX.equalTo(view)
      make.leading.trailing.equalTo(view)
      make.bottom.equalTo(topHorizontalStackView.snp.top).offset(-Dimension.viewMarginPaddingNormal)
    }

    topHorizontalStackView.snp.makeConstraints { make in
      make.centerX.equalTo(view)
      make.leading.trailing.equalTo(view).inset(Dimension.viewMarginPaddingNormal)
      make.height.equalTo(iconHeight)
      make.bottom.equalTo(mediaProgressSlider.snp.top).offset(-Dimension.viewMarginPaddingNormal)
    }

    mediaProgressSlider.snp.makeConstraints { make in
      make.centerX.equalTo(view)
      make.leading.trailing.equalTo(view).inset(Dimension.viewMarginPaddingLarge)
      make.height.equalTo(iconHeight)
      make.bottom.equalTo(midHorizontalStackView.snp.top).offset(-Dimension.viewMarginPaddingLarge)
    }

    midHorizontalStackView.snp.makeConstraints { make in
      make.centerX.equalTo(view)
      make.leading.trailing.equalTo(topHorizontalStackView)
      make.height.equalTo(iconHeight)
      make.bottom.equalTo(bottomHorizontalStackView.snp.top).offset(-Dimension.viewMarginPaddingLarge)
    }

    bottomHorizontalStackView.snp.makeConstraints { make in
      make.centerX.equalTo(view)
      make.leading.trailing.equalTo(topHorizontalStackView)
      make.height.equalTo(iconHeight)
      make.bottom.equalTo(view.safeAreaLayoutGuide).inset(Dimension.viewMarginPaddingNormal)
    }

    super.updateViewConstraints()
  }

  private func bindViewModel() {
    bindUserNotifications()
    bindControls()
    bindRelays()
    bindSubjects()
    bindGestureRecognizer()
  }

  private func bindUserNotifications() {
    viewModel.didBecomeActiveNotification
      .observe(on: MainScheduler.instance)
      .bind { [weak self] in
        if self?.selectedAsViewController() == true {
          self?.prepareViewToAppear()
        }
      }.disposed(by: disposeBag)

    viewModel.willResignActiveNotification
      .observe(on: MainScheduler.instance)
      .bind { [weak self] in
        if self?.selectedAsViewController() == true {
          self?.prepareViewToDisappear()
        }
      }.disposed(by: disposeBag)
  }

  private func selectedAsViewController() -> Bool {
    if ((tabBarController?.selectedViewController as? UINavigationController)?.topViewController
      as? RemoteViewController) != nil {
      return true
    }
    return false
  }

  private func bindControls() {
    skipPreviousButton.rx.tap
      .bind { [weak self] in
        self?.viewModel.previousFile()
      }.disposed(by: disposeBag)

    waveButton.rx.tap
      .bind { [weak self] in
        self?.viewModel.toggleProximityIcon()
      }.disposed(by: disposeBag)

    playPauseButton.rx.tap
      .bind { [weak self] in
        self?.viewModel.togglePlayPause()
      }.disposed(by: disposeBag)

    subtitlesButton.rx.tap
      .bind { [weak self] in
        self?.viewModel.nextSubtitle()
      }.disposed(by: disposeBag)

    skipNextButton.rx.tap
      .bind { [weak self] in
        self?.viewModel.nextFile()
      }.disposed(by: disposeBag)

    fullScreenButton.rx.tap
      .bind { [weak self] in
        self?.viewModel.toggleFullscreen()
      }.disposed(by: disposeBag)

    audioTrackButton.rx.tap
      .bind { [weak self] in
        self?.viewModel.nextAudioTrack()
      }.disposed(by: disposeBag)

    repeatButton.rx.tap
      .bind { [weak self] in
        self?.viewModel.toggleRepeatMode()
      }.disposed(by: disposeBag)

    scheduleButtonTouchDownEvent(of: fastForwardButton, to: viewModel.fastForward)
    scheduleButtonTouchDownEvent(of: fastBackwardButton, to: viewModel.fastBackward)
    bindSliderValue(of: volumeSlider, to: viewModel.setVolume)
    bindSliderValue(of: mediaProgressSlider, to: viewModel.seek)
    bindSliderTouchEvents(of: mediaProgressSlider)
  }

  private func scheduleButtonTouchDownEvent(of button: UIButton, to action: @escaping () -> Void) {
    button.rx.controlEvent(.touchDown)
      .flatMap { _ in
        Observable<Int>.timer(.seconds(0), period: .milliseconds(100), scheduler: MainScheduler.instance)
          .take(until: button.rx.controlEvent([.touchUpInside, .touchDragOutside]))
      }
      .bind { _ in
        action()
      }.disposed(by: disposeBag)
  }

  private func bindSliderValue(of slider: UISlider, to action: @escaping (Float) -> Void) {
    slider.rx.value
      .skip(1)
      .throttle(.milliseconds(50), scheduler: MainScheduler.instance)
      .bind { value in
        action(value)
      }.disposed(by: disposeBag)
  }

  private func bindSliderTouchEvents(of slider: UISlider) {
    slider.rx.controlEvent(.touchDown)
      .bind { [weak self] _ in
        self?.viewModel.stopFetchVariablesTimer()
      }.disposed(by: disposeBag)

    slider.rx.controlEvent([.touchUpInside, .touchUpOutside])
      .bind { [weak self] _ in
        self?.viewModel.restartFetchVariablesTimer()
      }.disposed(by: disposeBag)
  }

  private func bindRelays() {
    viewModel.waveIconState
      .observe(on: MainScheduler.instance)
      .bind { [weak self] waveButtonIconState in
        switch waveButtonIconState {
        case .active:
          self?.waveButton.setImage(R.image.remote.icon_wave(), for: .normal)
        case .inactive:
          self?.waveButton.setImage(R.image.remote.icon_wave_inactive(), for: .normal)
        }
      }.disposed(by: disposeBag)

    viewModel.playPauseIconState
      .observe(on: MainScheduler.instance)
      .bind { [weak self] playPauseButtonIconState in
        switch playPauseButtonIconState {
        case .play:
          self?.playPauseButton.setImage(R.image.remote.icon_play(), for: .normal)
        case .pause:
          self?.playPauseButton.setImage(R.image.remote.icon_pause(), for: .normal)
        }
      }.disposed(by: disposeBag)

    bindRelayToSlider(relay: viewModel.volumeSliderPosition, to: volumeSlider)
    bindRelayToSlider(relay: viewModel.mediaProgressSliderPosition, to: mediaProgressSlider)
    bindRelayToLabel(relay: viewModel.mediaCurrentPosition, to: mediaCurrentPositionLabel)
    bindRelayToLabel(relay: viewModel.mediaEndPosition, to: mediaEndPositionLabel)

    viewModel.mediaFileName
      .observe(on: MainScheduler.instance)
      .bind { [weak self] filename in
        self?.navigationItem.title = filename
      }.disposed(by: disposeBag)

    viewModel.mediaImageViewImageData
      .observe(on: MainScheduler.instance)
      .bind { [weak self] imageData in
        self?.mediaImageView.image = UIImage(data: imageData)
      }.disposed(by: disposeBag)

    viewModel.mediaImageViewContentMode
      .observe(on: MainScheduler.instance)
      .bind { [weak self] imageViewContentMode in
        switch imageViewContentMode {
        case .scaleAspectFit:
          self?.mediaImageView.contentMode = .scaleAspectFit
        case .scaleToFill:
          self?.mediaImageView.contentMode = .scaleAspectFill
        }
      }.disposed(by: disposeBag)
  }

  private func bindRelayToSlider(relay: BehaviorRelay<Float>, to slider: UISlider) {
    relay
      .bind(to: slider.rx.value)
      .disposed(by: disposeBag)
  }

  private func bindRelayToLabel(relay: BehaviorRelay<String>, to label: UILabel) {
    relay
      .bind(to: label.rx.text)
      .disposed(by: disposeBag)
  }

  private func bindSubjects() {
    viewModel.dimScreen
      .observe(on: MainScheduler.instance)
      .bind { [weak self] in
        self?.dimScreen()
      }.disposed(by: disposeBag)

    viewModel.vibrate.bind { UIDevice.vibrate(.success) }.disposed(by: disposeBag)
  }

  private func dimScreen() {
    tabBarController?.view.addSubview(dimView)
  }

  private func bindGestureRecognizer() {
    tapGestureRecognizer.rx.event
      .bind { [weak self] _ in
        self?.restartScreenDimming()
      }.disposed(by: disposeBag)
  }

  private func restartScreenDimming() {
    viewModel.restartDimScreenTimer()
    dimView.removeFromSuperview()
  }
}
