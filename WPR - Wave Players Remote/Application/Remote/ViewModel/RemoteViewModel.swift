//
//  RemoteViewModel.swift
//  WPR - Wave Players Remote
//
//  Created by Tomasz Brzozowski on 22/01/2021.
//

import RxRelay
import RxSwift

class RemoteViewModel: RemoteViewModeling {
  let waveIconState = BehaviorRelay<WaveButtonIconState>(value: .inactive)
  let playPauseIconState = BehaviorRelay<PlayPauseButtonIconState>(value: .play)
  let mediaImageViewContentMode = BehaviorRelay<ImageViewContentMode>(value: .scaleToFill)
  let mediaFileName = BehaviorRelay<String>(value: R.string.localizable.remoteViewTitle())
  let mediaProgressSliderPosition = BehaviorRelay<Float>(value: 0)
  let volumeSliderPosition = BehaviorRelay<Float>(value: 0)
  let mediaImageViewImageData = BehaviorRelay<Data>(value: Data())
  let mediaCurrentPosition = BehaviorRelay<String>(value: R.string.localizable.mediaDefaultPosition())
  let mediaEndPosition = BehaviorRelay<String>(value: R.string.localizable.mediaDefaultPosition())
  let dimScreen = PublishSubject<Void>()
  let vibrate = PublishSubject<Void>()
  let didBecomeActiveNotification: Observable<Void>
  let willResignActiveNotification: Observable<Void>

  private let log: Logger
  private let timerService: TimerProvider
  private let userDefaultsService: UserDefaultsProvider
  private let proximityService: ProximityStateProvider
  private let incomingCallService: IncomingCallProvider
  private let userNotificationsService: UserNotificationsProvider
  private let mpcClient: MediaPlayerClient
  private let vlcClient: MediaPlayerClient
  private let jumpSecondsInterval = 5
  private let disposeBag = DisposeBag()
  private var mediaDuration: Int = 0
  private var repeatMode = 0
  private var subtitleSwitch = false
  private var fetchVariablesTimer: Disposable?
  private var fetchMediaImageTimer: Disposable?
  private var dimScreenTimer: Disposable?

  init(
    log: Logger,
    timerService: TimerProvider,
    userDefaultsService: UserDefaultsProvider,
    proximityService: ProximityStateProvider,
    incomingCallService: IncomingCallProvider,
    userNotificationsService: UserNotificationsProvider,
    mpcClient: MediaPlayerClient,
    vlcClient: MediaPlayerClient
  ) {
    self.log = log
    self.timerService = timerService
    self.userDefaultsService = userDefaultsService
    self.proximityService = proximityService
    self.incomingCallService = incomingCallService
    self.userNotificationsService = userNotificationsService
    self.mpcClient = mpcClient
    self.vlcClient = vlcClient
    didBecomeActiveNotification = self.userNotificationsService.didBecomeActiveNotification()
    willResignActiveNotification = self.userNotificationsService.willResignActiveNotification()
    bindServices()
  }

  private func bindServices() {
    userDefaultsService.controlledPlayerObservable().bind { [weak self] controlledPlayer in
      if let self = self {
        self.mediaImageViewImageData.accept(Data())
        self.mediaImageViewContentMode.accept(self.getMediaImageViewContentMode(for: controlledPlayer))
        self.playPauseIconState.accept(.play)
        self.volumeSliderPosition.accept(0.0)
        self.mediaDuration = 0
        self.mediaProgressSliderPosition.accept(0.0)
        self.mediaCurrentPosition.accept(R.string.localizable.mediaDefaultPosition())
        self.mediaEndPosition.accept(R.string.localizable.mediaDefaultPosition())
      }
    }.disposed(by: disposeBag)

    proximityService.proximityStateClose()
      .bind { [weak self] _ in
        self?.onProximityStateClose()
      }.disposed(by: disposeBag)

    incomingCallService.incomingCall()
      .bind { [weak self] _ in
        self?.handleIncomingCall()
      }.disposed(by: disposeBag)
  }

  private func getMediaImageViewContentMode(for controlledPlayer: MediaPlayer) -> ImageViewContentMode {
    switch controlledPlayer {
    case .mpc:
      return .scaleToFill
    case .vlc:
      return .scaleAspectFit
    }
  }

  func restartTimers() {
    restartFetchVariablesTimer()
    fetchVariablesTimer?.disposed(by: disposeBag)
    fetchMediaImageTimer = timerService.timer(.milliseconds(0), period: .seconds(2))
      .bind { [weak self] _ in
        self?.fetchMediaImageData()
      }
    fetchMediaImageTimer?.disposed(by: disposeBag)
    restartDimScreenTimer()
  }

  func restartFetchVariablesTimer() {
    fetchVariablesTimer = timerService.timer(.milliseconds(0), period: .seconds(1))
      .bind { [weak self] _ in
        self?.fetchVariables()
      }
  }

  private func fetchVariables() {
    log.info("Fetch variables", #file, #function, #line)
    getMediaPlayerClient().getVariables(getServerAddress(), getServerPort(), getServerPassword())
      .subscribe { [weak self] result in
        switch result {
        case let .success(variables):
          let mediaFileName = variables.fileName
          self?.mediaFileName.accept(mediaFileName)
          switch variables.state {
          case MediaPlayerState.paused.rawValue, MediaPlayerState.stopped.rawValue:
            self?.playPauseIconState.accept(.play)
          case MediaPlayerState.playing.rawValue:
            self?.playPauseIconState.accept(.pause)
          default:
            break
          }
          self?.volumeSliderPosition.accept(Float(variables.volumeLevel) ?? 0.0)
          self?.mediaDuration = Int(variables.duration) ?? 0
          self?.mediaProgressSliderPosition.accept(Float(variables.position) ?? 0.0)
          self?.mediaCurrentPosition.accept(variables.positionString)
          self?.mediaEndPosition.accept(variables.durationString)
        case let .failure(error):
          self?.log.error(error, #file, #function, #line)
        }
      }.disposed(by: disposeBag)
  }

  private func getMediaPlayerClient() -> MediaPlayerClient {
    switch userDefaultsService.controlledPlayer() {
    case .mpc:
      return mpcClient
    case .vlc:
      return vlcClient
    }
  }

  private func getServerAddress() -> String { userDefaultsService.serverAddress() }
  private func getServerPort() -> Int { userDefaultsService.serverPort() }
  private func getServerPassword() -> String { userDefaultsService.serverPassword() }

  private func fetchMediaImageData() {
    log.info("Fetch media image view image data", #file, #function, #line)
    getMediaPlayerClient().getSnapshot(getServerAddress(), getServerPort(), getServerPassword())
      .subscribe { [weak self] result in
        switch result {
        case let .success(snapshot):
          self?.mediaImageViewImageData.accept(snapshot)
        case let .failure(error):
          self?.mediaImageViewImageData.accept(Data())
          self?.log.error(error, #file, #function, #line)
        }
      }.disposed(by: disposeBag)
  }

  func restartDimScreenTimer() {
    stopDimScreenTimer()
    let timeout = userDefaultsService.dimScreenTimeout()
    switch timeout {
    case .never:
      break
    default:
      dimScreenTimer = timerService.singleTimer(dueTime: .seconds(timeout.rawValue)).bind { [weak self] _ in
        self?.dimScreen.onNext(())
      }
      dimScreenTimer?.disposed(by: disposeBag)
    }
  }

  private func stopDimScreenTimer() {
    dimScreenTimer?.dispose()
    dimScreenTimer = nil
  }

  func stopTimers() {
    stopFetchVariablesTimer()
    fetchMediaImageTimer?.dispose()
    fetchMediaImageTimer = nil
    stopDimScreenTimer()
  }

  func stopFetchVariablesTimer() {
    fetchVariablesTimer?.dispose()
    fetchVariablesTimer = nil
  }

  func togglePlayPause() {
    log.info("Button tapped", #file, #function, #line)
    getMediaPlayerClient().togglePlayPause(getServerAddress(), getServerPort(), getServerPassword())
      .subscribe { [weak self] event in
        self?.handleCompletableEvent(event) {
          switch self?.playPauseIconState.value {
          case .play:
            self?.playPauseIconState.accept(.pause)
          case .pause:
            self?.playPauseIconState.accept(.play)
          case .none:
            break
          }
        }
      }.disposed(by: disposeBag)
  }

  private func handleCompletableEvent(
    _ completableEvent: CompletableEvent,
    onCompletedAction: @escaping () -> Void = {}
  ) {
    switch completableEvent {
    case .completed:
      onCompletedAction()
    case let .error(error):
      log.warning(error, #file, #function, #line)
    }
  }

  func toggleFullscreen() {
    log.info("Button tapped", #file, #function, #line)
    getMediaPlayerClient().toggleFullscreen(getServerAddress(), getServerPort(), getServerPassword())
      .subscribe { [weak self] event in
        self?.handleCompletableEvent(event)
      }.disposed(by: disposeBag)
  }

  func setVolume(to volumeSliderPosition: Float) {
    let volume = volumeSliderPosition
    log.info("Slider value changed to \(volumeSliderPosition)", #file, #function, #line)
    getMediaPlayerClient().setVolume(to: volume, getServerAddress(), getServerPort(), getServerPassword())
      .subscribe { [weak self] event in
        self?.handleCompletableEvent(event)
      }.disposed(by: disposeBag)
  }

  func seek(to mediaProgressSliderPosition: Float) {
    fetchVariables()
    let position = Int(mediaProgressSliderPosition * Float(mediaDuration) / 1000.0)
    let positionTimeCode = TimeCode(totalSeconds: position)
    log.info("Slider value changed to \(mediaProgressSliderPosition)", #file, #function, #line)
    getMediaPlayerClient().seek(to: positionTimeCode, getServerAddress(), getServerPort(), getServerPassword())
      .subscribe { [weak self] event in
        self?.handleCompletableEvent(event)
      }.disposed(by: disposeBag)
  }

  func fastForward() {
    log.info("Button tapped", #file, #function, #line)
    jump(by: jumpSecondsInterval)
  }

  func fastBackward() {
    log.info("Button tapped", #file, #function, #line)
    jump(by: -jumpSecondsInterval)
  }

  private func jump(by seconds: Int) {
    fetchVariables()
    getMediaPlayerClient().jump(by: seconds, getServerAddress(), getServerPort(), getServerPassword())
      .subscribe { [weak self] event in
        self?.handleCompletableEvent(event)
      }.disposed(by: disposeBag)
  }

  func nextSubtitle() {
    log.info("Button tapped", #file, #function, #line)
    getMediaPlayerClient()
      .nextSubtitleTrack(subtitleOn: subtitleSwitch, getServerAddress(), getServerPort(), getServerPassword())
      .subscribe { [weak self] event in
        self?.handleCompletableEvent(event) {
          if let subtitleSwitch = self?.subtitleSwitch { self?.subtitleSwitch = !subtitleSwitch }
        }
      }.disposed(by: disposeBag)
  }

  func nextAudioTrack() {
    log.info("Button tapped", #file, #function, #line)
    getMediaPlayerClient().nextAudioTrack(getServerAddress(), getServerPort(), getServerPassword())
      .subscribe { [weak self] event in
        self?.handleCompletableEvent(event)
      }.disposed(by: disposeBag)
  }

  func toggleRepeatMode() {
    log.info("Button tapped", #file, #function, #line)
    getMediaPlayerClient().nextRepeatMode(after: repeatMode, getServerAddress(), getServerPort(), getServerPassword())
      .subscribe { [weak self] event in
        guard let self = self else { return }
        self.handleCompletableEvent(event) {
          self.repeatMode += 1
          if self.repeatMode > 3 {
            self.repeatMode = 0
          }
        }
      }.disposed(by: disposeBag)
  }

  func nextFile() {
    log.info("Button tapped", #file, #function, #line)
    getMediaPlayerClient().nextFile(getServerAddress(), getServerPort(), getServerPassword())
      .subscribe { [weak self] event in
        self?.handleCompletableEvent(event)
      }.disposed(by: disposeBag)
  }

  func previousFile() {
    log.info("Button tapped", #file, #function, #line)
    getMediaPlayerClient().previousFile(getServerAddress(), getServerPort(), getServerPassword())
      .subscribe { [weak self] event in
        self?.handleCompletableEvent(event)
      }.disposed(by: disposeBag)
  }

  func toggleProximityIcon() {
    proximityService.toggleObservingProximityState()
    switch waveIconState.value {
    case .active:
      waveIconState.accept(.inactive)
    case .inactive:
      waveIconState.accept(.active)
    }
  }

  func onProximityStateClose() {
    togglePlayPause()
    if userDefaultsService.vibrateOnWave() {
      vibrate.onNext(())
    }
  }

  func handleIncomingCall() {
    if userDefaultsService.pauseOnIncomingCall() {
      disableObservingProximityState()
      getMediaPlayerClient().pause(getServerAddress(), getServerPort(), getServerPassword())
        .subscribe { [weak self] event in
          self?.handleCompletableEvent(event)
        }.disposed(by: disposeBag)
    }
  }

  private func disableObservingProximityState() {
    proximityService.disableObservingProximityState()
    waveIconState.accept(.inactive)
  }
}
