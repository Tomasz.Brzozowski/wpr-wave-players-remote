//
//  RemoteViewModeling.swift
//  WPR - Wave Players Remote
//
//  Created by Tomasz Brzozowski on 19/05/2021.
//

import RxRelay
import RxSwift

protocol RemoteViewModeling {
  var waveIconState: BehaviorRelay<WaveButtonIconState> { get }
  var playPauseIconState: BehaviorRelay<PlayPauseButtonIconState> { get }
  var mediaImageViewContentMode: BehaviorRelay<ImageViewContentMode> { get }
  var mediaFileName: BehaviorRelay<String> { get }
  var mediaProgressSliderPosition: BehaviorRelay<Float> { get }
  var volumeSliderPosition: BehaviorRelay<Float> { get }
  var mediaImageViewImageData: BehaviorRelay<Data> { get }
  var mediaCurrentPosition: BehaviorRelay<String> { get }
  var mediaEndPosition: BehaviorRelay<String> { get }
  var dimScreen: PublishSubject<Void> { get }
  var vibrate: PublishSubject<Void> { get }
  var didBecomeActiveNotification: Observable<Void> { get }
  var willResignActiveNotification: Observable<Void> { get }

  func restartTimers()
  func stopTimers()
  func restartFetchVariablesTimer()
  func stopFetchVariablesTimer()
  func restartDimScreenTimer()
  func togglePlayPause()
  func toggleFullscreen()
  func setVolume(to volumeSliderPosition: Float)
  func seek(to mediaProgressSliderPosition: Float)
  func fastForward()
  func fastBackward()
  func nextSubtitle()
  func nextAudioTrack()
  func toggleRepeatMode()
  func nextFile()
  func previousFile()
  func toggleProximityIcon()
  func onProximityStateClose()
  func handleIncomingCall()
}
