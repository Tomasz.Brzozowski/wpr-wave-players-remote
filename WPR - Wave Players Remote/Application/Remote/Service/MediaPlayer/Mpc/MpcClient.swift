//
//  MpcClient.swift
//  WPR - Wave Players Remote
//
//  Created by Tomasz Brzozowski on 19/11/2020.
//

import RxRelay
import RxSwift
import SwiftSoup

class MpcClient: ExtendedMediaPlayerClient {
  let httpMpcClient: HttpMediaPlayerProvider

  init(httpMpcClient: HttpMediaPlayerProvider) {
    self.httpMpcClient = httpMpcClient
  }

  func getVariables(
    _ serverAddress: String,
    _ serverPort: Int,
    _ serverPassword: String
  ) -> Single<MediaPlayerVariables> {
    return httpMpcClient.getStatusString(serverAddress, serverPort, serverPassword)
      .map { htmlString -> MediaPlayerVariables in
        var variables = [String: String]()
        let doc: Document = try SwiftSoup.parse(htmlString)
        for variableId in MpcVariableKey.all() {
          let element = try doc.getElementById(variableId)
          var value: String
          if element == nil {
            value = ""
          } else {
            value = try element!.text()
            if variableId == MpcVariableKey.volumeLevel, let intValue = Int(value) {
              value = String(Float(intValue) / 100.0)
            }
          }
          variables[variableId] = value
        }

        if variables[MpcVariableKey.fileName] == nil || variables[MpcVariableKey.fileName]?.isEmpty == true {
          let filepath = variables[MpcVariableKey.filePath] ?? ""
          let lastIndexOfBackslash = filepath.lastIndex(of: "\\") ?? "".startIndex
          let fileName = String(filepath[filepath.index(after: lastIndexOfBackslash)...])
          variables[MpcVariableKey.fileName] = fileName
        }
        let position = variables[MpcVariableKey.position] ?? ""
        let duration = variables[MpcVariableKey.duration] ?? ""
        let positionRatio = (Float(position) ?? 0.0) / (Float(duration) ?? 1.0)
        variables[MpcVariableKey.position] = String(positionRatio)
        let mediaPlayerVariables = MediaPlayerVariables(mpcVariables: variables)
        return mediaPlayerVariables
      }
  }

  func previousFile(_ serverAddress: String, _ serverPort: Int, _ serverPassword: String) -> Completable {
    return httpMpcClient.previousFile(serverAddress, serverPort, serverPassword).asCompletable()
  }

  func nextFile(_ serverAddress: String, _ serverPort: Int, _ serverPassword: String) -> Completable {
    return httpMpcClient.nextFile(serverAddress, serverPort, serverPassword).asCompletable()
  }

  func toggleFullscreen(_ serverAddress: String, _ serverPort: Int, _ serverPassword: String) -> Completable {
    return httpMpcClient.toggleFullscreen(serverAddress, serverPort, serverPassword).asCompletable()
  }

  func nextAudioTrack(_ serverAddress: String, _ serverPort: Int, _ serverPassword: String) -> Completable {
    return httpMpcClient.nextAudioTrack(serverAddress, serverPort, serverPassword).asCompletable()
  }

  func togglePlayPause(_ serverAddress: String, _ serverPort: Int, _ serverPassword: String) -> Completable {
    return httpMpcClient.togglePlayPause(serverAddress, serverPort, serverPassword).asCompletable()
  }

  func pause(_ serverAddress: String, _ serverPort: Int, _ serverPassword: String) -> Completable {
    return httpMpcClient.pause(serverAddress, serverPort, serverPassword).asCompletable()
  }

  func setVolume(
    to volume: Float,
    _ serverAddress: String,
    _ serverPort: Int,
    _ serverPassword: String
  ) -> Completable {
    var volume = volume
    if volume < 0.0 {
      volume = 0.0
    } else if volume > 1.0 {
      volume = 1.0
    }
    let volumeString = String(Int(volume * 100))
    return httpMpcClient.setVolume(volumeString, serverAddress, serverPort, serverPassword).asCompletable()
  }

  func seek(
    to position: TimeCode,
    _ serverAddress: String,
    _ serverPort: Int,
    _ serverPassword: String
  ) -> Completable {
    var position = position
    let durationSingle = getDuration(serverAddress: serverAddress, serverPort: serverPort,
                                     serverPassword: serverPassword)
    return durationSingle.map { duration -> String in
      if TimeCode.minus(minuend: position, subtrahend: duration).compareTo(other: TimeCode(totalSeconds: 0)) == 1 {
        position = duration
      }
      let positionString = position.toString()
      return positionString
    }.flatMap { [weak self] positionString -> Single<Data> in
      self?.httpMpcClient.seek(positionString, serverAddress, serverPort, serverPassword) ?? Single.just(Data())
    }.asCompletable()
  }

  func nextRepeatMode(
    after repeatMode: Int,
    _ serverAddress: String,
    _ serverPort: Int,
    _ serverPassword: String
  ) -> Completable {
    let repeatPlayForever = self.repeatPlayForever(serverAddress: serverAddress, serverPort: serverPort,
                                                   serverPassword: serverPassword)
    switch repeatMode {
    case 1:
      return repeatPlayForever.andThen(repeatPlayOneFile(serverAddress: serverAddress, serverPort: serverPort,
                                                         serverPassword: serverPassword))
    case 2:
      return repeatPlayForever
    case 3:
      return repeatPlayForever.andThen(repeatPlayWholePlaylist(serverAddress: serverAddress, serverPort: serverPort,
                                                               serverPassword: serverPassword))
    default:
      return repeatPlayForever
    }
  }

  private func repeatPlayForever(serverAddress: String, serverPort: Int, serverPassword: String) -> Completable {
    return httpMpcClient.repeatPlayForever(serverAddress, serverPort, serverPassword).asCompletable()
  }

  private func repeatPlayOneFile(serverAddress: String, serverPort: Int, serverPassword: String) -> Completable {
    return httpMpcClient.repeatPlayOneFile(serverAddress, serverPort, serverPassword).asCompletable()
  }

  private func repeatPlayWholePlaylist(serverAddress: String, serverPort: Int, serverPassword: String) -> Completable {
    return httpMpcClient.repeatPlayWholePlaylist(serverAddress, serverPort, serverPassword).asCompletable()
  }

  func nextSubtitleTrack(
    subtitleOn: Bool,
    _ serverAddress: String,
    _ serverPort: Int,
    _ serverPassword: String
  ) -> Completable {
    let onOffSubtitle = httpMpcClient.onOffSubtitle(serverAddress, serverPort, serverPassword).asCompletable()
    if subtitleOn {
      return onOffSubtitle
    } else {
      return onOffSubtitle.andThen(
        httpMpcClient.nextSubtitleTrack(serverAddress, serverPort, serverPassword).asCompletable()
      )
    }
  }

  func getSnapshot(_ serverAddress: String, _ serverPort: Int, _ serverPassword: String) -> Single<Data> {
    return httpMpcClient.getSnapshot(serverAddress, serverPort, serverPassword)
  }
}
