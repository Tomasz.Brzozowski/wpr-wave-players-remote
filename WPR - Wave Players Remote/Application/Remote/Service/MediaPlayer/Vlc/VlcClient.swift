//
//  VlcClient.swift
//  WPR - Wave Players Remote
//
//  Created by Tomasz Brzozowski on 19/11/2020.
//

import RxRelay
import RxSwift
import SwiftyJSON

class VlcClient: ExtendedMediaPlayerClient {
  let volumeRatio: Float = 320.0
  let httpVlcClient: HttpMediaPlayerProvider

  init(httpVlcClient: HttpMediaPlayerProvider) {
    self.httpVlcClient = httpVlcClient
  }

  func getVariables(
    _ serverAddress: String,
    _ serverPort: Int,
    _ serverPassword: String
  ) -> Single<MediaPlayerVariables> {
    return httpVlcClient.getStatusJson(serverAddress, serverPort, serverPassword)
      .map { [weak self] jsonData -> MediaPlayerVariables in
        let json = JSON(jsonData)
        let filenameSubscript: [JSONSubscriptType] = VlcVariableKey.fileName
        let filename = json[filenameSubscript].stringValue
        let vlcState = json[VlcVariableKey.state].stringValue
        let state: String = {
          switch vlcState {
          case "stopped":
            return MediaPlayerState.stopped.rawValue
          case "paused":
            return MediaPlayerState.paused.rawValue
          case "playing":
            return MediaPlayerState.playing.rawValue
          default:
            return ""
          }
        }()
        let positionRatio = json[VlcVariableKey.positionRatio].stringValue
        let positionSeconds = json[VlcVariableKey.positionSeconds].intValue
        let positionString = TimeCode(totalSeconds: positionSeconds).toString()
        let durationStringMsec = json[VlcVariableKey.durationString].stringValue
        let durationString = TimeCode(totalSeconds: json[VlcVariableKey.length].intValue).toString()
        let duration = String(TimeCode.getTotalMiliseconds(timeCode: durationStringMsec) ?? 0)
        let volume = String(json[VlcVariableKey.volumeLevel].floatValue / (self?.volumeRatio ?? 1))

        let mediaPlayerVariables = MediaPlayerVariables(fileName: filename, filePath: "", state: state,
                                                        position: positionRatio, positionString: positionString,
                                                        duration: duration, durationString: durationString,
                                                        volumeLevel: volume)
        return mediaPlayerVariables
      }
  }

  func previousFile(_ serverAddress: String, _ serverPort: Int, _ serverPassword: String) -> Completable {
    return httpVlcClient.previousFile(serverAddress, serverPort, serverPassword).asCompletable()
  }

  func nextFile(_ serverAddress: String, _ serverPort: Int, _ serverPassword: String) -> Completable {
    return httpVlcClient.nextFile(serverAddress, serverPort, serverPassword).asCompletable()
  }

  func toggleFullscreen(_ serverAddress: String, _ serverPort: Int, _ serverPassword: String) -> Completable {
    return httpVlcClient.toggleFullscreen(serverAddress, serverPort, serverPassword).asCompletable()
  }

  func nextAudioTrack(_ serverAddress: String, _ serverPort: Int, _ serverPassword: String) -> Completable {
    return httpVlcClient.nextAudioTrack(serverAddress, serverPort, serverPassword).asCompletable()
  }

  func togglePlayPause(_ serverAddress: String, _ serverPort: Int, _ serverPassword: String) -> Completable {
    return httpVlcClient.togglePlayPause(serverAddress, serverPort, serverPassword).asCompletable()
  }

  func pause(_ serverAddress: String, _ serverPort: Int, _ serverPassword: String) -> Completable {
    return httpVlcClient.pause(serverAddress, serverPort, serverPassword).asCompletable()
  }

  func setVolume(
    to volume: Float,
    _ serverAddress: String,
    _ serverPort: Int,
    _ serverPassword: String
  ) -> Completable {
    var volume = volume
    if volume < 0.0 {
      volume = 0.0
    } else if volume > 1.0 {
      volume = 1.0
    }
    let volumeString = String(Int(volume * volumeRatio))
    return httpVlcClient.setVolume(volumeString, serverAddress, serverPort, serverPassword).asCompletable()
  }

  func seek(
    to position: TimeCode,
    _ serverAddress: String,
    _ serverPort: Int,
    _ serverPassword: String
  ) -> Completable {
    var position = position
    let durationSingle = getDuration(serverAddress: serverAddress, serverPort: serverPort,
                                     serverPassword: serverPassword)
    return durationSingle.map { duration -> String in
      if TimeCode.minus(minuend: position, subtrahend: duration).compareTo(other: TimeCode(totalSeconds: 0)) == 1 {
        position = duration
      }
      let positionSeconds = position.getTotalSeconds()
      return String(positionSeconds)
    }.flatMap { [weak self] positionSeconds -> Single<Data> in
      self?.httpVlcClient.seek(positionSeconds, serverAddress, serverPort, serverPassword) ?? Single.just(Data())
    }.asCompletable()
  }

  func nextRepeatMode(
    after _: Int,
    _ serverAddress: String,
    _ serverPort: Int,
    _ serverPassword: String
  ) -> Completable {
    return httpVlcClient.nextRepeatMode(serverAddress, serverPort, serverPassword).asCompletable()
  }

  func nextSubtitleTrack(
    subtitleOn _: Bool,
    _ serverAddress: String,
    _ serverPort: Int,
    _ serverPassword: String
  ) -> Completable {
    return httpVlcClient.nextSubtitleTrack(serverAddress, serverPort, serverPassword).asCompletable()
  }

  func getSnapshot(
    _ serverAddress: String,
    _ serverPort: Int,
    _ serverPassword: String
  ) -> Single<Data> {
    return httpVlcClient.getStatusJson(serverAddress, serverPort, serverPassword)
      .map { _ -> Data in
        R.image.remote.icon_vlc()?.pngData() ?? Data()
      }.catchAndReturn(Data())
  }
}
