//
//  MediaPlayerApi.swift
//  WPR - Wave Players Remote
//
//  Created by Tomasz Brzozowski on 03/12/2020.
//

import RxSwift

protocol MediaPlayerClient {
  func getVariables(_ serverAddress: String, _ serverPort: Int, _ serverPassword: String)
    -> Single<MediaPlayerVariables>
  func setVolume(to volume: Float, _ serverAddress: String, _ serverPort: Int, _ serverPassword: String) -> Completable
  func seek(to position: TimeCode, _ serverAddress: String, _ serverPort: Int, _ serverPassword: String) -> Completable
  func jump(by seconds: Int, _ serverAddress: String, _ serverPort: Int, _ serverPassword: String) -> Completable
  func togglePlayPause(_ serverAddress: String, _ serverPort: Int, _ serverPassword: String) -> Completable
  func pause(_ serverAddress: String, _ serverPort: Int, _ serverPassword: String) -> Completable
  func previousFile(_ serverAddress: String, _ serverPort: Int, _ serverPassword: String) -> Completable
  func nextFile(_ serverAddress: String, _ serverPort: Int, _ serverPassword: String) -> Completable
  func nextSubtitleTrack(subtitleOn: Bool, _ serverAddress: String, _ serverPort: Int, _ serverPassword: String)
    -> Completable
  func toggleFullscreen(_ serverAddress: String, _ serverPort: Int, _ serverPassword: String) -> Completable
  func nextAudioTrack(_ serverAddress: String, _ serverPort: Int, _ serverPassword: String) -> Completable
  func nextRepeatMode(after repeatMode: Int, _ serverAddress: String, _ serverPort: Int, _ serverPassword: String)
    -> Completable
  func getSnapshot(_ serverAddress: String, _ serverPort: Int, _ serverPassword: String) -> Single<Data>
}

protocol ExtendedMediaPlayerClient: MediaPlayerClient {}

extension ExtendedMediaPlayerClient {
  func jump(by seconds: Int, _ serverAddress: String, _ serverPort: Int, _ serverPassword: String) -> Completable {
    var destination = TimeCode(totalSeconds: 0)
    return getPosition(serverAddress: serverAddress, serverPort: serverPort, serverPassword: serverPassword)
      .map { position -> TimeCode in
        if seconds >= 0 {
          destination = TimeCode.plus(augend: position, addend: TimeCode(totalSeconds: seconds))
        } else {
          destination = TimeCode.minus(minuend: position, subtrahend: TimeCode(totalSeconds: abs(seconds)))
        }
        return destination
      }.flatMapCompletable { destination -> Completable in
        seek(to: destination, serverAddress, serverPort, serverPassword)
      }
  }

  func getPosition(serverAddress: String, serverPort: Int, serverPassword: String) -> Single<TimeCode> {
    return getVariables(serverAddress, serverPort, serverPassword)
      .map { (variables) -> TimeCode in
        let position = variables.positionString
        return TimeCode(timeCode: position) ?? TimeCode(totalSeconds: 0)
      }
  }

  func getDuration(serverAddress: String, serverPort: Int, serverPassword: String) -> Single<TimeCode> {
    return getVariables(serverAddress, serverPort, serverPassword)
      .map { (variables) -> TimeCode in
        let duration = variables.durationString
        return TimeCode(timeCode: duration) ?? TimeCode(totalSeconds: 0)
      }
  }
}
