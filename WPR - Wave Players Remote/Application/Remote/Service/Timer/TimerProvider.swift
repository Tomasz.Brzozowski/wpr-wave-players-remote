//
//  TimerProvider.swift
//  WPR - Wave Players Remote
//
//  Created by Tomasz Brzozowski on 17/06/2021.
//

import RxSwift

protocol TimerProvider {
  func timer(_ start: RxTimeInterval, period: RxTimeInterval) -> Observable<Int>
  func singleTimer(dueTime: RxTimeInterval) -> Observable<Int>
}
