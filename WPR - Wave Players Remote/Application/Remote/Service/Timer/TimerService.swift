//
//  TimerService.swift
//  WPR - Wave Players Remote
//
//  Created by Tomasz Brzozowski on 17/06/2021.
//

import RxSwift

class TimerService: TimerProvider {
  func timer(_ start: RxTimeInterval, period: RxTimeInterval) -> Observable<Int> {
    return Observable<Int>.timer(start, period: period, scheduler: SerialDispatchQueueScheduler(qos: .background))
  }

  func singleTimer(dueTime: RxTimeInterval) -> Observable<Int> {
    return Single<Int>.timer(dueTime, scheduler: SerialDispatchQueueScheduler(qos: .background)).asObservable()
  }
}
