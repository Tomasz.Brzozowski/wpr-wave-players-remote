//
//  IncomingCallProvider.swift
//  WPR - Wave Players Remote
//
//  Created by Tomasz Brzozowski on 28/04/2021.
//

import RxSwift

protocol IncomingCallProvider {
  func incomingCall() -> PublishSubject<Void>
}
