//
//  IncomingCallService.swift
//  WPR - Wave Players Remote
//
//  Created by Tomasz Brzozowski on 28/04/2021.
//

import CallKit
import RxSwift

@available(iOS 10, *)
class IncomingCallService: NSObject, IncomingCallProvider {
  private let callObserver = CXCallObserver()
  private let incomingCallSubject = PublishSubject<Void>()

  override init() {
    super.init()
    callObserver.setDelegate(self, queue: nil)
  }

  func incomingCall() -> PublishSubject<Void> {
    return incomingCallSubject
  }
}

@available(iOS 10, *)
extension IncomingCallService: CXCallObserverDelegate {
  func callObserver(_: CXCallObserver, callChanged call: CXCall) {
    if !call.isOutgoing, !call.hasEnded, !call.isOnHold, !call.hasConnected {
      incomingCallSubject.onNext(())
    }
  }
}
