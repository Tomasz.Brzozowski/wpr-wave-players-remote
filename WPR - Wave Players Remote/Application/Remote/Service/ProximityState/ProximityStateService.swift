//
//  ProximityStateService.swift
//  WPR - Wave Players Remote
//
//  Created by Tomasz Brzozowski on 16/02/2021.
//

import Foundation
import RxSwift
import UIKit

class ProximityStateService: ProximityStateProvider {
  private let log: Logger

  init(log: Logger) {
    self.log = log
  }

  func toggleObservingProximityState() {
    let device = UIDevice.current
    device.isProximityMonitoringEnabled = !device.isProximityMonitoringEnabled
  }

  func disableObservingProximityState() {
    UIDevice.current.isProximityMonitoringEnabled = false
  }

  func proximityStateClose() -> Observable<Void> {
    let observerName = NSNotification.Name(rawValue: "UIDeviceProximityStateDidChangeNotification")
    return NotificationCenter.default.rx.notification(observerName).filter { [weak self] notification -> Bool in
      if let device = notification.object as? UIDevice {
        if device.proximityState == true {
          return true
        }
        return false
      } else {
        self?.log.warning("No proximity device detected", #file, #function, #line)
        return false
      }
    }.map { _ in }
  }
}
