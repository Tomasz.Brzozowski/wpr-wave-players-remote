//
//  ProximityStateProvider.swift
//  WPR - Wave Players Remote
//
//  Created by Tomasz Brzozowski on 12/03/2021.
//

import RxSwift

protocol ProximityStateProvider {
  func toggleObservingProximityState()
  func disableObservingProximityState()
  func proximityStateClose() -> Observable<Void>
}
