//
//  NavigationAssembly.swift
//  WPR - Wave Players Remote
//
//  Created by Tomasz Brzozowski on 24/05/2021.
//

import Swinject

class NavigationAssembly: Assembly {
  func assemble(container: Container) {
    container.register(MainTabBarController.self) { (r) -> MainTabBarController in
      MainTabBarController(
        userDefaultsService: r.resolve(UserDefaultsProvider.self)!,
        remoteVc: r.resolve(RemoteViewController.self)!,
        serverManagerVc: r.resolve(ServerManagerViewController.self)!,
        settingsVc: r.resolve(SettingsViewController.self)!
      )
    }.inObjectScope(.container)
  }
}
