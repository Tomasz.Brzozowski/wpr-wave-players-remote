//
//  RemoteAssembly.swift
//  WPR - Wave Players Remote
//
//  Created by Tomasz Brzozowski on 19/05/2021.
//

import Swinject

class RemoteAssembly: Assembly {
  func assemble(container: Container) {
    container.register(RemoteViewModeling.self) { r in
      RemoteViewModel(
        log: r.resolve(Logger.self)!,
        timerService: r.resolve(TimerProvider.self)!,
        userDefaultsService: r.resolve(UserDefaultsProvider.self)!,
        proximityService: r.resolve(ProximityStateProvider.self)!,
        incomingCallService: r.resolve(IncomingCallProvider.self)!,
        userNotificationsService: r.resolve(UserNotificationsProvider.self)!,
        mpcClient: r.resolve(MediaPlayerClient.self, name: TypeName.of(MpcClient.self))!,
        vlcClient: r.resolve(MediaPlayerClient.self, name: TypeName.of(VlcClient.self))!
      )
    }

    container.register(RemoteViewController.self) { (r) -> RemoteViewController in
      RemoteViewController(viewModel: r.resolve(RemoteViewModeling.self)!)
    }
  }
}
