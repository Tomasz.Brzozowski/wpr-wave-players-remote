//
//  ServerManagerAssembly.swift
//  WPR - Wave Players Remote
//
//  Created by Tomasz Brzozowski on 19/05/2021.
//

import Swinject

typealias ServerNameDetailViewCell = TextTableViewCell<ServerNameDetailCellViewModel>
typealias ServerAddressDetailViewCell = TextTableViewCell<ServerAddressDetailCellViewModel>
typealias ServerPortDetailViewCell = TextTableViewCell<ServerPortDetailCellViewModel>
typealias ServerPasswordDetailViewCell = TextTableViewCell<ServerPasswordDetailCellViewModel>

class ServerManagerAssembly: Assembly {
  func assemble(container: Container) {
    assembleServerDetailCells(container: container)
    assembleServerDetail(container: container)
    assembleServerManager(container: container)
  }

  private func assembleServerDetailCells(container: Container) {
    container.register(
      TextCellViewModel.self,
      name: TypeName.of(ServerNameDetailCellViewModel.self)
    ) { (r, text: String) -> TextCellViewModel in
      ServerNameDetailCellViewModel(
        text: text,
        textValidationService: r.resolve(TextValidationProvider.self)!
      )
    }.inObjectScope(.transient)

    container.register(
      TextCellViewModel.self,
      name: TypeName.of(ServerAddressDetailCellViewModel.self)
    ) { (r, text: String) -> TextCellViewModel in
      ServerAddressDetailCellViewModel(
        text: text,
        textValidationService: r.resolve(TextValidationProvider.self)!
      )
    }.inObjectScope(.transient)

    container.register(
      TextCellViewModel.self,
      name: TypeName.of(ServerPortDetailCellViewModel.self)
    ) { (r, text: String) -> TextCellViewModel in
      ServerPortDetailCellViewModel(
        text: text,
        textValidationService: r.resolve(TextValidationProvider.self)!
      )
    }.inObjectScope(.transient)

    container.register(
      TextCellViewModel.self,
      name: TypeName.of(ServerPasswordDetailCellViewModel.self)
    ) { (r, text: String) -> TextCellViewModel in
      ServerPasswordDetailCellViewModel(
        text: text,
        textValidationService: r.resolve(TextValidationProvider.self)!
      )
    }.inObjectScope(.transient)
  }

  private func assembleServerDetail(container: Container) {
    container.register(
      ServerDetailViewModeling.self
    ) { (r, editAddress: String?, editPort: String?) -> ServerDetailViewModeling in
      let getServerNameDetailCellViewModel = { (text: String) -> TextCellViewModel in
        r.resolve(
          TextCellViewModel.self, name: TypeName.of(ServerNameDetailCellViewModel.self), argument: text
        )!
      }

      let getServerAddressDetailCellViewModel = { (text: String) -> TextCellViewModel in
        r.resolve(
          TextCellViewModel.self, name: TypeName.of(ServerAddressDetailCellViewModel.self), argument: text
        )!
      }

      let getServerPortDetailCellViewModel = { (text: String) -> TextCellViewModel in
        r.resolve(
          TextCellViewModel.self, name: TypeName.of(ServerPortDetailCellViewModel.self), argument: text
        )!
      }

      let getServerPasswordDetailCellViewModel = { (text: String) -> TextCellViewModel in
        r.resolve(
          TextCellViewModel.self, name: TypeName.of(ServerPasswordDetailCellViewModel.self), argument: text
        )!
      }

      return ServerDetailViewModel(
        log: r.resolve(Logger.self)!,
        userDefaultsService: r.resolve(UserDefaultsProvider.self)!,
        textValidationService: r.resolve(TextValidationProvider.self)!,
        serverMpcService: r.resolve(ServerProvider.self, name: TypeName.of(ServerMpcService.self))!,
        serverVlcService: r.resolve(ServerProvider.self, name: TypeName.of(ServerVlcService.self))!,
        getServerNameDetailCellViewModel: getServerNameDetailCellViewModel,
        getServerAddressDetailCellViewModel: getServerAddressDetailCellViewModel,
        getServerPortDetailCellViewModel: getServerPortDetailCellViewModel,
        getServerPasswordDetailCellViewModel: getServerPasswordDetailCellViewModel,
        editAddress: editAddress,
        editPort: editPort
      )
    }

    container.register(
      ServerDetailViewController.self
    ) { (r, navigationBarTitle: String, confirmButtonTitle: String, editAddress: String?, editPort: String?)
      -> ServerDetailViewController in

      let viewModel = r.resolve(
        ServerDetailViewModeling.self,
        arguments: editAddress, editPort
      )!
      return ServerDetailViewController(
        viewModel: viewModel,
        confirmButtonTitle: confirmButtonTitle,
        navigationBarTitle: navigationBarTitle
      )
    }
  }

  private func assembleServerManager(container: Container) {
    container.register(ServerCellViewModeling.self) { (_, serverCell: ServerCell) -> ServerCellViewModeling in
      ServerCellViewModel(userDefaultsService: container.resolve(UserDefaultsProvider.self)!, cell: serverCell)
    }.inObjectScope(.transient)

    container.register(ServerManagerViewModeling.self) { r in
      ServerManagerViewModel(
        log: r.resolve(Logger.self)!,
        userDefaultsService: r.resolve(UserDefaultsProvider.self)!,
        serverMpcService: r.resolve(ServerProvider.self, name: TypeName.of(ServerMpcService.self))!,
        serverVlcService: r.resolve(ServerProvider.self, name: TypeName.of(ServerVlcService.self))!,
        getServerCellViewModel: { serverCell in r.resolve(ServerCellViewModeling.self, argument: serverCell)! }
      )
    }

    container.register(ServerManagerViewController.self) { (r) -> ServerManagerViewController in
      let getServerDetailVc = { (navigationBarTitle: String, confirmButtonTitle: String,
                                 editAddress: String?, editPort: String?) -> ServerDetailViewController in
        r.resolve(
          ServerDetailViewController.self,
          arguments: navigationBarTitle, confirmButtonTitle, editAddress, editPort
        )!
      }

      return ServerManagerViewController(
        viewModel: r.resolve(ServerManagerViewModeling.self)!,
        getServerDetailVc: getServerDetailVc
      )
    }
  }
}
