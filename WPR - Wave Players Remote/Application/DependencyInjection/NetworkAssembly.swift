//
//  NetworkAssembly.swift
//  WPR - Wave Players Remote
//
//  Created by Tomasz Brzozowski on 04/06/2021.
//

import Swinject

typealias HttpVlcMoyaProvider = BaseMoyaProvider<VlcTarget>
typealias HttpMpcMoyaProvider = BaseMoyaProvider<MpcTarget>

class NetworkAssembly: Assembly {
  func assemble(container: Container) {
    container.register(HttpVlcMoyaProvider.self) { _ in BaseMoyaProvider<VlcTarget>() }

    container.register(HttpMpcMoyaProvider.self) { _ in BaseMoyaProvider<MpcTarget>() }

    container.register(HttpMediaPlayerProvider.self, name: TypeName.of(HttpVlcService.self)) { _ in
      HttpVlcService(provider: HttpVlcMoyaProvider())
    }.inObjectScope(.container)

    container.register(HttpMediaPlayerProvider.self, name: TypeName.of(HttpMpcService.self)) { _ in
      HttpMpcService(provider: HttpMpcMoyaProvider())
    }.inObjectScope(.container)

  }
}
