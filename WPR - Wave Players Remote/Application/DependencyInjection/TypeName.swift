//
//  TypeNames.swift
//  WPR - Wave Players Remote
//
//  Created by Tomasz Brzozowski on 24/05/2021.
//

enum TypeName {
  static func of(_ type: Any) -> String {
    return String(describing: type)
  }
}
