//
//  LoggerAssembly.swift
//  WPR - Wave Players Remote
//
//  Created by Tomasz Brzozowski on 04/06/2021.
//

import Swinject

class LoggerAssembly: Assembly {
  func assemble(container: Container) {
    container.register(Logger.self) { _ in SwiftyBeaverLogger() }.inObjectScope(.container)
  }
}
