//
//  DependencyProvider.swift
//  WPR - Wave Players Remote
//
//  Created by Tomasz Brzozowski on 18/05/2021.
//

import Swinject

class DependencyProvider {
  let container = Container()
  let assembler: Assembler
  let assemblies: [Assembly] = [
    LoggerAssembly(),
    NetworkAssembly(),
    ServiceAssembly(),
    SettingsAssembly(),
    ServerManagerAssembly(),
    RemoteAssembly(),
    NavigationAssembly(),
  ]

  init() {
    assembler = Assembler(assemblies, container: container)
  }
}
