//
//  ServiceAssembly.swift
//  WPR - Wave Players Remote
//
//  Created by Tomasz Brzozowski on 19/05/2021.
//

import Swinject

class ServiceAssembly: Assembly {
  func assemble(container: Container) {
    container.register(TimerProvider.self) { _ in TimerService() }.inObjectScope(.container)

    container.register(UserDefaultsProvider.self) { _ in UserDefaultsService() }.inObjectScope(.container)

    container.register(UserNotificationsProvider.self) { _ in UserNotificationsService() }.inObjectScope(.container)

    container.register(ServerProvider.self, name: TypeName.of(ServerMpcService.self)) { r in
      ServerMpcService(httpMpcClient: r.resolve(HttpMediaPlayerProvider.self, name: TypeName.of(HttpMpcService.self))!)
    }.inObjectScope(.container)

    container.register(ServerProvider.self, name: TypeName.of(ServerVlcService.self)) { r in
      ServerVlcService(httpVlcClient: r.resolve(HttpMediaPlayerProvider.self, name: TypeName.of(HttpVlcService.self))!)
    }.inObjectScope(.container)

    container.register(TextValidationProvider.self) { _ in TextValidationService() }.inObjectScope(.container)

    container.register(MediaPlayerClient.self, name: TypeName.of(MpcClient.self)) { r in
      MpcClient(httpMpcClient: r.resolve(HttpMediaPlayerProvider.self, name: TypeName.of(HttpMpcService.self))!)
    }.inObjectScope(.container)

    container.register(MediaPlayerClient.self, name: TypeName.of(VlcClient.self)) { r in
      VlcClient(httpVlcClient: r.resolve(HttpMediaPlayerProvider.self, name: TypeName.of(HttpVlcService.self))!)
    }.inObjectScope(.container)

    container.register(ProximityStateProvider.self) { r in
      ProximityStateService(log: r.resolve(Logger.self)!)
    }.inObjectScope(.container)

    container.register(IncomingCallProvider.self) { _ in IncomingCallService() }.inObjectScope(.container)
  }
}
