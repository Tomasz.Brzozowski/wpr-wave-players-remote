//
//  SettingsAssembly.swift
//  WPR - Wave Players Remote
//
//  Created by Tomasz Brzozowski on 20/05/2021.
//

import Swinject

typealias ServerAddressSettingsViewCell = TextSettingsTableViewCell<ServerAddressSettingsCellViewModel>
typealias ServerPortSettingsViewCell = TextSettingsTableViewCell<ServerPortSettingsCellViewModel>
typealias ServerPasswordSettingsViewCell = TextSettingsTableViewCell<ServerPasswordSettingsCellViewModel>

class SettingsAssembly: Assembly {
  func assemble(container: Container) {
    assembleRadioSetting(container: container)
    assembleSettingsCellViewModels(container: container)
    assembleSettings(container: container)
  }

  private func assembleRadioSetting(container: Container) {
    container.register(RadioSettingViewModel.self, name: TypeName.of(ControlledPlayerViewModel.self)) {
      (r) -> RadioSettingViewModel in
      ControlledPlayerViewModel(userDefaultsService: r.resolve(UserDefaultsProvider.self)!)
    }

    container.register(RadioSettingViewModel.self, name: TypeName.of(DimScreenTimeoutViewModel.self)) {
      (r) -> RadioSettingViewModel in
      DimScreenTimeoutViewModel(userDefaultsService: r.resolve(UserDefaultsProvider.self)!)
    }

    container.register(ControlledPlayerViewController.self) { (r) -> ControlledPlayerViewController in
      ControlledPlayerViewController(
        viewModel: r.resolve(RadioSettingViewModel.self, name: TypeName.of(ControlledPlayerViewModel.self))!
      )
    }

    container.register(DimScreenTimeoutViewController.self) { (r) -> DimScreenTimeoutViewController in
      DimScreenTimeoutViewController(
        viewModel: r.resolve(RadioSettingViewModel.self, name: TypeName.of(DimScreenTimeoutViewModel.self))!
      )
    }
  }

  private func assembleSettingsCellViewModels(container: Container) {
    assembleTextSettingsCellViewModels(container: container)

    container.register(
      RadioSettingsCellViewModeling.self,
      name: TypeName.of(ControlledPlayerSettingsCellViewModel.self)
    ) { (r) -> ControlledPlayerSettingsCellViewModel in
      ControlledPlayerSettingsCellViewModel(userDefaultsService: r.resolve(UserDefaultsProvider.self)!)
    }.inObjectScope(.transient)

    container.register(
      RadioSettingsCellViewModeling.self,
      name: TypeName.of(DimScreenTimeoutSettingsCellViewModel.self)
    ) { (r) -> DimScreenTimeoutSettingsCellViewModel in
      DimScreenTimeoutSettingsCellViewModel(userDefaultsService: r.resolve(UserDefaultsProvider.self)!)
    }.inObjectScope(.transient)

    container.register(
      ToggleSettingsCellViewModeling.self,
      name: TypeName.of(PauseOnCallSettingsCellViewModel.self)
    ) { (r) -> PauseOnCallSettingsCellViewModel in
      PauseOnCallSettingsCellViewModel(userDefaultsService: r.resolve(UserDefaultsProvider.self)!)
    }.inObjectScope(.transient)

    container.register(
      ToggleSettingsCellViewModeling.self,
      name: TypeName.of(VibrateOnWaveSettingsCellViewModel.self)
    ) { (r) -> VibrateOnWaveSettingsCellViewModel in
      VibrateOnWaveSettingsCellViewModel(userDefaultsService: r.resolve(UserDefaultsProvider.self)!)
    }.inObjectScope(.transient)
  }

  private func assembleTextSettingsCellViewModels(container: Container) {
    container.register(
      TextSettingsCellViewModel.self,
      name: TypeName.of(ServerAddressSettingsCellViewModel.self)
    ) { (r) -> TextSettingsCellViewModel in
      ServerAddressSettingsCellViewModel(
        userDefaultsService: r.resolve(UserDefaultsProvider.self)!,
        textValidationService: r.resolve(TextValidationProvider.self)!
      )
    }.inObjectScope(.transient)

    container.register(
      TextSettingsCellViewModel.self,
      name: TypeName.of(ServerPortSettingsCellViewModel.self)
    ) { (r) -> TextSettingsCellViewModel in
      ServerPortSettingsCellViewModel(
        userDefaultsService: r.resolve(UserDefaultsProvider.self)!,
        textValidationService: r.resolve(TextValidationProvider.self)!
      )
    }.inObjectScope(.transient)

    container.register(
      TextSettingsCellViewModel.self,
      name: TypeName.of(ServerPasswordSettingsCellViewModel.self)
    ) { (r) -> TextSettingsCellViewModel in
      ServerPasswordSettingsCellViewModel(userDefaultsService: r.resolve(UserDefaultsProvider.self)!)
    }.inObjectScope(.transient)
  }

  private func assembleSettings(container: Container) {
    container.register(SettingsViewModeling.self) { r -> SettingsViewModeling in
      let getServerAddressCellViewModel = {
        r.resolve(TextSettingsCellViewModel.self, name: TypeName.of(ServerAddressSettingsCellViewModel.self))!
      }
      let getServerPortCellViewModel = {
        r.resolve(TextSettingsCellViewModel.self, name: TypeName.of(ServerPortSettingsCellViewModel.self))!
      }
      let getServerPasswordCellViewModel = {
        r.resolve(TextSettingsCellViewModel.self, name: TypeName.of(ServerPasswordSettingsCellViewModel.self))!
      }
      let getControlledPlayerCellViewModel = {
        r.resolve(RadioSettingsCellViewModeling.self, name: TypeName.of(ControlledPlayerSettingsCellViewModel.self))!
      }
      let getVibrateOnWaveCellViewModel = {
        r.resolve(ToggleSettingsCellViewModeling.self, name: TypeName.of(VibrateOnWaveSettingsCellViewModel.self))!
      }
      let getPauseOnCallCellViewModel = {
        r.resolve(ToggleSettingsCellViewModeling.self, name: TypeName.of(PauseOnCallSettingsCellViewModel.self))!
      }
      let getDimScreenTimeoutCellViewModel = {
        r.resolve(RadioSettingsCellViewModeling.self, name: TypeName.of(DimScreenTimeoutSettingsCellViewModel.self))!
      }

      return SettingsViewModel(
        getServerAddressCellViewModel: getServerAddressCellViewModel,
        getServerPortCellViewModel: getServerPortCellViewModel,
        getServerPasswordCellViewModel: getServerPasswordCellViewModel,
        getControlledPlayerCellViewModel: getControlledPlayerCellViewModel,
        getVibrateOnWaveCellViewModel: getVibrateOnWaveCellViewModel,
        getPauseOnCallCellViewModel: getPauseOnCallCellViewModel,
        getDimScreenTimeoutCellViewModel: getDimScreenTimeoutCellViewModel
      )
    }

    container.register(SettingsViewController.self) { (r) -> SettingsViewController in
      SettingsViewController(
        viewModel: r.resolve(SettingsViewModeling.self)!,
        getControlledPlayerVc: { r.resolve(ControlledPlayerViewController.self)! },
        getDimScreenTimeoutVc: { r.resolve(DimScreenTimeoutViewController.self)! }
      )
    }
  }
}
