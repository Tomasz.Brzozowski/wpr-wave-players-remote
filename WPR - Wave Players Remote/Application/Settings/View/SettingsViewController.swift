//
//  SettingsViewController.swift
//  WPR - Wave Players Remote
//
//  Created by Tomasz Brzozowski on 02/11/2020.
//

import RxDataSources
import RxSwift
import UIKit

class SettingsViewController: UIViewController {
  private static let serverAddressCellReuseId = "serverAddressCell"
  private static let serverPortCellReuseId = "serverPortCell"
  private static let serverPasswordCellReuseId = "serverPasswordCell"
  private static let radioCellReuseId = "radioCell"
  private static let toggleCellReuseId = "toggleCell"
  private static let headerViewReuseId = "sectionHeader"
  private static let footerViewReuseId = "sectionFooter"

  private let tableView: UITableView = {
    let bounds = UIScreen.main.bounds
    let tableView = UITableView(frame: CGRect(x: 0, y: 0, width: bounds.width, height: bounds.height), style: .grouped)
    tableView.backgroundColor = R.color.background()
    tableView.dragDelegate = nil
    tableView.sectionHeaderHeight = 50
    return tableView
  }()

  private let viewModel: SettingsViewModeling
  private let getControlledPlayerVc: () -> ControlledPlayerViewController
  private let getDimScreenTimeoutVc: () -> DimScreenTimeoutViewController
  private let disposeBag = DisposeBag()
  private let dataSource: RxTableViewSectionedReloadDataSource<SectionOfSettings> = {
    RxTableViewSectionedReloadDataSource<SectionOfSettings>(
      configureCell: { _, table, _, item in
        switch item {
        case let .serverAddress(textSettingsCellViewModel):
          return makeCell(with: SettingsCellViewModelType.serverAddress(textSettingsCellViewModel), from: table)
        case let .serverPort(textSettingsCellViewModel):
          return makeCell(with: SettingsCellViewModelType.serverPort(textSettingsCellViewModel), from: table)
        case let .text(textSettingsCellViewModel):
          return makeCell(with: SettingsCellViewModelType.text(textSettingsCellViewModel), from: table)
        case let .radio(radioSettingsViewModel):
          return makeCell(with: SettingsCellViewModelType.radio(radioSettingsViewModel), from: table)
        case let .toggle(toggleSettingsViewModel):
          return makeCell(with: SettingsCellViewModelType.toggle(toggleSettingsViewModel), from: table)
        }
      }
    )
  }()

  private static func makeCell(
    with element: SettingsCellViewModelType,
    from tableView: UITableView
  ) -> UITableViewCell {
    switch element {
    case let .serverAddress(viewModel):
      if let cell = tableView.dequeueReusableCell(withIdentifier: SettingsViewController.serverAddressCellReuseId)
        as? ServerAddressSettingsViewCell {
        cell.viewModel = viewModel as? ServerAddressSettingsCellViewModel
        cell.valueTextField.keyboardType = .numbersAndPunctuation
        return cell
      }
    case let .serverPort(viewModel):
      if let cell = tableView.dequeueReusableCell(withIdentifier: SettingsViewController.serverPortCellReuseId)
        as? ServerPortSettingsViewCell {
        cell.viewModel = viewModel as? ServerPortSettingsCellViewModel
        cell.valueTextField.keyboardType = .numbersAndPunctuation
        return cell
      }
    case let .text(viewModel):
      if let cell = tableView.dequeueReusableCell(withIdentifier: SettingsViewController.serverPasswordCellReuseId)
        as? ServerPasswordSettingsViewCell {
        cell.viewModel = viewModel as? ServerPasswordSettingsCellViewModel
        return cell
      }
    case let .radio(viewModel):
      if let cell = tableView.dequeueReusableCell(
        withIdentifier: SettingsViewController.radioCellReuseId
      ) as? RadioSettingsTableViewCell {
        cell.viewModel = viewModel
        return cell
      }
    case let .toggle(viewModel):
      if let cell = tableView.dequeueReusableCell(
        withIdentifier: SettingsViewController.toggleCellReuseId
      ) as? ToggleSettingsTableViewCell {
        cell.viewModel = viewModel
        return cell
      }
    }
    return UITableViewCell()
  }

  init(
    viewModel: SettingsViewModeling,
    getControlledPlayerVc: @escaping () -> ControlledPlayerViewController,
    getDimScreenTimeoutVc: @escaping () -> DimScreenTimeoutViewController
  ) {
    self.viewModel = viewModel
    self.getControlledPlayerVc = getControlledPlayerVc
    self.getDimScreenTimeoutVc = getDimScreenTimeoutVc
    super.init(nibName: nil, bundle: nil)
  }

  @available(*, unavailable)
  required init?(coder _: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  override func viewDidLoad() {
    super.viewDidLoad()
    configureUi()
    bindViewModel()
    bindCellTapHandling()
  }

  private func configureUi() {
    definesPresentationContext = true

    navigationItem.title = R.string.localizable.settingsViewTitle()
    navigationItem.hidesBackButton = false

    tableView.delegate = self
    tableView.tableFooterView = UIView()
    tableView.register(
      SettingsHeaderView.self,
      forHeaderFooterViewReuseIdentifier: SettingsViewController.headerViewReuseId
    )
    tableView.register(
      SettingsFooterView.self,
      forHeaderFooterViewReuseIdentifier: SettingsViewController.footerViewReuseId
    )
    tableView.register(
      ServerAddressSettingsViewCell.self,
      forCellReuseIdentifier: SettingsViewController.serverAddressCellReuseId
    )
    tableView.register(
      ServerPortSettingsViewCell.self,
      forCellReuseIdentifier: SettingsViewController.serverPortCellReuseId
    )
    tableView.register(
      ServerPasswordSettingsViewCell.self,
      forCellReuseIdentifier: SettingsViewController.serverPasswordCellReuseId
    )
    tableView.register(
      RadioSettingsTableViewCell.self,
      forCellReuseIdentifier: SettingsViewController.radioCellReuseId
    )
    tableView.register(
      ToggleSettingsTableViewCell.self,
      forCellReuseIdentifier: SettingsViewController.toggleCellReuseId
    )

    view.backgroundColor = R.color.background()
    view.addSubview(tableView)
    view.setNeedsUpdateConstraints()
  }

  override func updateViewConstraints() {
    tableView.snp.makeConstraints { make in
      make.top.bottom.leading.trailing.equalTo(view.safeAreaLayoutGuide)
    }
    super.updateViewConstraints()
  }

  private func bindViewModel() {
    viewModel.sections
      .bind(to: tableView.rx.items(dataSource: dataSource))
      .disposed(by: disposeBag)
  }

  private func bindCellTapHandling() {
    tableView.rx.itemSelected
      .bind { [weak self] index in
        guard let self = self else { return }
        self.tableView.deselectRow(at: index, animated: true)
        if self.tableView.cellForRow(at: index) as? RadioSettingsTableViewCell != nil {
          if index.section == 0 {
            let controlledPlayerVc = self.getControlledPlayerVc()
            self.navigationController?.show(controlledPlayerVc, sender: self)
          } else if index.section == 3 {
            let dimScreenTimeoutVc = self.getDimScreenTimeoutVc()
            self.navigationController?.show(dimScreenTimeoutVc, sender: self)
          }
        }
      }.disposed(by: disposeBag)
  }
}

extension SettingsViewController: UITableViewDelegate {
  func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
    let headerView = tableView.dequeueReusableHeaderFooterView(
      withIdentifier: SettingsViewController.headerViewReuseId
    ) as? SettingsHeaderView
    headerView?.headerLabel.text = dataSource.sectionModels[section].headerFooter.header
    return headerView
  }

  func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
    let footerView = tableView.dequeueReusableHeaderFooterView(
      withIdentifier: SettingsViewController.footerViewReuseId
    ) as? SettingsFooterView
    footerView?.footerLabel.text = dataSource.sectionModels[section].headerFooter.footer
    return footerView
  }

  func tableView(_: UITableView, heightForFooterInSection section: Int) -> CGFloat {
    return CGFloat(viewModel.heightForFooterInSection(section))
  }
}
