//
//  SettingsFooterView.swift
//  WPR - Wave Players Remote
//
//  Created by Tomasz Brzozowski on 07/04/2021.
//

import UIKit

class SettingsFooterView: UITableViewHeaderFooterView {
  let footerLabel =
    UILabel(textColor: R.color.greyedOut(), font: .wprFont(size: 15), backgroundColor: R.color.background())

  override init(reuseIdentifier: String?) {
    super.init(reuseIdentifier: reuseIdentifier)
    configureUi()
  }

  private func configureUi() {
    footerLabel.translatesAutoresizingMaskIntoConstraints = false
    footerLabel.lineBreakMode = .byWordWrapping
    footerLabel.numberOfLines = 0

    contentView.backgroundColor = R.color.background()
    contentView.addSubview(footerLabel)

    let sideInset = CGFloat(Dimension.viewMarginPaddingLarge)
    let topInset = CGFloat(-Dimension.viewMarginPaddingNormal)
    NSLayoutConstraint.activate([
      footerLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: sideInset),
      footerLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -sideInset),
      footerLabel.topAnchor.constraint(equalTo: contentView.topAnchor, constant: -topInset),
    ])
  }

  @available(*, unavailable)
  required init?(coder _: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
}
