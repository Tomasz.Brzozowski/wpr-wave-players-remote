//
//  SettingsHeaderView.swift
//  WPR - Wave Players Remote
//
//  Created by Tomasz Brzozowski on 22/03/2021.
//

import UIKit

class SettingsHeaderView: UITableViewHeaderFooterView {
  let headerLabel =
    UILabel(textColor: R.color.greyedOut(), font: .wprFont(size: 15), backgroundColor: R.color.background())

  override init(reuseIdentifier: String?) {
    super.init(reuseIdentifier: reuseIdentifier)
    configureUi()
  }

  private func configureUi() {
    headerLabel.translatesAutoresizingMaskIntoConstraints = false

    contentView.backgroundColor = R.color.background()
    contentView.addSubview(headerLabel)

    let leadingInset = CGFloat(Dimension.viewMarginPaddingLarge)
    let bottomInset = CGFloat(-Dimension.viewMarginPaddingNormal)
    NSLayoutConstraint.activate([
      headerLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: leadingInset),
      headerLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
      headerLabel.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: bottomInset),
    ])
  }

  @available(*, unavailable)
  required init?(coder _: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
}
