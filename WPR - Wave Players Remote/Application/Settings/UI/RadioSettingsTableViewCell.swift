//
//  RadioSettingsTableViewCell.swift
//  WPR - Wave Players Remote
//
//  Created by Tomasz Brzozowski on 18/03/2021.
//

import RxRelay
import RxSwift
import UIKit

class RadioSettingsTableViewCell: UITableViewCell {
  var viewModel: RadioSettingsCellViewModeling? {
    didSet {
      bindViewModel()
    }
  }

  private let nameLabel = UILabel(font: .wprFont(size: 19))
  private let valueDescriptionLabel = UILabel(textAlignment: .right, font: .wprFont(size: 19))
  private let detailImageView: UIImageView = {
    let imageView = UIImageView(image: R.image.settings.icon_detail())
    return imageView
  }()

  private let disposeBag = DisposeBag()

  private func bindViewModel() {
    backgroundColor = R.color.surface()

    if let viewModel = viewModel {
      nameLabel.text = "\(viewModel.name)"
      bind(relay: viewModel.description, to: valueDescriptionLabel)
    }
  }

  private func bind(relay: BehaviorRelay<String>, to label: UILabel) {
    relay.asObservable()
      .bind(to: label.rx.text)
      .disposed(by: disposeBag)
  }

  override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
    super.init(style: style, reuseIdentifier: reuseIdentifier)
    configureUi()
  }

  private func configureUi() {
    contentView.addSubview(nameLabel)
    contentView.addSubview(valueDescriptionLabel)
    contentView.addSubview(detailImageView)
    setNeedsUpdateConstraints()
  }

  override func updateConstraints() {
    nameLabel.snp.makeConstraints { make in
      make.top.bottom.equalTo(contentView.safeAreaLayoutGuide).inset(Dimension.viewMarginPaddingLarge)
      make.leading.equalTo(contentView.safeAreaLayoutGuide).inset(Dimension.viewMarginPaddingLarge)
    }
    valueDescriptionLabel.snp.makeConstraints { make in
      make.top.bottom.equalTo(contentView.safeAreaLayoutGuide).inset(Dimension.viewMarginPaddingLarge)
      make.leading.equalTo(contentView.snp.centerX)
      make.trailing.equalTo(detailImageView.snp.leading).inset(-Dimension.viewMarginPaddingNormal)
    }
    detailImageView.snp.makeConstraints { make in
      make.top.bottom.equalTo(contentView.safeAreaLayoutGuide).inset(Dimension.viewMarginPaddingLarge)
      make.trailing.equalTo(contentView.safeAreaLayoutGuide).inset(Dimension.viewMarginPaddingLarge)
    }
    super.updateConstraints()
  }

  @available(*, unavailable)
  required init?(coder _: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
}
