//
//  ToggleSettingsTableViewCell.swift
//  WPR - Wave Players Remote
//
//  Created by Tomasz Brzozowski on 19/03/2021.
//

import RxRelay
import RxSwift
import UIKit

class ToggleSettingsTableViewCell: UITableViewCell {
  var viewModel: ToggleSettingsCellViewModeling? {
    didSet {
      bindViewModel()
    }
  }

  private let nameLabel = UILabel(font: .wprFont(size: 19))
  private let toggleSwitch: UISwitch = {
    let toggleSwitch = UISwitch()
    toggleSwitch.thumbTintColor = R.color.onPrimary()
    return toggleSwitch
  }()

  private let disposeBag = DisposeBag()

  private func bindViewModel() {
    backgroundColor = R.color.surface()

    guard let viewModel = viewModel else { return }

    nameLabel.text = "\(viewModel.name)"

    viewModel.state
      .bind { [weak self] state in
        self?.toggleSwitch.isOn = state
      }.disposed(by: disposeBag)

    toggleSwitch.rx.controlEvent(.valueChanged)
      .bind { [weak self] _ in
        self?.viewModel?.setSettingValue()
      }.disposed(by: disposeBag)
  }

  override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
    super.init(style: style, reuseIdentifier: reuseIdentifier)
    configureUi()
  }

  private func configureUi() {
    selectionStyle = .none

    contentView.addSubview(nameLabel)
    contentView.addSubview(toggleSwitch)
    setNeedsUpdateConstraints()
  }

  override func updateConstraints() {
    nameLabel.snp.makeConstraints { make in
      make.top.bottom.equalTo(contentView.safeAreaLayoutGuide).inset(Dimension.viewMarginPaddingLarge)
      make.leading.equalTo(contentView.safeAreaLayoutGuide).inset(Dimension.viewMarginPaddingLarge)
    }
    toggleSwitch.snp.makeConstraints { make in
      make.top.bottom.equalTo(contentView.safeAreaLayoutGuide).inset(Dimension.viewMarginPaddingLarge)
      make.trailing.equalTo(contentView.safeAreaLayoutGuide).inset(Dimension.viewMarginPaddingLarge)
    }
    super.updateConstraints()
  }

  @available(*, unavailable)
  required init?(coder _: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
}
