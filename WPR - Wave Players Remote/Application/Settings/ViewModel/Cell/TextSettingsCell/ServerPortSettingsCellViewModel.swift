//
//  ServerPortSettingsCellViewModel.swift
//  WPR - Wave Players Remote
//
//  Created by Tomasz Brzozowski on 25/06/2021.
//

import RxRelay

class ServerPortSettingsCellViewModel:
  ValidatingTextSettingsViewModel,
  TextSettingsCellViewModel,
  ServerPortCellViewModeling {

  let name = R.string.localizable.serverPortSettingName()
  let text = BehaviorRelay<String>(value: "")

  override init(userDefaultsService: UserDefaultsProvider, textValidationService: TextValidationProvider) {
    super.init(userDefaultsService: userDefaultsService, textValidationService: textValidationService)
    userDefaultsService.serverPortObservable()
      .bind { [weak self] serverPort in
        self?.text.accept(String(serverPort))
      }.disposed(by: disposeBag)
  }

  func validateInputWhileTyping(_ text: String) {
    self.text.accept(validatedPortWhileTyping(text))
  }

  func setSettingValue(for text: String) {
    if let port = Int(text), textValidationService.validatePortText(text) {
      userDefaultsService.setServerPort(port)
    } else {
      self.text.accept(String(userDefaultsService.serverPort()))
    }
  }
}
