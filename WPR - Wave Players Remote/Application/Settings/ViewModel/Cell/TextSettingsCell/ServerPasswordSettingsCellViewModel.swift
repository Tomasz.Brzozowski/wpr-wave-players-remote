//
//  ServerPasswordSettingsCellViewModel.swift
//  WPR - Wave Players Remote
//
//  Created by Tomasz Brzozowski on 25/06/2021.
//

import RxRelay

class ServerPasswordSettingsCellViewModel: SettingsCellViewModel, TextSettingsCellViewModel {
  let name = R.string.localizable.serverPasswordSettingName()
  let text = BehaviorRelay<String>(value: "")

  override init(userDefaultsService: UserDefaultsProvider) {
    super.init(userDefaultsService: userDefaultsService)
    userDefaultsService.serverPasswordObservable()
      .bind { [weak self] serverPassword in
        self?.text.accept(serverPassword)
      }.disposed(by: disposeBag)
  }

  func validateInputWhileTyping(_: String) {}

  func setSettingValue(for text: String) {
    userDefaultsService.setServerPassword(text)
  }
}
