//
//  ValidatedTextViewModel.swift
//  WPR - Wave Players Remote
//
//  Created by Tomasz Brzozowski on 25/06/2021.
//

import RxSwift

class ValidatingTextSettingsViewModel: SettingsCellViewModel {
  let textValidationService: TextValidationProvider

  init(userDefaultsService: UserDefaultsProvider, textValidationService: TextValidationProvider) {
    self.textValidationService = textValidationService
    super.init(userDefaultsService: userDefaultsService)
  }
}
