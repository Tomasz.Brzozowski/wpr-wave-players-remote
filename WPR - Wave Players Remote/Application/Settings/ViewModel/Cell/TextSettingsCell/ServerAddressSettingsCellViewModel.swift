//
//  ServerAddressSettingsCellViewModel.swift
//  WPR - Wave Players Remote
//
//  Created by Tomasz Brzozowski on 25/06/2021.
//

import RxRelay

class ServerAddressSettingsCellViewModel:
  ValidatingTextSettingsViewModel,
  TextSettingsCellViewModel,
  ServerAddressCellViewModel {

  let name = R.string.localizable.serverAddressSettingName()
  let text = BehaviorRelay<String>(value: "")

  override init(userDefaultsService: UserDefaultsProvider, textValidationService: TextValidationProvider) {
    super.init(userDefaultsService: userDefaultsService, textValidationService: textValidationService)
    userDefaultsService.serverAddressObservable()
      .bind { [weak self] serverAddress in
        self?.text.accept(serverAddress)
      }.disposed(by: disposeBag)
  }

  func validateInputWhileTyping(_ text: String) {
    self.text.accept(validatedIpWhileTyping(text))
  }

  func setSettingValue(for text: String) {
    if textValidationService.validateIp(text) {
      userDefaultsService.setServerAddress(text)
    } else {
      self.text.accept(userDefaultsService.serverAddress())
    }
  }
}
