//
//  DimScreenTimeoutSettingsCellViewModel.swift
//  WPR - Wave Players Remote
//
//  Created by Tomasz Brzozowski on 25/06/2021.
//

import RxRelay

class DimScreenTimeoutSettingsCellViewModel: SettingsCellViewModel, RadioSettingsCellViewModeling {
  let name = R.string.localizable.dimScreenTimeoutSettingName()
  let description = BehaviorRelay<String>(value: "")

  override init(userDefaultsService: UserDefaultsProvider) {
    super.init(userDefaultsService: userDefaultsService)
    userDefaultsService.dimScreenTimeoutObservable()
      .bind { [weak self] dimScreenTimeout in
        self?.description.accept(dimScreenTimeout.description)
      }.disposed(by: disposeBag)
  }
}
