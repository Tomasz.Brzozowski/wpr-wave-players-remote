//
//  RadioSettingsCellViewModeling.swift
//  WPR - Wave Players Remote
//
//  Created by Tomasz Brzozowski on 21/05/2021.
//

import RxRelay

protocol RadioSettingsCellViewModeling {
  var name: String { get }
  var description: BehaviorRelay<String> { get }
}
