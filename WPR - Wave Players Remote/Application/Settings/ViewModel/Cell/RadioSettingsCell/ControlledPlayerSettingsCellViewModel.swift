//
//  ControlledPlayerSettingsCellViewModel.swift
//  WPR - Wave Players Remote
//
//  Created by Tomasz Brzozowski on 25/06/2021.
//

import RxRelay

class ControlledPlayerSettingsCellViewModel: SettingsCellViewModel, RadioSettingsCellViewModeling {
  let name = R.string.localizable.controlledPlayerSettingName()
  let description = BehaviorRelay<String>(value: "")

  override init(userDefaultsService: UserDefaultsProvider) {
    super.init(userDefaultsService: userDefaultsService)
    userDefaultsService.controlledPlayerObservable()
      .bind { [weak self] controlledPlayer in
        self?.description.accept(controlledPlayer.description)
      }.disposed(by: disposeBag)
  }
}
