//
//  SettingsCellViewModel.swift
//  WPR - Wave Players Remote
//
//  Created by Tomasz Brzozowski on 19/04/2021.
//

import RxSwift

class SettingsCellViewModel {
  let userDefaultsService: UserDefaultsProvider
  let disposeBag = DisposeBag()

  init(userDefaultsService: UserDefaultsProvider) {
    self.userDefaultsService = userDefaultsService
  }
}
