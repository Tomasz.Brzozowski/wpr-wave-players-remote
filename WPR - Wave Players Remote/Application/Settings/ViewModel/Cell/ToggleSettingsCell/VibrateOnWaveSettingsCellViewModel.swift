//
//  VibrateOnWaveSettingsCellViewModel.swift
//  WPR - Wave Players Remote
//
//  Created by Tomasz Brzozowski on 25/06/2021.
//

import RxRelay

class VibrateOnWaveSettingsCellViewModel: SettingsCellViewModel, ToggleSettingsCellViewModeling {
  let name = R.string.localizable.vibrateOnWaveSettingName()
  let state = BehaviorRelay<Bool>(value: false)

  override init(userDefaultsService: UserDefaultsProvider) {
    super.init(userDefaultsService: userDefaultsService)
    userDefaultsService.vibrateOnWaveObservable()
      .bind { [weak self] value in
        self?.state.accept(value)
      }.disposed(by: disposeBag)
  }

  func setSettingValue() {
    userDefaultsService.setVibrateOnWave(!state.value)
  }
}
