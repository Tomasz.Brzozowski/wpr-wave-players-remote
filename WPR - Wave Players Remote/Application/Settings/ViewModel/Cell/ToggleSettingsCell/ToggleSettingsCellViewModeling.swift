//
//  ToggleSettingsCellViewModeling.swift
//  WPR - Wave Players Remote
//
//  Created by Tomasz Brzozowski on 21/05/2021.
//

import RxRelay

protocol ToggleSettingsCellViewModeling {
  var name: String { get }
  var state: BehaviorRelay<Bool> { get }

  func setSettingValue()
}
