//
//  PauseOnCallSettingsCellViewModel.swift
//  WPR - Wave Players Remote
//
//  Created by Tomasz Brzozowski on 19/04/2021.
//

import RxRelay

class PauseOnCallSettingsCellViewModel: SettingsCellViewModel, ToggleSettingsCellViewModeling {
  let name = R.string.localizable.pauseOnIncomingCallSettingName()
  let state = BehaviorRelay<Bool>(value: false)

  override init(userDefaultsService: UserDefaultsProvider) {
    super.init(userDefaultsService: userDefaultsService)
    userDefaultsService.pauseOnIncomingCallObservable()
      .bind { [weak self] value in
        self?.state.accept(value)
      }.disposed(by: disposeBag)
  }

  func setSettingValue() {
    userDefaultsService.setPauseOnIncomingCall(!state.value)
  }
}
