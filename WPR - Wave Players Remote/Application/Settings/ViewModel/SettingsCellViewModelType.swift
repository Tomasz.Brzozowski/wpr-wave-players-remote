//
//  SettingsCellViewModelType.swift
//  WPR - Wave Players Remote
//
//  Created by Tomasz Brzozowski on 19/04/2021.
//

enum SettingsCellViewModelType {
  case serverAddress(TextSettingsCellViewModel)
  case serverPort(TextSettingsCellViewModel)
  case text(TextSettingsCellViewModel)
  case radio(RadioSettingsCellViewModeling)
  case toggle(ToggleSettingsCellViewModeling)
}
