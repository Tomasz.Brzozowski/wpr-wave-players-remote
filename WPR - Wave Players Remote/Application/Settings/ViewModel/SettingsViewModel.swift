//
//  SettingsViewModel.swift
//  WPR - Wave Players Remote
//
//  Created by Tomasz Brzozowski on 16/03/2021.
//

import RxRelay

class SettingsViewModel: SettingsViewModeling {
  let sections: BehaviorRelay<[SectionOfSettings]>

  private let getServerAddressCellViewModel: () -> TextSettingsCellViewModel
  private let getServerPortCellViewModel: () -> TextSettingsCellViewModel
  private let getServerPasswordCellViewModel: () -> TextSettingsCellViewModel
  private let getControlledPlayerCellViewModel: () -> RadioSettingsCellViewModeling
  private let getVibrateOnWaveCellViewModel: () -> ToggleSettingsCellViewModeling
  private let getPauseOnCallCellViewModel: () -> ToggleSettingsCellViewModeling
  private let getDimScreenTimeoutCellViewModel: () -> RadioSettingsCellViewModeling

  init(
    getServerAddressCellViewModel: @escaping () -> TextSettingsCellViewModel,
    getServerPortCellViewModel: @escaping () -> TextSettingsCellViewModel,
    getServerPasswordCellViewModel: @escaping () -> TextSettingsCellViewModel,
    getControlledPlayerCellViewModel: @escaping () -> RadioSettingsCellViewModeling,
    getVibrateOnWaveCellViewModel: @escaping () -> ToggleSettingsCellViewModeling,
    getPauseOnCallCellViewModel: @escaping () -> ToggleSettingsCellViewModeling,
    getDimScreenTimeoutCellViewModel: @escaping () -> RadioSettingsCellViewModeling
  ) {
    self.getServerAddressCellViewModel = getServerAddressCellViewModel
    self.getServerPortCellViewModel = getServerPortCellViewModel
    self.getServerPasswordCellViewModel = getServerPasswordCellViewModel
    self.getControlledPlayerCellViewModel = getControlledPlayerCellViewModel
    self.getVibrateOnWaveCellViewModel = getVibrateOnWaveCellViewModel
    self.getPauseOnCallCellViewModel = getPauseOnCallCellViewModel
    self.getDimScreenTimeoutCellViewModel = getDimScreenTimeoutCellViewModel

    sections = BehaviorRelay<[SectionOfSettings]>(value: [
      SectionOfSettings(headerFooter: .connection, items: [
        .serverAddress(getServerAddressCellViewModel()),
        .serverPort(getServerPortCellViewModel()),
        .text(getServerPasswordCellViewModel()),
        .radio(getControlledPlayerCellViewModel()),
      ]),
      SectionOfSettings(headerFooter: .vibration, items: [
        .toggle(getVibrateOnWaveCellViewModel()),
      ]),
      SectionOfSettings(headerFooter: .playback, items: [
        .toggle(getPauseOnCallCellViewModel()),
      ]),
      SectionOfSettings(headerFooter: .screenDimming, items: [
        .radio(getDimScreenTimeoutCellViewModel()),
      ]),
    ])
  }

  func heightForFooterInSection(_ section: Int) -> Int {
    return sections.value[section].headerFooter.footerHeight
  }
}
