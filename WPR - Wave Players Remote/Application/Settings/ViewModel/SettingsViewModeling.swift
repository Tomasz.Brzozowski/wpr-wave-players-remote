//
//  SettingsViewModeling.swift
//  WPR - Wave Players Remote
//
//  Created by Tomasz Brzozowski on 20/05/2021.
//

import RxRelay

protocol SettingsViewModeling {
  var sections: BehaviorRelay<[SectionOfSettings]> { get }

  func heightForFooterInSection(_ section: Int) -> Int
}
