//
//  SectionOfSettings.swift
//  WPR - Wave Players Remote
//
//  Created by Tomasz Brzozowski on 07/04/2021.
//

import RxDataSources

struct SectionOfSettings {
  var headerFooter: SettingsHeaderFooter
  var items: [Item]
}

extension SectionOfSettings: SectionModelType {
  typealias Item = SettingsCellViewModelType

  init(original: SectionOfSettings, items: [Item]) {
    self = original
    self.items = items
  }
}
