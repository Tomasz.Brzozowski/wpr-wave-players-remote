//
//  SettingsHeaderFooter.swift
//  WPR - Wave Players Remote
//
//  Created by Tomasz Brzozowski on 12/04/2021.
//

enum SettingsHeaderFooter: String {
  case connection
  case vibration
  case playback
  case screenDimming

  var header: String {
    switch self {
    case .connection: return R.string.localizable.connectionSettingsSectionHeader()
    case .vibration: return R.string.localizable.vibrationSettingsSectionHeader()
    case .playback: return R.string.localizable.playbackSettingsSectionHeader()
    case .screenDimming: return R.string.localizable.screenDimmingSettingsSectionHeader()
    }
  }

  var footer: String {
    switch self {
    case .connection: return ""
    case .vibration: return ""
    case .playback: return R.string.localizable.playbackSettingsSectionFooter()
    case .screenDimming: return R.string.localizable.screenDimmingSettingsSectionFooter()
    }
  }

  var footerHeight: Int {
    switch self {
    case .connection: return 0
    case .vibration: return 0
    case .playback: return 60
    case .screenDimming: return 60
    }
  }
}
