//
//  BaseRadioSettingCell.swift
//  WPR - Wave Players Remote
//
//  Created by Tomasz Brzozowski on 22/04/2021.
//

class BaseRadioSettingCell {
  let setting: Any
  let description: String
  let selected: Bool

  init(setting: Any, description: String, selected: Bool) {
    self.setting = setting
    self.description = description
    self.selected = selected
  }
}
