//
//  RadioSettingCell.swift
//  WPR - Wave Players Remote
//
//  Created by Tomasz Brzozowski on 15/04/2021.
//

protocol RadioSettingCell {
  var setting: Any { get }
  var description: String { get }
  var selected: Bool { get }
}
