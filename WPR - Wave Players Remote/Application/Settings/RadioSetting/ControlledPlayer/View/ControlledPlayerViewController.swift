//
//  ControlledPlayerViewController.swift
//  WPR - Wave Players Remote
//
//  Created by Tomasz Brzozowski on 25/03/2021.
//

class ControlledPlayerViewController: RadioSettingViewController {
  override func viewDidLoad() {
    super.viewDidLoad()
    navigationItem.title = R.string.localizable.controlledPlayerViewTitle()
  }

}
