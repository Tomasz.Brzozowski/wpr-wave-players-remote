//
//  MediaPlayer.swift
//  WPR - Wave Players Remote
//
//  Created by Tomasz Brzozowski on 16/03/2021.
//

extension MediaPlayer {
  var description: String {
    switch self {
    case .mpc: return "MPC-HC"
    case .vlc: return "VLC"
    }
  }
}
