//
//  ControlledPlayerCell.swift
//  WPR - Wave Players Remote
//
//  Created by Tomasz Brzozowski on 22/04/2021.
//

class ControlledPlayerCell: BaseRadioSettingCell, RadioSettingCell {
  init(setting: MediaPlayer, selected: Bool) {
    super.init(setting: setting, description: setting.description, selected: selected)
  }
}
