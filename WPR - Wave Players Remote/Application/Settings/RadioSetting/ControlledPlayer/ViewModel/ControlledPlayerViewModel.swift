//
//  ControlledPlayerViewModel.swift
//  WPR - Wave Players Remote
//
//  Created by Tomasz Brzozowski on 25/03/2021.
//

import RxRelay
import RxSwift

class ControlledPlayerViewModel: SettingsCellViewModel, RadioSettingViewModel {
  let cellViewModels = BehaviorRelay<[RadioSettingCell]>(value: [])

  override init(userDefaultsService: UserDefaultsProvider) {
    super.init(userDefaultsService: userDefaultsService)

    userDefaultsService.controlledPlayerObservable().bind { [weak self] controlledPlayer in
      let cellViewModels = [
        ControlledPlayerCell(setting: .mpc, selected: controlledPlayer == .mpc ? true : false),
        ControlledPlayerCell(setting: .vlc, selected: controlledPlayer == .vlc ? true : false),
      ]
      self?.cellViewModels.accept(cellViewModels)
    }.disposed(by: disposeBag)
  }

  func setSettingValue(_ setting: Any) {
    guard let setting = setting as? MediaPlayer else { return }
    userDefaultsService.setControlledPlayer(setting)
  }
}
