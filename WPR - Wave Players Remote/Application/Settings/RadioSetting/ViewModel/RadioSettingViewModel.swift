//
//  RadioSettingViewModel.swift
//  WPR - Wave Players Remote
//
//  Created by Tomasz Brzozowski on 15/04/2021.
//

import RxRelay

protocol RadioSettingViewModel {
  var cellViewModels: BehaviorRelay<[RadioSettingCell]> { get }

  func setSettingValue(_ setting: Any)
}
