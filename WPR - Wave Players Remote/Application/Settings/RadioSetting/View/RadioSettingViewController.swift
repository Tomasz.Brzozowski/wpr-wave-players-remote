//
//  RadioSettingViewController.swift
//  WPR - Wave Players Remote
//
//  Created by Tomasz Brzozowski on 15/04/2021.
//

import RxSwift
import UIKit

class RadioSettingViewController: UIViewController {
  private static let checkmarkCellReuseId = "checkmarkCell"

  private let tableView: UITableView = {
    let bounds = UIScreen.main.bounds
    let tableView = UITableView(frame: CGRect(x: 0, y: 0, width: bounds.width, height: bounds.height), style: .grouped)
    tableView.backgroundColor = R.color.background()
    tableView.dragDelegate = nil
    tableView.sectionHeaderHeight = 50
    tableView.sectionFooterHeight = 0
    return tableView
  }()

  private let viewModel: RadioSettingViewModel
  private let disposeBag = DisposeBag()

  init(viewModel: RadioSettingViewModel) {
    self.viewModel = viewModel
    super.init(nibName: nil, bundle: nil)
  }

  @available(*, unavailable)
  required init?(coder _: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  override func viewDidLoad() {
    super.viewDidLoad()
    configureUi()
    bindViewModel()
    bindCellTapHandling()
  }

  private func configureUi() {
    view.backgroundColor = R.color.background()

    tableView.tableFooterView = UIView()
    tableView.register(UITableViewCell.self, forCellReuseIdentifier: RadioSettingViewController.checkmarkCellReuseId)

    navigationItem.hidesBackButton = false

    view.addSubview(tableView)
    view.setNeedsUpdateConstraints()
  }

  override func updateViewConstraints() {
    tableView.snp.makeConstraints { make in
      make.bottom.leading.trailing.top.equalTo(view.safeAreaLayoutGuide)
    }
    super.updateViewConstraints()
  }

  private func bindViewModel() {
    tableView.dataSource = nil
    viewModel.cellViewModels
      .bind(to: tableView.rx.items) { tableView, index, element -> UITableViewCell in
        let indexPath = IndexPath(item: index, section: 0)
        let cell = tableView.dequeueReusableCell(
          withIdentifier: RadioSettingViewController.checkmarkCellReuseId,
          for: indexPath
        )
        cell.textLabel?.font = UIFont.wprFont(size: 19)
        cell.textLabel?.text = element.description
        cell.accessoryType = element.selected == true ? .checkmark : .none
        cell.selectionStyle = .none
        return cell
      }.disposed(by: disposeBag)
  }

  private func bindCellTapHandling() {
    tableView.rx.modelSelected(RadioSettingCell.self)
      .subscribe { [weak self] cell in
        if let viewModel = self?.viewModel, let element = cell.element {
          viewModel.setSettingValue(element.setting)
        }
      }.disposed(by: disposeBag)
  }
}
