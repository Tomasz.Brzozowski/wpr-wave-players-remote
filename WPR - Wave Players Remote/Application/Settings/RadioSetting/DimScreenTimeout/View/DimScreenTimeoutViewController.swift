//
//  DimScreenTimeoutViewController.swift
//  WPR - Wave Players Remote
//
//  Created by Tomasz Brzozowski on 07/04/2021.
//

class DimScreenTimeoutViewController: RadioSettingViewController {
  override func viewDidLoad() {
    super.viewDidLoad()
    navigationItem.title = R.string.localizable.dimScreenTimeoutViewTitle()
  }
}
