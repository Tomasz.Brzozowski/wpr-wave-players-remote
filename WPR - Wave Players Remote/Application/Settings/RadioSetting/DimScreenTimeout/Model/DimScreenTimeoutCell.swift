//
//  DimScreenTimeoutCell.swift
//  WPR - Wave Players Remote
//
//  Created by Tomasz Brzozowski on 22/04/2021.
//

class DimScreenTimeoutCell: BaseRadioSettingCell, RadioSettingCell {
  init(setting: DimScreenTimeout, selected: Bool) {
    super.init(setting: setting, description: setting.description, selected: selected)
  }
}
