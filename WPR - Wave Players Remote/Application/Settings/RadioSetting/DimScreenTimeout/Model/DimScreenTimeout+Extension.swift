//
//  DimScreenTimeout+Extension.swift
//  WPR - Wave Players Remote
//
//  Created by Tomasz Brzozowski on 07/04/2021.
//

extension DimScreenTimeout {
  var description: String {
    switch self {
    case .five: return R.string.localizable.fiveSecondsDimScreenTimeout()
    case .ten: return R.string.localizable.tenSecondsDimScreenTimeout()
    case .thirty: return R.string.localizable.thirtySecondsDimScreenTimeout()
    case .oneMinute: return R.string.localizable.oneMinuteDimScreenTimeout()
    case .twoMinutes: return R.string.localizable.twoMinutesDimScreenTimeout()
    case .threeMinutes: return R.string.localizable.threeMinutesDimScreenTimeout()
    case .never: return R.string.localizable.neverDimScreenTimeout()
    }
  }
}
