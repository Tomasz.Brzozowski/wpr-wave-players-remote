//
//  DimScreenTimeoutViewModel.swift
//  WPR - Wave Players Remote
//
//  Created by Tomasz Brzozowski on 07/04/2021.
//

import RxRelay
import RxSwift

class DimScreenTimeoutViewModel: SettingsCellViewModel, RadioSettingViewModel {
  let cellViewModels = BehaviorRelay<[RadioSettingCell]>(value: [])

  override init(userDefaultsService: UserDefaultsProvider) {
    super.init(userDefaultsService: userDefaultsService)

    userDefaultsService.dimScreenTimeoutObservable().bind(onNext: { [weak self] dimScreenTimeout in
      let cellViewModels = [
        DimScreenTimeoutCell(setting: .five, selected: dimScreenTimeout == .five ? true : false),
        DimScreenTimeoutCell(setting: .ten, selected: dimScreenTimeout == .ten ? true : false),
        DimScreenTimeoutCell(setting: .thirty, selected: dimScreenTimeout == .thirty ? true : false),
        DimScreenTimeoutCell(setting: .oneMinute, selected: dimScreenTimeout == .oneMinute ? true : false),
        DimScreenTimeoutCell(setting: .twoMinutes, selected: dimScreenTimeout == .twoMinutes ? true : false),
        DimScreenTimeoutCell(setting: .threeMinutes, selected: dimScreenTimeout == .threeMinutes ? true : false),
        DimScreenTimeoutCell(setting: .never, selected: dimScreenTimeout == .never ? true : false),
      ]
      self?.cellViewModels.accept(cellViewModels)
    }).disposed(by: disposeBag)
  }

  func setSettingValue(_ setting: Any) {
    guard let setting = setting as? DimScreenTimeout else { return }
    userDefaultsService.setDimScreenTimeout(setting)
  }
}
