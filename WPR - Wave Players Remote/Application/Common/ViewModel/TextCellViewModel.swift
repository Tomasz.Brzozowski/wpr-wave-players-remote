//
//  TextCellViewModel.swift
//  WPR - Wave Players Remote
//
//  Created by Tomasz Brzozowski on 12/05/2021.
//

import RxRelay

protocol TextCellViewModel {
  var name: String { get }
  var text: BehaviorRelay<String> { get }

  func validateInputWhileTyping(_ text: String)
}

protocol TextSettingsCellViewModel: TextCellViewModel {
  func setSettingValue(for text: String)
}
