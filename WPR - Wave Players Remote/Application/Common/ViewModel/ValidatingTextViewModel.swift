//
//  ValidatingTextViewModel.swift
//  WPR - Wave Players Remote
//
//  Created by Tomasz Brzozowski on 01/07/2021.
//

protocol ValidatingTextViewModel {
  var textValidationService: TextValidationProvider { get }
}

protocol ServerAddressCellViewModel: ValidatingTextViewModel {}

extension ServerAddressCellViewModel {
  func validatedIpWhileTyping(_ text: String) -> String {
    if textValidationService.validateIpWhileTyping(text) {
      return text
    } else if text != "" {
      return String(textValidationService.textWithoutLastCharacter(from: text))
    } else {
      return ""
    }
  }
}

protocol ServerPortCellViewModeling: ValidatingTextViewModel {}

extension ServerPortCellViewModeling {
  func validatedPortWhileTyping(_ text: String) -> String {
    if textValidationService.validatePortText(text) {
      return text
    } else if text != "" {
      return String(textValidationService.textWithoutLastCharacter(from: text))
    } else {
      return ""
    }
  }
}
