//
//  Dimensions.swift
//  WPR - Wave Players Remote
//
//  Created by Tomasz Brzozowski on 10/11/2020.
//

enum Dimension {
  static let viewMarginPaddingNormal = 8
  static let viewMarginPaddingLarge = 16
}
