//
//  TextSettingsTableViewCell.swift
//  WPR - Wave Players Remote
//
//  Created by Tomasz Brzozowski on 28/06/2021.
//
//

class TextSettingsTableViewCell<Type>: TextTableViewCell<Type> where Type: TextSettingsCellViewModel {
  override var viewModel: Type? {
    didSet {
      super.bindViewModel()
      valueTextField.rx.controlEvent(.editingDidEnd)
        .bind { [weak self] _ in
          self?.viewModel?.setSettingValue(for: self?.valueTextField.text ?? "")
        }.disposed(by: disposeBag)
    }
  }
}
