//
//  TextTableViewCell.swift
//  WPR - Wave Players Remote
//
//  Created by Tomasz Brzozowski on 12/05/2021.
//

import RxSwift
import UIKit

class TextTableViewCell<ViewModelType>: UITableViewCell where ViewModelType: TextCellViewModel {
  let valueTextField: UITextField = {
    let textField = UITextField()
    textField.textColor = R.color.onSurface()
    textField.textAlignment = .right
    textField.font = .wprFont(size: 19)
    return textField
  }()

  let disposeBag = DisposeBag()

  var viewModel: ViewModelType? {
    didSet {
      bindViewModel()
    }
  }

  private let nameLabel = UILabel(font: .wprFont(size: 19))

  func bindViewModel() {
    backgroundColor = R.color.surface()

    guard let viewModel = self.viewModel else { return }

    nameLabel.text = "\(viewModel.name)"

    viewModel.text
      .bind(to: valueTextField.rx.text)
      .disposed(by: disposeBag)

    valueTextField.rx.controlEvent(.editingDidEndOnExit)
      .bind { [weak self] in
        self?.valueTextField.resignFirstResponder()
      }.disposed(by: disposeBag)

    valueTextField.placeholder = nameLabel.text ?? ""

    valueTextField.rx.text.changed
      .bind { text in
        viewModel.validateInputWhileTyping(text ?? "")
      }.disposed(by: disposeBag)
  }

  override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
    super.init(style: style, reuseIdentifier: reuseIdentifier)
    configureUi()
  }

  private func configureUi() {
    selectionStyle = .none

    contentView.addSubview(nameLabel)
    contentView.addSubview(valueTextField)
    setNeedsUpdateConstraints()
  }

  override func updateConstraints() {
    nameLabel.snp.makeConstraints { make in
      make.top.bottom.equalTo(contentView.safeAreaLayoutGuide).inset(Dimension.viewMarginPaddingLarge)
      make.leading.equalTo(contentView.safeAreaLayoutGuide).inset(Dimension.viewMarginPaddingLarge)
    }
    valueTextField.snp.makeConstraints { make in
      make.top.bottom.equalTo(contentView.safeAreaLayoutGuide).inset(Dimension.viewMarginPaddingLarge)
      make.trailing.equalTo(contentView.safeAreaLayoutGuide).inset(Dimension.viewMarginPaddingLarge)
      make.leading.equalTo(contentView.snp.centerX)
    }
    super.updateConstraints()
  }

  @available(*, unavailable)
  required init?(coder _: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
}
