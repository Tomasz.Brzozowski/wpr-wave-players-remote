//
//  UserNotificationsService.swift
//  WPR - Wave Players Remote
//
//  Created by Tomasz Brzozowski on 28/04/2021.
//

import Foundation
import RxSwift

class UserNotificationsService: UserNotificationsProvider {
  func didBecomeActiveNotification() -> Observable<Void> {
    return NotificationCenter.default.rx.notification(UIApplication.didBecomeActiveNotification).map { _ in }
  }

  func willResignActiveNotification() -> Observable<Void> {
    return NotificationCenter.default.rx.notification(UIApplication.willResignActiveNotification).map { _ in }
  }
}
