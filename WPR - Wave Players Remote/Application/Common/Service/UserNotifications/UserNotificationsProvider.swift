//
//  UserNotificationsProvider.swift
//  WPR - Wave Players Remote
//
//  Created by Tomasz Brzozowski on 28/04/2021.
//

import RxSwift

protocol UserNotificationsProvider {
  func didBecomeActiveNotification() -> Observable<Void>
  func willResignActiveNotification() -> Observable<Void>
}
