//
//  MpcTarget.swift
//  WPR - Wave Players Remote
//
//  Created by Tomasz Brzozowski on 02/12/2020.
//

import Moya

enum MpcTarget {
  case status(_ address: String, _ port: Int, _ password: String)
  case setVolume(_ volume: String, _ address: String, _ port: Int, _ password: String)
  case seek(_ positionString: String, _ address: String, _ port: Int, _ password: String)
  case repeatPlayForever(_ address: String, _ port: Int, _ password: String)
  case repeatPlayOneFile(_ address: String, _ port: Int, _ password: String)
  case repeatPlayWholePlaylist(_ address: String, _ port: Int, _ password: String)
  case toggleFullscreen(_ address: String, _ port: Int, _ password: String)
  case pause(_ address: String, _ port: Int, _ password: String)
  case togglePlayPause(_ address: String, _ port: Int, _ password: String)
  case previousFile(_ address: String, _ port: Int, _ password: String)
  case nextFile(_ address: String, _ port: Int, _ password: String)
  case nextAudioTrack(_ address: String, _ port: Int, _ password: String)
  case nextSubtitleTrack(_ address: String, _ port: Int, _ password: String)
  case onOffSubtitle(_ address: String, _ port: Int, _ password: String)
  case snapshot(_ address: String, _ port: Int, _ password: String)
  case info(_ address: String, _ port: Int, _ password: String)
}

extension MpcTarget: TargetType, MediaPlayerTargetType {
  var baseURL: URL {
    switch self {
    case let .status(address, port, _):
      return baseUrl(address, port)
    case let .setVolume(_, address, port, _):
      return baseUrl(address, port)
    case let .seek(_, address, port, _):
      return baseUrl(address, port)
    case let .repeatPlayForever(address, port, _):
      return baseUrl(address, port)
    case let .repeatPlayOneFile(address, port, _):
      return baseUrl(address, port)
    case let .repeatPlayWholePlaylist(address, port, _):
      return baseUrl(address, port)
    case let .toggleFullscreen(address, port, _):
      return baseUrl(address, port)
    case let .pause(address, port, _):
      return baseUrl(address, port)
    case let .togglePlayPause(address, port, _):
      return baseUrl(address, port)
    case let .previousFile(address, port, _):
      return baseUrl(address, port)
    case let .nextFile(address, port, _):
      return baseUrl(address, port)
    case let .nextAudioTrack(address, port, _):
      return baseUrl(address, port)
    case let .nextSubtitleTrack(address, port, _):
      return baseUrl(address, port)
    case let .onOffSubtitle(address, port, _):
      return baseUrl(address, port)
    case let .snapshot(address, port, _):
      return baseUrl(address, port)
    case let .info(address, port, _):
      return baseUrl(address, port)
    }
  }

  var path: String {
    switch self {
    case .status:
      return "variables.html"
    case .snapshot:
      return "snapshot.jpg"
    case .info:
      return "info.html"
    default:
      return "command.html"
    }
  }

  var method: Method {
    switch self {
    case .seek, .setVolume: return .post
    default: return .get
    }
  }

  var sampleData: Data {
    return Data()
  }

  var task: Task {
    let commandKey = "wm_command"
    let encoding = URLEncoding(destination: .queryString)
    switch self {
    case .status, .info:
      return .requestPlain
    case let .setVolume(volume, _, _, _):
      return .requestParameters(parameters: [commandKey: "-2", "volume": volume], encoding: encoding)
    case let .seek(positionString, _, _, _):
      return .requestParameters(parameters: [commandKey: "-1", "position": positionString], encoding: encoding)
    case .repeatPlayForever:
      return .requestParameters(parameters: [commandKey: "33449"], encoding: encoding)
    case .repeatPlayOneFile:
      return .requestParameters(parameters: [commandKey: "33450"], encoding: encoding)
    case .repeatPlayWholePlaylist:
      return .requestParameters(parameters: [commandKey: "33451"], encoding: encoding)
    case .toggleFullscreen:
      return .requestParameters(parameters: [commandKey: "830"], encoding: encoding)
    case .pause:
      return .requestParameters(parameters: [commandKey: "888"], encoding: encoding)
    case .togglePlayPause:
      return .requestParameters(parameters: [commandKey: "889"], encoding: encoding)
    case .previousFile:
      return .requestParameters(parameters: [commandKey: "919"], encoding: encoding)
    case .nextFile:
      return .requestParameters(parameters: [commandKey: "920"], encoding: encoding)
    case .nextAudioTrack:
      return .requestParameters(parameters: [commandKey: "952"], encoding: encoding)
    case .nextSubtitleTrack:
      return .requestParameters(parameters: [commandKey: "954"], encoding: encoding)
    case .onOffSubtitle:
      return .requestParameters(parameters: [commandKey: "956"], encoding: encoding)
    case .snapshot:
      return .requestParameters(parameters: ["1": ""], encoding: encoding)
    }
  }

  var headers: [String: String]? {
    switch self {
    case let .status(_, _, password):
      return headers(password)
    case let .setVolume(_, _, _, password):
      return headers(password)
    case let .seek(_, _, _, password):
      return headers(password)
    case let .repeatPlayForever(_, _, password):
      return headers(password)
    case let .repeatPlayOneFile(_, _, password):
      return headers(password)
    case let .repeatPlayWholePlaylist(_, _, password):
      return headers(password)
    case let .toggleFullscreen(_, _, password):
      return headers(password)
    case let .pause(_, _, password):
      return headers(password)
    case let .togglePlayPause(_, _, password):
      return headers(password)
    case let .previousFile(_, _, password):
      return headers(password)
    case let .nextFile(_, _, password):
      return headers(password)
    case let .nextAudioTrack(_, _, password):
      return headers(password)
    case let .nextSubtitleTrack(_, _, password):
      return headers(password)
    case let .onOffSubtitle(_, _, password):
      return headers(password)
    case let .snapshot(_, _, password):
      return headers(password)
    case let .info(_, _, password):
      return headers(password)
    }
  }

  var validationType: ValidationType {
    return .successCodes
  }
}
