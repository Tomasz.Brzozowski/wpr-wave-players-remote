//
//  HttpMpcProvider.swift
//  WPR - Wave Players Remote
//
//  Created by Tomasz Brzozowski on 02/06/2021.
//

import RxSwift

class HttpMpcService: HttpMediaPlayerProvider {
  private let provider: HttpMpcMoyaProvider

  init(provider: HttpMpcMoyaProvider) {
    self.provider = provider
  }

  func getStatusString(_ serverAddress: String, _ serverPort: Int, _ serverPassword: String) -> Single<String> {
    return provider.moyaSingleString(target: .status(serverAddress, serverPort, serverPassword))
  }

  func getStatusJson(_: String, _: Int, _: String) -> Single<Any> {
    return Single.just("")
  }

  func setVolume(
    _ volume: String,
    _ serverAddress: String,
    _ serverPort: Int,
    _ serverPassword: String
  ) -> Single<Data> {
    return provider.moyaSingleData(target: .setVolume(volume, serverAddress, serverPort, serverPassword))
  }

  func seek(
    _ positionSeconds: String,
    _ serverAddress: String,
    _ serverPort: Int,
    _ serverPassword: String
  ) -> Single<Data> {
    return provider.moyaSingleData(target: .seek(positionSeconds, serverAddress, serverPort, serverPassword))
  }

  func pause(_ serverAddress: String, _ serverPort: Int, _ serverPassword: String) -> Single<Data> {
    return provider.moyaSingleData(target: .pause(serverAddress, serverPort, serverPassword))
  }

  func togglePlayPause(_ serverAddress: String, _ serverPort: Int, _ serverPassword: String) -> Single<Data> {
    return provider.moyaSingleData(target: .togglePlayPause(serverAddress, serverPort, serverPassword))
  }

  func previousFile(_ serverAddress: String, _ serverPort: Int, _ serverPassword: String) -> Single<Data> {
    return provider.moyaSingleData(target: .previousFile(serverAddress, serverPort, serverPassword))
  }

  func nextFile(_ serverAddress: String, _ serverPort: Int, _ serverPassword: String) -> Single<Data> {
    return provider.moyaSingleData(target: .nextFile(serverAddress, serverPort, serverPassword))
  }

  func nextSubtitleTrack(_ serverAddress: String, _ serverPort: Int, _ serverPassword: String) -> Single<Data> {
    return provider.moyaSingleData(target: .nextSubtitleTrack(serverAddress, serverPort, serverPassword))
  }

  func onOffSubtitle(_ serverAddress: String, _ serverPort: Int, _ serverPassword: String) -> Single<Data> {
    return provider.moyaSingleData(target: .onOffSubtitle(serverAddress, serverPort, serverPassword))
  }

  func toggleFullscreen(_ serverAddress: String, _ serverPort: Int, _ serverPassword: String) -> Single<Data> {
    return provider.moyaSingleData(target: .toggleFullscreen(serverAddress, serverPort, serverPassword))
  }

  func nextAudioTrack(_ serverAddress: String, _ serverPort: Int, _ serverPassword: String) -> Single<Data> {
    return provider.moyaSingleData(target: .nextAudioTrack(serverAddress, serverPort, serverPassword))
  }

  func nextRepeatMode(_: String, _: Int, _: String) -> Single<Data> {
    return Single.just(Data())
  }

  func repeatPlayForever(_ serverAddress: String, _ serverPort: Int, _ serverPassword: String) -> Single<Data> {
    return provider.moyaSingleData(target: .repeatPlayForever(serverAddress, serverPort, serverPassword))
  }

  func repeatPlayOneFile(_ serverAddress: String, _ serverPort: Int, _ serverPassword: String) -> Single<Data> {
    return provider.moyaSingleData(target: .repeatPlayOneFile(serverAddress, serverPort, serverPassword))
  }

  func repeatPlayWholePlaylist(_ serverAddress: String, _ serverPort: Int, _ serverPassword: String) -> Single<Data> {
    return provider.moyaSingleData(target: .repeatPlayWholePlaylist(serverAddress, serverPort, serverPassword))
  }

  func getSnapshot(_ serverAddress: String, _ serverPort: Int, _ serverPassword: String) -> Single<Data> {
    return provider.moyaSingleData(target: .snapshot(serverAddress, serverPort, serverPassword))
  }

  func getInfo(_ serverAddress: String, _ serverPort: Int, _ serverPassword: String) -> Single<Data> {
    return provider.moyaSingleData(target: .info(serverAddress, serverPort, serverPassword))
  }
}
