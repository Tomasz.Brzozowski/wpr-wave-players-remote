//
//  MediaPlayerTargetType.swift
//  WPR - Wave Players Remote
//
//  Created by Tomasz Brzozowski on 02/06/2021.
//

import Alamofire

protocol MediaPlayerTargetType {
  func headers(_ password: String) -> [String: String]
  func baseUrl(_ address: String, _ port: Int) -> URL
}

extension MediaPlayerTargetType {
  func baseUrl(_ address: String, _ port: Int) -> URL {
    return URL(string: "http://\(address):\(String(port))")!
  }

  func headers(_ password: String) -> [String: String] {
    let login = ""
    let authData = "\(login):\(password)".data(using: String.Encoding.utf8) ?? Data()
    let authBase64 = authData.base64EncodedString()
    let header = ["Authorization": "Basic \(authBase64)"]
    return header
  }
}
