//
//  VlcTarget.swift
//  WPR - Wave Players Remote
//
//  Created by Tomasz Brzozowski on 02/12/2020.
//

import Moya

enum VlcTarget {
  case status(_ address: String, _ port: Int, _ password: String)
  case setVolume(_ volume: String, _ address: String, _ port: Int, _ password: String)
  case seek(_ positionSeconds: String, _ address: String, _ port: Int, _ password: String)
  case nextRepeatMode(_ address: String, _ port: Int, _ password: String)
  case toggleFullscreen(_ address: String, _ port: Int, _ password: String)
  case pause(_ address: String, _ port: Int, _ password: String)
  case togglePlayPause(_ address: String, _ port: Int, _ password: String)
  case previousFile(_ address: String, _ port: Int, _ password: String)
  case nextFile(_ address: String, _ port: Int, _ password: String)
  case nextAudioTrack(_ address: String, _ port: Int, _ password: String)
  case nextSubtitleTrack(_ address: String, _ port: Int, _ password: String)
}

extension VlcTarget: TargetType, MediaPlayerTargetType {
  var baseURL: URL {
    switch self {
    case let .status(address, port, _):
      return baseUrl(address, port)
    case let .setVolume(_, address, port, _):
      return baseUrl(address, port)
    case let .seek(_, address, port, _):
      return baseUrl(address, port)
    case let .nextRepeatMode(address, port, _):
      return baseUrl(address, port)
    case let .toggleFullscreen(address, port, _):
      return baseUrl(address, port)
    case let .pause(address, port, _):
      return baseUrl(address, port)
    case let .togglePlayPause(address, port, _):
      return baseUrl(address, port)
    case let .previousFile(address, port, _):
      return baseUrl(address, port)
    case let .nextFile(address, port, _):
      return baseUrl(address, port)
    case let .nextAudioTrack(address, port, _):
      return baseUrl(address, port)
    case let .nextSubtitleTrack(address, port, _):
      return baseUrl(address, port)
    }
  }

  var path: String {
    return "/requests/status.json"
  }

  var method: Method {
    switch self {
    case .seek, .setVolume:
      return .post
    default:
      return .get
    }
  }

  var sampleData: Data {
    return Data()
  }

  var task: Task {
    let commandKey = "command"
    let commandValueKey = "val"
    let encoding = URLEncoding(destination: .queryString)
    switch self {
    case .status:
      return .requestPlain
    case let .setVolume(volume, _, _, _):
      return .requestParameters(parameters: [commandKey: "volume", commandValueKey: volume], encoding: encoding)
    case let .seek(positionSeconds, _, _, _):
      return .requestParameters(parameters: [commandKey: "seek", commandValueKey: positionSeconds], encoding: encoding)
    case .nextRepeatMode:
      return .requestParameters(parameters: [commandKey: "key", commandValueKey: "loop"], encoding: encoding)
    case .toggleFullscreen:
      return .requestParameters(parameters: [commandKey: "fullscreen"], encoding: encoding)
    case .pause:
      return .requestParameters(parameters: [commandKey: "pl_forcepause"], encoding: encoding)
    case .togglePlayPause:
      return .requestParameters(parameters: [commandKey: "pl_pause"], encoding: encoding)
    case .previousFile:
      return .requestParameters(parameters: [commandKey: "pl_previous"], encoding: encoding)
    case .nextFile:
      return .requestParameters(parameters: [commandKey: "pl_next"], encoding: encoding)
    case .nextAudioTrack:
      return .requestParameters(parameters: [commandKey: "key", commandValueKey: "audio-track"], encoding: encoding)
    case .nextSubtitleTrack:
      return .requestParameters(parameters: [commandKey: "key", commandValueKey: "subtitle-track"], encoding: encoding)
    }
  }

  var headers: [String: String]? {
    switch self {
    case let .status(_, _, password):
      return headers(password)
    case let .setVolume(_, _, _, password):
      return headers(password)
    case let .seek(_, _, _, password):
      return headers(password)
    case let .nextRepeatMode(_, _, password):
      return headers(password)
    case let .toggleFullscreen(_, _, password):
      return headers(password)
    case let .pause(_, _, password):
      return headers(password)
    case let .togglePlayPause(_, _, password):
      return headers(password)
    case let .previousFile(_, _, password):
      return headers(password)
    case let .nextFile(_, _, password):
      return headers(password)
    case let .nextAudioTrack(_, _, password):
      return headers(password)
    case let .nextSubtitleTrack(_, _, password):
      return headers(password)
    }
  }

  var validationType: ValidationType {
    return .successCodes
  }
}
