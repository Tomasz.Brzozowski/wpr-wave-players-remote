//
//  HttpBaseMoyaProvider.swift
//  WPR - Wave Players Remote
//
//  Created by Tomasz Brzozowski on 02/06/2021.
//

import Moya
import RxSwift

class BaseMoyaProvider<Target: Moya.TargetType> {
  let provider: MoyaProvider<Target>

  init(
    endpointClosure: @escaping MoyaProvider<Target>.EndpointClosure = MoyaProvider<Target>.defaultEndpointMapping,
    requestClosure: @escaping MoyaProvider<Target>.RequestClosure = MoyaProvider<Target>.defaultRequestMapping,
    stubClosure: @escaping MoyaProvider<Target>.StubClosure = MoyaProvider.neverStub,
    plugins: [PluginType] = [],
    session: Session = {
      let configuration = URLSessionConfiguration.af.default
      configuration.timeoutIntervalForRequest = TimeInterval(3)
      configuration.timeoutIntervalForResource = TimeInterval(3)
      configuration.httpMaximumConnectionsPerHost = 270 * 2
      return Session(configuration: configuration)
    }(),
    trackInflights: Bool = false
  ) {
    provider = MoyaProvider(endpointClosure: endpointClosure, requestClosure: requestClosure,
                            stubClosure: stubClosure, session: session, plugins: plugins,
                            trackInflights: trackInflights)
  }

  func moyaSingleData(target: Target) -> Single<Data> {
    return Single<Data>.create { singleResult -> Disposable in
      self.provider.request(target) { result in
        switch result {
        case let .success(response):
          singleResult(.success(response.data))
        case let .failure(error):
          singleResult(.failure(error))
        }
      }
      return Disposables.create()
    }
  }

  func moyaSingleString(target: Target) -> Single<String> {
    return Single<String>.create { singleResult -> Disposable in
      self.provider.request(target) { result in
        switch result {
        case let .success(response):
          do {
            let string = try response.mapString()
            singleResult(.success(string))
          } catch {
            singleResult(.failure(error))
          }
        case let .failure(error):
          singleResult(.failure(error))
        }
      }
      return Disposables.create()
    }
  }

  func moyaSingleJson(target: Target) -> Single<Any> {
    return Single<Any>.create { singleResult -> Disposable in
      self.provider.request(target) { result in
        switch result {
        case let .success(response):
          do {
            let json = try response.mapJSON()
            singleResult(.success(json))
          } catch {
            singleResult(.failure(error))
          }
        case let .failure(error):
          singleResult(.failure(error))
        }
      }
      return Disposables.create()
    }
  }
}
