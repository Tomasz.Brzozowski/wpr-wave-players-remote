//
//  HttpVlcProvider.swift
//  WPR - Wave Players Remote
//
//  Created by Tomasz Brzozowski on 01/06/2021.
//

import RxSwift

class HttpVlcService: HttpMediaPlayerProvider {
  private let provider: HttpVlcMoyaProvider

  init(provider: HttpVlcMoyaProvider) {
    self.provider = provider
  }

  func getStatusString(_: String, _: Int, _: String) -> Single<String> {
    return Single.just("")
  }

  func getStatusJson(_ serverAddress: String, _ serverPort: Int, _ serverPassword: String) -> Single<Any> {
    return provider.moyaSingleJson(target: .status(serverAddress, serverPort, serverPassword))
  }

  func setVolume(
    _ volume: String,
    _ serverAddress: String,
    _ serverPort: Int,
    _ serverPassword: String
  ) -> Single<Data> {
    return provider.moyaSingleData(target: .setVolume(volume, serverAddress, serverPort, serverPassword))
  }

  func seek(
    _ positionSeconds: String,
    _ serverAddress: String,
    _ serverPort: Int,
    _ serverPassword: String
  ) -> Single<Data> {
    return provider.moyaSingleData(target: .seek(positionSeconds, serverAddress, serverPort, serverPassword))
  }

  func pause(_ serverAddress: String, _ serverPort: Int, _ serverPassword: String) -> Single<Data> {
    return provider.moyaSingleData(target: .pause(serverAddress, serverPort, serverPassword))
  }

  func togglePlayPause(_ serverAddress: String, _ serverPort: Int, _ serverPassword: String) -> Single<Data> {
    return provider.moyaSingleData(target: .togglePlayPause(serverAddress, serverPort, serverPassword))
  }

  func previousFile(_ serverAddress: String, _ serverPort: Int, _ serverPassword: String) -> Single<Data> {
    return provider.moyaSingleData(target: .previousFile(serverAddress, serverPort, serverPassword))
  }

  func nextFile(_ serverAddress: String, _ serverPort: Int, _ serverPassword: String) -> Single<Data> {
    return provider.moyaSingleData(target: .nextFile(serverAddress, serverPort, serverPassword))
  }

  func nextSubtitleTrack(_ serverAddress: String, _ serverPort: Int, _ serverPassword: String) -> Single<Data> {
    return provider.moyaSingleData(target: .nextSubtitleTrack(serverAddress, serverPort, serverPassword))
  }

  func onOffSubtitle(_: String, _: Int, _: String) -> Single<Data> {
    Single.just(Data())
  }

  func toggleFullscreen(_ serverAddress: String, _ serverPort: Int, _ serverPassword: String) -> Single<Data> {
    return provider.moyaSingleData(target: .toggleFullscreen(serverAddress, serverPort, serverPassword))
  }

  func nextAudioTrack(_ serverAddress: String, _ serverPort: Int, _ serverPassword: String) -> Single<Data> {
    return provider.moyaSingleData(target: .nextAudioTrack(serverAddress, serverPort, serverPassword))
  }

  func nextRepeatMode(_ serverAddress: String, _ serverPort: Int, _ serverPassword: String) -> Single<Data> {
    return provider.moyaSingleData(target: .nextRepeatMode(serverAddress, serverPort, serverPassword))
  }

  func repeatPlayForever(_: String, _: Int, _: String) -> Single<Data> {
    Single.just(Data())
  }

  func repeatPlayOneFile(_: String, _: Int, _: String) -> Single<Data> {
    Single.just(Data())
  }

  func repeatPlayWholePlaylist(_: String, _: Int, _: String) -> Single<Data> {
    Single.just(Data())
  }

  func getSnapshot(_: String, _: Int, _: String) -> Single<Data> {
    Single.just(Data())
  }

  func getInfo(_: String, _: Int, _: String) -> Single<Data> {
    Single.just(Data())
  }
}
