//
//  HttpMediaPlayerProvider.swift
//  WPR - Wave Players Remote
//
//  Created by Tomasz Brzozowski on 01/06/2021.
//

import RxSwift

protocol HttpMediaPlayerProvider {
  func getStatusString(_ serverAddress: String, _ serverPort: Int, _ serverPassword: String) -> Single<String>
  func getStatusJson(_ serverAddress: String, _ serverPort: Int, _ serverPassword: String) -> Single<Any>
  func setVolume(_ volume: String, _ serverAddress: String, _ serverPort: Int, _ serverPassword: String)
    -> Single<Data>
  func seek(_ positionSeconds: String, _ serverAddress: String, _ serverPort: Int, _ serverPassword: String)
    -> Single<Data>
  func pause(_ serverAddress: String, _ serverPort: Int, _ serverPassword: String) -> Single<Data>
  func togglePlayPause(_ serverAddress: String, _ serverPort: Int, _ serverPassword: String) -> Single<Data>
  func previousFile(_ serverAddress: String, _ serverPort: Int, _ serverPassword: String) -> Single<Data>
  func nextFile(_ serverAddress: String, _ serverPort: Int, _ serverPassword: String) -> Single<Data>
  func nextSubtitleTrack(_ serverAddress: String, _ serverPort: Int, _ serverPassword: String) -> Single<Data>
  func onOffSubtitle(_ serverAddress: String, _ serverPort: Int, _ serverPassword: String) -> Single<Data>
  func toggleFullscreen(_ serverAddress: String, _ serverPort: Int, _ serverPassword: String) -> Single<Data>
  func nextAudioTrack(_ serverAddress: String, _ serverPort: Int, _ serverPassword: String) -> Single<Data>
  func nextRepeatMode(_ serverAddress: String, _ serverPort: Int, _ serverPassword: String) -> Single<Data>
  func repeatPlayForever(_ serverAddress: String, _ serverPort: Int, _ serverPassword: String) -> Single<Data>
  func repeatPlayOneFile(_ serverAddress: String, _ serverPort: Int, _ serverPassword: String) -> Single<Data>
  func repeatPlayWholePlaylist(_ serverAddress: String, _ serverPort: Int, _ serverPassword: String) -> Single<Data>
  func getSnapshot(_ serverAddress: String, _ serverPort: Int, _ serverPassword: String) -> Single<Data>
  func getInfo(_ serverAddress: String, _ serverPort: Int, _ serverPassword: String) -> Single<Data>
}
