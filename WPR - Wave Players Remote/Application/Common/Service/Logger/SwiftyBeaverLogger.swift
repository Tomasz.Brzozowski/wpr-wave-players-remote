//
//  SwiftyBeaverLogger.swift
//  WPR - Wave Players Remote
//
//  Created by Tomasz Brzozowski on 26/05/2021.
//

import SwiftyBeaver

class SwiftyBeaverLogger: Logger {
  let log = SwiftyBeaver.self

  init() {
    let console = ConsoleDestination()
    let file = FileDestination()
    log.addDestination(console)
    log.addDestination(file)
  }

  func verbose(_ message: Any, _ file: String, _ function: String, _ line: Int) {
    log.custom(level: .verbose, message: message, file: file, function: function, line: line)
  }

  func debug(_ message: Any, _ file: String, _ function: String, _ line: Int) {
    log.custom(level: .debug, message: message, file: file, function: function, line: line)
  }

  func info(_ message: Any, _ file: String, _ function: String, _ line: Int) {
    log.custom(level: .info, message: message, file: file, function: function, line: line)
  }

  func warning(_ message: Any, _ file: String, _ function: String, _ line: Int) {
    log.custom(level: .warning, message: message, file: file, function: function, line: line)
  }

  func error(_ message: Any, _ file: String, _ function: String, _ line: Int) {
    log.custom(level: .error, message: message, file: file, function: function, line: line)
  }
}
