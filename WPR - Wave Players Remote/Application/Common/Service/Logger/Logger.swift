//
//  Logger.swift
//  WPR - Wave Players Remote
//
//  Created by Tomasz Brzozowski on 26/05/2021.
//

protocol Logger {
  func verbose(_ message: Any, _ file: String, _ function: String, _ line: Int)
  func debug(_ message: Any, _ file: String, _ function: String, _ line: Int)
  func info(_ message: Any, _ file: String, _ function: String, _ line: Int)
  func warning(_ message: Any, _ file: String, _ function: String, _ line: Int)
  func error(_ message: Any, _ file: String, _ function: String, _ line: Int)
}
