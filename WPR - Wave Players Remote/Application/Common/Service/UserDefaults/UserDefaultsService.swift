//
//  UserDefaultsService.swift
//  WPR - Wave Players Remote
//
//  Created by Tomasz Brzozowski on 09/04/2021.
//

import RxSwift

class UserDefaultsService: UserDefaultsProvider {
  func serverAddress() -> String { UserDefaults.standard.serverAddress }
  func serverPort() -> Int { UserDefaults.standard.serverPort }
  func serverPassword() -> String { UserDefaults.standard.serverPassword }
  func controlledPlayer() -> MediaPlayer { MediaPlayer(rawValue: controlledPlayerValue()) ?? .mpc }
  func vibrateOnWave() -> Bool { UserDefaults.standard.vibrateOnWave }
  func pauseOnIncomingCall() -> Bool { UserDefaults.standard.pauseOnIncomingCall }
  func dimScreenTimeout() -> DimScreenTimeout { DimScreenTimeout(rawValue: dimScreenTimeoutValue()) ?? .ten }

  private func controlledPlayerValue() -> Int { UserDefaults.standard.controlledPlayer }
  private func dimScreenTimeoutValue() -> Int { UserDefaults.standard.dimScreenTimeout }

  func setServerAddress(_ value: String) {
    setAnyValue(value, forKey: .serverAddress)
  }

  private func setAnyValue<ValueType: Any>(_ value: ValueType, forKey key: UserDefaultsKey) {
    UserDefaults.standard.set(value, forKey: key.rawValue)
  }

  func setServerPort(_ value: Int) {
    setAnyValue(value, forKey: .serverPort)
  }

  func setServerPassword(_ value: String) {
    setAnyValue(value, forKey: .serverPassword)
  }

  func setControlledPlayer(_ value: MediaPlayer) {
    setAnyValue(value.rawValue, forKey: .controlledPlayer)
  }

  func setVibrateOnWave(_ value: Bool) {
    setAnyValue(value, forKey: .vibrateOnWave)
  }

  func setPauseOnIncomingCall(_ value: Bool) {
    setAnyValue(value, forKey: .pauseOnIncomingCall)
  }

  func setDimScreenTimeout(_ value: DimScreenTimeout) {
    setAnyValue(value.rawValue, forKey: .dimScreenTimeout)
  }

  func serverAddressObservable() -> Observable<String> {
    return userDefaultsObservable(key: .serverAddress, startWith: serverAddress())
  }

  private func userDefaultsObservable<Type: Equatable>(
    key: UserDefaultsKey,
    startWith value: Type
  ) -> Observable<Type> {
    return UserDefaults.standard.rx.observe(Type.self, key.rawValue)
      .distinctUntilChanged()
      .startWith(value)
      .compactMap { $0 }
  }

  func serverPortObservable() -> Observable<Int> {
    return userDefaultsObservable(key: .serverPort, startWith: serverPort())
  }

  func serverPasswordObservable() -> Observable<String> {
    return userDefaultsObservable(key: .serverPassword, startWith: serverPassword())
  }

  func controlledPlayerObservable() -> Observable<MediaPlayer> {
    return userDefaultsObservable(key: .controlledPlayer, startWith: controlledPlayerValue())
      .map { controlledPlayer -> MediaPlayer in
        MediaPlayer(rawValue: controlledPlayer) ?? .mpc
      }
  }

  func vibrateOnWaveObservable() -> Observable<Bool> {
    return userDefaultsObservable(key: .vibrateOnWave, startWith: vibrateOnWave())
  }

  func pauseOnIncomingCallObservable() -> Observable<Bool> {
    return userDefaultsObservable(key: .pauseOnIncomingCall, startWith: pauseOnIncomingCall())
  }

  func dimScreenTimeoutObservable() -> Observable<DimScreenTimeout> {
    return userDefaultsObservable(key: .dimScreenTimeout, startWith: dimScreenTimeoutValue())
      .map { dimScreenTimeout -> DimScreenTimeout in
        DimScreenTimeout(rawValue: dimScreenTimeout) ?? .ten
      }
  }
}
