//
//  UserDefaultsProvider.swift
//  WPR - Wave Players Remote
//
//  Created by Tomasz Brzozowski on 09/04/2021.
//

import RxSwift

protocol UserDefaultsProvider {
  func serverAddress() -> String
  func serverPort() -> Int
  func serverPassword() -> String
  func controlledPlayer() -> MediaPlayer
  func vibrateOnWave() -> Bool
  func pauseOnIncomingCall() -> Bool
  func dimScreenTimeout() -> DimScreenTimeout

  func setServerAddress(_ value: String)
  func setServerPort(_ value: Int)
  func setServerPassword(_ value: String)
  func setControlledPlayer(_ value: MediaPlayer)
  func setVibrateOnWave(_ value: Bool)
  func setPauseOnIncomingCall(_ value: Bool)
  func setDimScreenTimeout(_ value: DimScreenTimeout)

  func serverAddressObservable() -> Observable<String>
  func serverPortObservable() -> Observable<Int>
  func serverPasswordObservable() -> Observable<String>
  func controlledPlayerObservable() -> Observable<MediaPlayer>
  func vibrateOnWaveObservable() -> Observable<Bool>
  func pauseOnIncomingCallObservable() -> Observable<Bool>
  func dimScreenTimeoutObservable() -> Observable<DimScreenTimeout>
}
