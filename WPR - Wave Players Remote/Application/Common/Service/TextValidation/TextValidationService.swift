//
//  TextValidationService.swift
//  WPR - Wave Players Remote
//
//  Created by Tomasz Brzozowski on 04/05/2021.
//

import Foundation

class TextValidationService: TextValidationProvider {
  func validateIpWhileTyping(_ text: String) -> Bool {
    let pattern = """
    ^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])[.]){0,3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])?$
    """
    return evaluateNSPredicate(with: text, pattern: pattern)
  }

  func validateIp(_ text: String) -> Bool {
    let pattern = """
    ^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$
    """
    return evaluateNSPredicate(with: text, pattern: pattern)
  }

  private func evaluateNSPredicate(with text: String, pattern: String) -> Bool {
    let regexText = NSPredicate(format: "SELF MATCHES %@", pattern)
    return regexText.evaluate(with: text)
  }

  func validatePortText(_ text: String) -> Bool {
    if let port = Int(text), port >= 1, port <= 65535 {
      return true
    }
    return false
  }

  func textWithoutLastCharacter(from text: String) -> String {
    let range = text.startIndex ..< text.index(text.endIndex, offsetBy: -1)
    return String(text[range])
  }
}
