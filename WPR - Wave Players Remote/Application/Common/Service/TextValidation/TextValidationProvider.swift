//
//  TextValidationProvider.swift
//  WPR - Wave Players Remote
//
//  Created by Tomasz Brzozowski on 04/05/2021.
//

protocol TextValidationProvider {
  func validateIpWhileTyping(_ text: String) -> Bool
  func validateIp(_ text: String) -> Bool
  func validatePortText(_ text: String) -> Bool
  func textWithoutLastCharacter(from text: String) -> String
}
