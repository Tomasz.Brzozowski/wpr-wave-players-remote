//
//  UserDefaultsKey.swift
//  WPR - Wave Players Remote
//
//  Created by Tomasz Brzozowski on 20/04/2021.
//

enum UserDefaultsKey: String {
  case serverAddress
  case serverPort
  case serverPassword
  case controlledPlayer
  case vibrateOnWave
  case pauseOnIncomingCall
  case dimScreenTimeout
}
