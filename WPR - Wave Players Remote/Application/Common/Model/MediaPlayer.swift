//
//  MediaPlayer.swift
//  WPR - Wave Players Remote
//
//  Created by Tomasz Brzozowski on 22/04/2021.
//

enum MediaPlayer: Int {
  case mpc = 0
  case vlc = 1
}
