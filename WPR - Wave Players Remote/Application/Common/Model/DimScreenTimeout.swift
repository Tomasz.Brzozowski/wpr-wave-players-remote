//
//  DimScreenTimeout.swift
//  WPR - Wave Players Remote
//
//  Created by Tomasz Brzozowski on 26/04/2021.
//

enum DimScreenTimeout: Int {
  case five = 5
  case ten = 10
  case thirty = 30
  case oneMinute = 60
  case twoMinutes = 120
  case threeMinutes = 180
  case never = -1
}
