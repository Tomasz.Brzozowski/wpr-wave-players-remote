//
//  UINavigationBar+Extension.swift
//  WPR - Wave Players Remote
//
//  Created by Tomasz Brzozowski on 12/03/2021.
//

import UIKit

extension UINavigationBar {
  convenience init(
    barTintColor: UIColor,
    tintColor: UIColor? = R.color.onPrimary(),
    font: UIFont = .wprFont(size: 19),
    isTranslucent: Bool = false
  ) {
    self.init()
    self.barTintColor = barTintColor
    self.tintColor = tintColor ?? .init(white: 1, alpha: 1)
    self.titleTextAttributes = [NSAttributedString.Key.font: font]
    self.isTranslucent = isTranslucent
  }
}
