//
//  UIImageView+Extension.swift
//  WPR - Wave Players Remote
//
//  Created by Tomasz Brzozowski on 13/11/2020.
//

import UIKit

extension UIImageView {
  func setImageColor(color: UIColor?) {
    let templateImage = image?.withRenderingMode(UIImage.RenderingMode.alwaysTemplate)
    image = templateImage
    tintColor = color
  }
}
