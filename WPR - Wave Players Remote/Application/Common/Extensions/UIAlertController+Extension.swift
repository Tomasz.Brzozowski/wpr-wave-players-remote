//
//  UIAlertController+Extension.swift
//  WPR - Wave Players Remote
//
//  Created by Tomasz Brzozowski on 05/05/2021.
//

import UIKit

extension UIAlertController {
  convenience init(
    message: String,
    preferredStyle: UIAlertController.Style,
    font: UIFont = .wprFont()
  ) {
    self.init(title: "", message: "", preferredStyle: preferredStyle)
    let messageAttributes = [NSAttributedString.Key.font: font]
    let messageString = NSAttributedString(string: message, attributes: messageAttributes)
    self.setValue(messageString, forKey: "attributedMessage")
  }
}
