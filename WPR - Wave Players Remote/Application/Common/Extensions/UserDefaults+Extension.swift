//
//  UserDefaults+Extension.swift
//  WPR - Wave Players Remote
//
//  Created by Tomasz Brzozowski on 09/04/2021.
//

import Foundation

extension UserDefaults {
  var serverAddress: String {
    get {
      let val = value(forKey: UserDefaultsKey.serverAddress.rawValue)
      return val as? String ?? "192.168.0.0"
    }
    set {
      set(newValue, forKey: UserDefaultsKey.serverAddress.rawValue)
    }
  }

  var serverPort: Int {
    get {
      let val = value(forKey: UserDefaultsKey.serverPort.rawValue)
      return val as? Int ?? 13579
    }
    set {
      set(newValue, forKey: UserDefaultsKey.serverPort.rawValue)
    }
  }

  var serverPassword: String {
    get {
      let val = value(forKey: UserDefaultsKey.serverPassword.rawValue)
      return val as? String ?? "1234"
    }
    set {
      set(newValue, forKey: UserDefaultsKey.serverPassword.rawValue)
    }
  }

  var controlledPlayer: Int {
    get {
      let val = value(forKey: UserDefaultsKey.controlledPlayer.rawValue)
      return val as? Int ?? 0
    }
    set {
      set(newValue, forKey: UserDefaultsKey.controlledPlayer.rawValue)
    }
  }

  var vibrateOnWave: Bool {
    get {
      let val = value(forKey: UserDefaultsKey.vibrateOnWave.rawValue)
      return val as? Bool ?? true
    }
    set {
      set(newValue, forKey: UserDefaultsKey.vibrateOnWave.rawValue)
    }
  }

  var pauseOnIncomingCall: Bool {
    get {
      let val = value(forKey: UserDefaultsKey.pauseOnIncomingCall.rawValue)
      return val as? Bool ?? true
    }
    set {
      set(newValue, forKey: UserDefaultsKey.pauseOnIncomingCall.rawValue)
    }
  }

  var dimScreenTimeout: Int {
    get {
      let val = value(forKey: UserDefaultsKey.dimScreenTimeout.rawValue)
      return val as? Int ?? 10
    }
    set {
      set(newValue, forKey: UserDefaultsKey.dimScreenTimeout.rawValue)
    }
  }
}
