//
//  UIDevice+Extension.swift
//  WPR - Wave Players Remote
//
//  Created by Tomasz Brzozowski on 18/02/2021.
//

import AudioToolbox
import UIKit

extension UIDevice {
  static func vibrate(_ feedbackType: UINotificationFeedbackGenerator.FeedbackType = .success) {
    let generator = UINotificationFeedbackGenerator()
    generator.notificationOccurred(feedbackType)
  }
}
