//
//  UIView+Extension.swift
//  WPR - Wave Players Remote
//
//  Created by Tomasz Brzozowski on 05/03/2021.
//

import UIKit

extension UIView {
  convenience init(backgroundColor: UIColor?) {
    self.init()
    self.backgroundColor = backgroundColor
  }

  convenience init(frame: CGRect, isUserInteractionEnabled: Bool = false, backgroundColor: UIColor?) {
    self.init()
    self.frame = frame
    self.isUserInteractionEnabled = isUserInteractionEnabled
    self.backgroundColor = backgroundColor
  }
}
