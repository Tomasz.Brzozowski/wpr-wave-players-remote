//
//  BinaryInteger+Extension.swift
//  WPR - Wave Players Remote
//
//  Created by Tomasz Brzozowski on 18/01/2021.
//

extension BinaryInteger {
  func clamped(to range: ClosedRange<Self>) -> Self {
    return max(min(self, range.upperBound), range.lowerBound)
  }
}
