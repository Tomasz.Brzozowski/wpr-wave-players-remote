//
//  UIButton+Extension.swift
//  WPR - Wave Players Remote
//
//  Created by Tomasz Brzozowski on 13/11/2020.
//

import UIKit

extension UIButton {
  convenience init(image: UIImage?, text: String, contentMode: UIView.ContentMode = .scaleAspectFit,
                   imageColor: UIColor? = R.color.onBackground()) {
    self.init()
    titleLabel?.text = text
    if image == nil {
      setTitle(text, for: .normal)
      setTitleColor(R.color.onBackground(), for: .normal)
      titleLabel?.font = .wprFont(size: 12)
      titleLabel?.numberOfLines = 0
      titleLabel?.lineBreakMode = .byWordWrapping
      titleLabel?.textAlignment = .center
      let inset = CGFloat(3)
      contentEdgeInsets = UIEdgeInsets(top: inset, left: inset, bottom: inset, right: inset)
    }
    imageView?.contentMode = contentMode
    setImage(image, for: .normal)
    imageView?.setImageColor(color: imageColor)
  }
}
