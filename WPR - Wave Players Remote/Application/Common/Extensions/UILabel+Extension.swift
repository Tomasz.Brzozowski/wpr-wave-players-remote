//
//  UILabel+Extension.swift
//  WPR - Wave Players Remote
//
//  Created by Tomasz Brzozowski on 13/11/2020.
//

import UIKit

extension UILabel {
  convenience init(
    textColor: UIColor? = R.color.onSurface(),
    text: String = "",
    textAlignment: NSTextAlignment = .left,
    font: UIFont = .wprFont(),
    backgroundColor: UIColor? = .init(white: 0, alpha: 0)
  ) {
    self.init()
    self.text = text
    self.textColor = textColor ?? .init(white: 1, alpha: 1)
    self.textAlignment = textAlignment
    self.font = font
    self.backgroundColor = backgroundColor
  }
}
