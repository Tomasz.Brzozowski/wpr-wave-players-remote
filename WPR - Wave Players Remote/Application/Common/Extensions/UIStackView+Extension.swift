//
//  UIStackView+Extension.swift
//  WPR - Wave Players Remote
//
//  Created by Tomasz Brzozowski on 13/11/2020.
//

import UIKit

extension UIStackView {
  convenience init(axis: NSLayoutConstraint.Axis = .horizontal,
                   distribution: UIStackView.Distribution = .fillEqually,
                   alignment: UIStackView.Alignment = .center) {
    self.init()
    self.axis = axis
    self.distribution = distribution
    self.alignment = alignment
  }
}
