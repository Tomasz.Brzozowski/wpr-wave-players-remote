//
//  UIFont+Extension.swift
//  WPR - Wave Players Remote
//
//  Created by Tomasz Brzozowski on 13/11/2020.
//

import UIKit

extension UIFont {
  static func wprFont(size: CGFloat = 17) -> UIFont {
    return R.font.avenirNextCondensedMedium(size: size) ?? UIFont.systemFont(ofSize: size, weight: .medium)
  }
}
