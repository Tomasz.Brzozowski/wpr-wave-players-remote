//
//  UISlider+Extension.swift
//  WPR - Wave Players Remote
//
//  Created by Tomasz Brzozowski on 13/11/2020.
//

import UIKit

extension UISlider {
  convenience init(thumbAndMinimumTrackTintColor: UIColor? = nil,
                   maximumTrackTintColor: UIColor? = R.color.greyedOut(),
                   valueImageColor: UIColor? = R.color.onBackground(),
                   minimumValueImage: UIImage? = nil,
                   maximumValueImage: UIImage? = nil) {
    self.init()
    self.tintColor = valueImageColor
    if let color = thumbAndMinimumTrackTintColor {
      self.thumbTintColor = color
      self.minimumTrackTintColor = color
    }
    self.maximumTrackTintColor = maximumTrackTintColor
    self.minimumValueImage = minimumValueImage
    self.maximumValueImage = maximumValueImage
  }
}
