//
//  MainTabBarController.swift
//  WPR - Wave Players Remote
//
//  Created by Tomasz Brzozowski on 24/05/2021.
//

import RxSwift
import UIKit

class MainTabBarController: UITabBarController {
  private let userDefaultsService: UserDefaultsProvider
  private let remoteVc: RemoteViewController
  private let serverManagerVc: ServerManagerViewController
  private let settingsVc: SettingsViewController
  private let disposeBag = DisposeBag()

  init(
    userDefaultsService: UserDefaultsProvider,
    remoteVc: RemoteViewController,
    serverManagerVc: ServerManagerViewController,
    settingsVc: SettingsViewController
  ) {
    self.userDefaultsService = userDefaultsService
    self.remoteVc = remoteVc
    self.serverManagerVc = serverManagerVc
    self.settingsVc = settingsVc
    super.init(nibName: nil, bundle: nil)
  }

  @available(*, unavailable)
  required init?(coder _: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  override func viewDidLoad() {
    super.viewDidLoad()
    configureUi()
  }

  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    bindUserDefaults()
  }

  private func configureUi() {
    let remoteNC = generateNavigationController(
      rootViewController: remoteVc,
      title: R.string.localizable.remoteViewTitle(),
      image: R.image.main.icon_remote() ?? UIImage()
    )
    let serverManagerNC = generateNavigationController(
      rootViewController: serverManagerVc,
      title: R.string.localizable.serverManagerViewTitle(),
      image: R.image.main.icon_server_manager() ?? UIImage()
    )
    let settingsNC = generateNavigationController(
      rootViewController: settingsVc,
      title: R.string.localizable.settingsViewTitle(),
      image: R.image.main.icon_settings() ?? UIImage()
    )
    viewControllers = [remoteNC, serverManagerNC, settingsNC]

    UITabBarItem.appearance().setTitleTextAttributes(
      [NSAttributedString.Key.font: UIFont.wprFont(size: 12)],
      for: .normal
    )

    let barItemTextAttributes = [NSAttributedString.Key.font: UIFont.wprFont(size: 19)]
    UINavigationBar.appearance().barTintColor = tabBar.barTintColor
    UINavigationBar.appearance().titleTextAttributes = barItemTextAttributes
    UINavigationBar.appearance().isTranslucent = true
    UIBarButtonItem.appearance().setTitleTextAttributes(barItemTextAttributes, for: UIControl.State.normal)

    let defaultTextColor = R.color.onSurface()
    UILabel.appearance().textColor = defaultTextColor
    UITextField.appearance().textColor = defaultTextColor
  }

  private func generateNavigationController(
    rootViewController: UIViewController,
    title: String,
    image: UIImage
  ) -> UIViewController {
    let navigationVc = UINavigationController(rootViewController: rootViewController)
    navigationVc.tabBarItem.title = title
    navigationVc.tabBarItem.image = image
    return navigationVc
  }

  private func bindUserDefaults() {
    userDefaultsService.controlledPlayerObservable()
      .bind { [weak self] controlledPlayer in
        guard let self = self, let window = self.view.window else { return }
        var primaryColor: UIColor?
        var selectedCellBackgroundColor: UIColor?
        switch controlledPlayer {
        case .mpc:
          primaryColor = R.color.primaryMpc()
          selectedCellBackgroundColor = R.color.primaryLightMpc()
        case .vlc:
          primaryColor = R.color.primaryVlc()
          selectedCellBackgroundColor = R.color.primaryLightVlc()
        }
        if let color = primaryColor {
          window.tintColor = color
          UISwitch.appearance().onTintColor = color
          UISlider.appearance().thumbTintColor = color
          UISlider.appearance().minimumTrackTintColor = color
          self.setTableViewCellSelectedBackgroundColor(
            whenContainedInInstancesOf: [ServerManagerViewController.self],
            to: selectedCellBackgroundColor
          )
          self.setTableViewCellSelectedBackgroundColor(
            whenContainedInInstancesOf: [SettingsViewController.self],
            to: selectedCellBackgroundColor
          )
          window.subviews.forEach { view in
            view.removeFromSuperview()
            window.addSubview(view)
          }
        }
      }.disposed(by: disposeBag)
  }

  private func setTableViewCellSelectedBackgroundColor(
    whenContainedInInstancesOf instances: [UIAppearanceContainer.Type],
    to color: UIColor?
  ) {
    UITableViewCell.appearance(whenContainedInInstancesOf: instances)
      .selectedBackgroundView = UIView(backgroundColor: color)
  }
}
