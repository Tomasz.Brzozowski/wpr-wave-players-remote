//
//  SceneDelegate.swift
//  WPR - Wave Players Remote
//
//  Created by Tomasz Brzozowski on 28/10/2020.
//

import UIKit

class SceneDelegate: UIResponder, UIWindowSceneDelegate {
  var window: UIWindow?

  private let container = DependencyProvider().container

  func scene(_ scene: UIScene, willConnectTo _: UISceneSession, options _: UIScene.ConnectionOptions) {

    let rootVc = container.resolve(MainTabBarController.self)!

    guard let windowScene = (scene as? UIWindowScene) else { return }
    window = UIWindow(frame: UIScreen.main.bounds)
    window?.windowScene = windowScene
    window?.rootViewController = rootVc
    window?.makeKeyAndVisible()
    window?.overrideUserInterfaceStyle = .dark
  }

  func sceneDidDisconnect(_: UIScene) {}

  func sceneDidBecomeActive(_: UIScene) {}

  func sceneWillResignActive(_: UIScene) {}

  func sceneWillEnterForeground(_: UIScene) {}

  func sceneDidEnterBackground(_: UIScene) {}
}
