//
//  DependencyProviderSpec.swift
//  WPR - Wave Players Remote
//
//  Created by Tomasz Brzozowski on 03/06/2021.
//
// swiftlint:disable function_body_length

import Nimble
import Quick
import Swinject
@testable import WPR___Wave_Players_Remote

class DependencyProviderSpec: QuickSpec {
  override func spec() {
    var container: Container!
    beforeEach {
      container = DependencyProvider().container
    }

    describe("Container") {
      it("resolves every type registered in assemblies.") {
        let optionalText: String? = ""
        let serverCell = ServerCell(name: "", address: "", port: "", password: "", isResponding: true)

        expect(container.resolve(Logger.self)).notTo(beNil())

        expect(container.resolve(HttpVlcMoyaProvider.self)).notTo(beNil())
        expect(container.resolve(HttpMpcMoyaProvider.self)).notTo(beNil())
        expect(container.resolve(HttpMediaPlayerProvider.self, name: TypeName.of(HttpVlcService.self))).notTo(beNil())
        expect(container.resolve(HttpMediaPlayerProvider.self, name: TypeName.of(HttpMpcService.self))).notTo(beNil())

        expect(container.resolve(UserDefaultsProvider.self)).notTo(beNil())
        expect(container.resolve(UserNotificationsProvider.self)).notTo(beNil())
        expect(container.resolve(ServerProvider.self, name: TypeName.of(ServerMpcService.self))).notTo(beNil())
        expect(container.resolve(ServerProvider.self, name: TypeName.of(ServerVlcService.self))).notTo(beNil())
        expect(container.resolve(TextValidationProvider.self)).notTo(beNil())
        expect(container.resolve(MediaPlayerClient.self, name: TypeName.of(MpcClient.self))).notTo(beNil())
        expect(container.resolve(MediaPlayerClient.self, name: TypeName.of(VlcClient.self))).notTo(beNil())
        expect(container.resolve(ProximityStateProvider.self)).notTo(beNil())
        expect(container.resolve(IncomingCallProvider.self)).notTo(beNil())

        expect(container.resolve(
          TextSettingsCellViewModel.self, name: TypeName.of(ServerAddressSettingsCellViewModel.self)
        )).notTo(beNil())
        expect(container.resolve(
          TextSettingsCellViewModel.self, name: TypeName.of(ServerPortSettingsCellViewModel.self)
        )).notTo(beNil())
        expect(container.resolve(
          TextSettingsCellViewModel.self, name: TypeName.of(ServerPasswordSettingsCellViewModel.self)
        )).notTo(beNil())
        expect(container.resolve(
          RadioSettingsCellViewModeling.self, name: TypeName.of(ControlledPlayerSettingsCellViewModel.self)
        )).notTo(beNil())
        expect(container.resolve(
          RadioSettingsCellViewModeling.self, name: TypeName.of(DimScreenTimeoutSettingsCellViewModel.self)
        )).notTo(beNil())
        expect(container.resolve(
          ToggleSettingsCellViewModeling.self, name: TypeName.of(VibrateOnWaveSettingsCellViewModel.self)
        )).notTo(beNil())
        expect(container.resolve(
          ToggleSettingsCellViewModeling.self, name: TypeName.of(PauseOnCallSettingsCellViewModel.self)
        )).notTo(beNil())
        expect(container.resolve(SettingsViewModeling.self)).notTo(beNil())
        expect(container.resolve(SettingsViewController.self)).notTo(beNil())
        expect(container.resolve(
          RadioSettingViewModel.self, name: TypeName.of(ControlledPlayerViewModel.self)
        )).notTo(beNil())
        expect(container.resolve(
          RadioSettingViewModel.self, name: TypeName.of(DimScreenTimeoutViewModel.self)
        )).notTo(beNil())
        expect(container.resolve(ControlledPlayerViewController.self)).notTo(beNil())
        expect(container.resolve(DimScreenTimeoutViewController.self)).notTo(beNil())

        expect(container.resolve(
          TextCellViewModel.self, name: TypeName.of(ServerNameDetailCellViewModel.self), argument: ""
        )).notTo(beNil())
        expect(container.resolve(
          TextCellViewModel.self, name: TypeName.of(ServerAddressDetailCellViewModel.self), argument: ""
        )).notTo(beNil())
        expect(container.resolve(
          TextCellViewModel.self, name: TypeName.of(ServerPortDetailCellViewModel.self), argument: ""
        )).notTo(beNil())
        expect(container.resolve(
          TextCellViewModel.self, name: TypeName.of(ServerPasswordDetailCellViewModel.self), argument: ""
        )).notTo(beNil())
        expect(container.resolve(ServerDetailViewModeling.self, arguments: optionalText, optionalText))
          .notTo(beNil())
        expect(container.resolve(ServerDetailViewController.self, arguments: "", "", optionalText, optionalText))
          .notTo(beNil())
        expect(container.resolve(ServerCellViewModeling.self, argument: serverCell)).notTo(beNil())
        expect(container.resolve(ServerManagerViewModeling.self)).notTo(beNil())
        expect(container.resolve(ServerManagerViewController.self)).notTo(beNil())

        expect(container.resolve(RemoteViewModeling.self)).notTo(beNil())
        expect(container.resolve(RemoteViewController.self)).notTo(beNil())

        expect(container.resolve(MainTabBarController.self)).notTo(beNil())
      }
    }
  }
}

// swiftlint:enable function_body_length
