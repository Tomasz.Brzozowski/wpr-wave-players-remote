//
//  rSwiftSpec.swift
//  WPR - Wave Players Remote
//
//  Created by Tomasz Brzozowski on 16/06/2021.
//

import Nimble
import Quick
@testable import WPR___Wave_Players_Remote

class RGeneratedSpec: QuickSpec {
  override func spec() {
    describe("R.generated") {
      it("loads all image assets and custom fonts") {
        do {
          try R.validate()
        } catch {
          fail("\(error)")
        }
      }
    }
  }
}
